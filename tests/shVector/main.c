/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <shds.h>
#include "io.h"

#define DUMP_FILE "error_case.out"

int *idata = NULL;
size_t idata_count = 0;
char **sdata = NULL;
size_t sdata_count = 0;

/*
 * iv: vector of int * i.e. pointers to idata[.]
 * sv: vector of char * i.e. copies of sdata[.]
 */
shVector iv = {0}, sv = {0};

static void _intptrcpy(void *to, void *from)
{
	*(int **)to = (int *)from;
}

static void _charptrcpy(void *to, void *from)
{
	*(char **)to = (char *)from;
}

int icmp(const void *i1, const void *i2)
{
	int i = *(const int *)i1;
	int j = *(const int *)i2;

	if (i == j)
		return 0;
	if (i < j)
		return -1;

	return 1;
}

int ipcmp(const void *i1, const void *i2)
{
	int *i = *(int * const *)i1;
	int *j = *(int * const *)i2;

	return icmp(i, j);
}

int scmp(const void *s1, const void *s2)
{
	return strcmp(*(char * const *)s1, *(char * const *)s2);
}

/* initialize structure */
int test_init(void)
{
	bool good = true;

	sh_vector_free(&iv);
	sh_vector_free(&sv);
	good = good && sh_vector_init(&iv, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_vector_init(&sv, _charptrcpy, sizeof(char *)) == 0;
	/* it's ok to reinitialize with these attributes */
	good = good && sh_vector_init(&iv, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_vector_init(&sv, _charptrcpy, sizeof(char *)) == 0;
	good = good && sh_vector_init(&iv, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_vector_init(&sv, _charptrcpy, sizeof(char *)) == 0;
	good = good && sh_vector_init(&iv, _intptrcpy, sizeof(int *), ipcmp) == 0;
	good = good && sh_vector_init(&sv, _charptrcpy, sizeof(char *), scmp) == 0;

	if (!good)
		dump_data(DUMP_FILE);

	return good?0:-1;
}

/*
 * test whether structure data match the provided data (for integer test)
 * d is the expected results and n is its count
 */
int test_idata(shVector *v, int *d, size_t n)
{
	size_t i;

	if (v->size != n)
		return -1;
	for (i = 0; i < n; ++i)
		if (**(int **)sh_vector_at(v, i) != d[i])
			return -1;

	return 0;
}

/*
 * test whether structure data match the provided data (for string test)
 * d is the expected results and n is its count
 */
int test_sdata(shVector *v, char **d, size_t n)
{
	size_t i;

	if (v->size != n)
		return -1;
	for (i = 0; i < n; ++i)
		if (strcmp(*(char **)sh_vector_at(v, i), d[i]) != 0)
			return -1;

	return 0;
}

int test_append(void)
{
	size_t i;
	bool good = true;

	if (sh_vector_reserve(&iv, idata_count / 3))
		goto exit_no_mem;
	if (sh_vector_reserve(&sv, sdata_count / 3))
		goto exit_no_mem;
	for (i = 0; i < idata_count; ++i)
		if (sh_vector_append(&iv, &idata[i]))
			goto exit_no_mem;
	for (i = 0; i < sdata_count; ++i)
		if (sh_vector_append(&sv, sdata[i]))
			goto exit_no_mem;
	sh_vector_reserve(&iv, idata_count / 3);
	sh_vector_reserve(&sv, sdata_count / 3);
	sh_vector_reserve(&iv, 0);
	sh_vector_reserve(&sv, 0);
	good = good && test_idata(&iv, idata, idata_count) == 0;
	good = good && test_sdata(&sv, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_no_mem:
	fprintf(stderr, "append: out of memory\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "append: mismatch between real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_duplicate(void)
{
	bool good = true;
	shVector iv1, sv1, iv2, sv2;

	/* test empty copy */
	sh_vector_init(&iv1, _intptrcpy, sizeof(int *));
	sh_vector_init(&sv1, _charptrcpy, sizeof(char *));
	good = good && sh_vector_duplicate(&iv1, &iv2) == 0;
	good = good && sh_vector_duplicate(&sv1, &sv2) == 0;
	if (!good)
		goto exit_bad_empty_duplicate;

	good = good && test_idata(&iv2, NULL, 0) == 0;
	good = good && test_sdata(&sv2, NULL, 0) == 0;
	if (!good)
		goto exit_test_empty;

	sh_vector_clear(&iv2);
	sh_vector_clear(&sv2);

	/* test full copy */
	good = good && sh_vector_duplicate(&iv, &iv2) == 0;
	good = good && sh_vector_duplicate(&sv, &sv2) == 0;
	if (!good)
		goto exit_bad_full_duplicate;

	good = good && test_idata(&iv2, idata, idata_count) == 0;
	good = good && test_sdata(&sv2, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_full;

	goto exit_cleanup;
exit_bad_empty_duplicate:
	fprintf(stderr, "duplicate: bad duplication of empty vector\n");
	goto exit_fail;
exit_test_empty:
	fprintf(stderr, "duplicate: duplicated empty array data mismatch\n");
	goto exit_fail;
exit_bad_full_duplicate:
	fprintf(stderr, "duplicate: bad duplication of non-empty vector\n");
	goto exit_fail;
exit_test_full:
	fprintf(stderr, "duplicate: duplicated full array data mismatch\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
exit_cleanup:
	sh_vector_free(&iv1);
	sh_vector_free(&sv1);
	sh_vector_free(&iv2);
	sh_vector_free(&sv2);

	return good?0:-1;
}

int test_minimalize(void)
{
	bool good = true;

	sh_vector_minimalize(&iv);
	sh_vector_minimalize(&sv);
	good = good && test_idata(&iv, idata, idata_count) == 0;
	good = good && test_sdata(&sv, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_test_fail:
	dump_data(DUMP_FILE);
	fprintf(stderr, "minimalize: mismatch between real data and those in structure\n");
	return -1;
}

int concat_and_test(shVector *iv1, shVector *iv2, shVector *sv1, shVector *sv2, int *id, size_t in, char **sd, size_t sn)
{
	bool good = true;

	good = good && sh_vector_concat(iv1, iv2) == 0;
	good = good && sh_vector_concat(sv1, sv2) == 0;
	if (!good)
		return -2;
	good = good && test_idata(iv1, id, in) == 0;
	good = good && test_sdata(sv1, sd, sn) == 0;
	if (!good)
		return -1;

	return 0;
}

int test_concat(void)
{
	shVector iv1, iv2, iv3, iv4;
	shVector sv1, sv2, sv3, sv4;
	shVector itemp, stemp;
	int ret;
	size_t i;
	int *ext_idata = NULL;
	size_t ext_idata_count;
	char **ext_sdata = NULL;
	size_t ext_sdata_count;
	bool good = true;

	sh_vector_init(&iv1, _intptrcpy, sizeof(int *));
	sh_vector_init(&iv2, _intptrcpy, sizeof(int *));
	sh_vector_init(&iv3, _intptrcpy, sizeof(int *));
	sh_vector_init(&iv4, _intptrcpy, sizeof(int *));
	sh_vector_init(&sv1, _charptrcpy, sizeof(char *));
	sh_vector_init(&sv2, _charptrcpy, sizeof(char *));
	sh_vector_init(&sv3, _charptrcpy, sizeof(char *));
	sh_vector_init(&sv4, _charptrcpy, sizeof(char *));
	sh_vector_init(&itemp, _intptrcpy, sizeof(int *));
	sh_vector_init(&stemp, _charptrcpy, sizeof(char *));

	/* {} + {} */
	ret = concat_and_test(&iv1, &iv1, &sv1, &sv1, NULL, 0, NULL, 0);
	if (ret == -2)
		goto exit_no_mem;
	else if (ret == -1)
		goto exit_test_fail_both_empty;

	/* v + {} */
	ret = concat_and_test(&iv, &iv1, &sv, &sv1, idata, idata_count, sdata, sdata_count);
	if (ret == -2)
		goto exit_no_mem;
	else if (ret == -1)
		goto exit_test_fail_empty_right;

	/* {} + v */
	ret = concat_and_test(&iv2, &iv, &sv2, &sv, idata, idata_count, sdata, sdata_count);
	if (ret == -2)
		goto exit_no_mem;
	else if (ret == -1)
		goto exit_test_fail_empty_left;

	/* v + v/2 */
	ext_idata_count = idata_count / 2;
	ext_sdata_count = sdata_count / 2;
	ext_idata = malloc((idata_count + ext_idata_count) * sizeof *ext_idata);
	ext_sdata = malloc((sdata_count + ext_sdata_count) * sizeof *ext_sdata);
	if (ext_idata == NULL || ext_sdata == NULL)
		goto exit_no_mem;

	memcpy(ext_idata, idata, idata_count * sizeof *idata);
	memcpy(ext_sdata, sdata, sdata_count * sizeof *sdata);
	for (i = 0; i < ext_idata_count; ++i)
	{
		ext_idata[i + idata_count] = idata[i];
		sh_vector_append(&iv3, &idata[i]);
	}
	for (i = 0; i < ext_sdata_count; ++i)
	{
		ext_sdata[i + sdata_count] = sdata[i];
		sh_vector_append(&sv3, sdata[i]);
	}
	sh_vector_concat(&itemp, &iv);
	sh_vector_concat(&stemp, &sv);
	ret = concat_and_test(&itemp, &iv3, &stemp, &sv3, ext_idata, idata_count + ext_idata_count, ext_sdata, sdata_count + ext_sdata_count);
	if (ret == -2)
		goto exit_no_mem;
	else if (ret == -1)
		goto exit_test_fail_smaller_right;

	/* v/2 + v */
	memcpy(ext_idata + ext_idata_count, idata, idata_count * sizeof *idata);
	memcpy(ext_sdata + ext_sdata_count, sdata, sdata_count * sizeof *sdata);
	for (i = 0; i < ext_idata_count; ++i)
	{
		ext_idata[i] = idata[i];
		sh_vector_append(&iv4, &idata[i]);
	}
	for (i = 0; i < ext_sdata_count; ++i)
	{
		ext_sdata[i] = sdata[i];
		sh_vector_append(&sv4, sdata[i]);
	}
	ret = concat_and_test(&iv4, &iv, &sv4, &sv, ext_idata, idata_count + ext_idata_count, ext_sdata, sdata_count + ext_sdata_count);
	if (ret == -2)
		goto exit_no_mem;
	else if (ret == -1)
		goto exit_test_fail_smaller_left;

	/* v/2 + v/2 */
	memcpy(ext_idata + ext_idata_count, ext_idata, ext_idata_count * sizeof *ext_idata);
	memcpy(ext_sdata + ext_sdata_count, ext_sdata, ext_sdata_count * sizeof *ext_sdata);
	ret = concat_and_test(&iv3, &iv3, &sv3, &sv3, ext_idata, ext_idata_count * 2, ext_sdata, ext_sdata_count * 2);
	if (ret == -2)
		goto exit_no_mem;
	else if (ret == -1)
		goto exit_test_fail_duplicate;
	good = true;

	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "concat: out of memory\n");
	goto exit_fail;
exit_test_fail_empty_right:
	fprintf(stderr, "concat: bad concatenation with empty vector on the right\n");
	goto exit_fail;
exit_test_fail_empty_left:
	fprintf(stderr, "concat: bad concatenation with empty vector on the left\n");
	goto exit_fail;
exit_test_fail_smaller_right:
	fprintf(stderr, "concat: bad concatenation with smaller vector on the right\n");
	goto exit_fail;
exit_test_fail_smaller_left:
	fprintf(stderr, "concat: bad concatenation with smaller vector on the left\n");
	goto exit_fail;
exit_test_fail_both_empty:
	fprintf(stderr, "concat: bad concatenation with two empty vectors\n");
	goto exit_fail;
exit_test_fail_duplicate:
	fprintf(stderr, "concat: bad concatenation with itself\n");
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_vector_free(&iv1);
	sh_vector_free(&iv2);
	sh_vector_free(&iv3);
	sh_vector_free(&iv4);
	sh_vector_free(&sv1);
	sh_vector_free(&sv2);
	sh_vector_free(&sv3);
	sh_vector_free(&sv4);
	sh_vector_free(&itemp);
	sh_vector_free(&stemp);
	free(ext_idata);
	free(ext_sdata);
	return good?0:-1;
}

int test_concat_array(void)
{
	shVector iv1, sv1;
	bool good = true;
	void **idata2 = NULL, **sdata2 = NULL;
	size_t i;

	sh_vector_init(&iv1, _intptrcpy, sizeof(int *));
	sh_vector_init(&sv1, _charptrcpy, sizeof(char *));
	idata2 = malloc(idata_count * sizeof *idata2);
	sdata2 = malloc(sdata_count * sizeof *sdata2);
	if (idata2 == NULL || sdata2 == NULL)
		goto exit_no_mem;
	for (i = 0; i < idata_count; ++i)
		idata2[i] = &idata[i];
	for (i = 0; i < sdata_count; ++i)
		sdata2[i] = sdata[i];

	/* {} + {} */
	good = good && sh_vector_concat_array(&iv1, NULL, 0) == 0;
	good = good && sh_vector_concat_array(&sv1, NULL, 0) == 0;
	if (!good)
		goto exit_bad_concat_array;
	good = good && test_idata(&iv1, idata, 0) == 0;
	good = good && test_sdata(&sv1, sdata, 0) == 0;
	if (!good)
		goto exit_test_fail_both_empty;

	/* {} + {X} */
	good = good && sh_vector_concat_array(&iv1, idata2, 1) == 0;
	good = good && sh_vector_concat_array(&sv1, sdata2, 1) == 0;
	if (!good)
		goto exit_bad_concat_array;
	good = good && test_idata(&iv1, idata, 1) == 0;
	good = good && test_sdata(&sv1, sdata, 1) == 0;
	if (!good)
		goto exit_test_fail_empty_left_one_right;

	/* {X} + {X} */
	good = good && sh_vector_concat_array(&iv1, idata2 + 1, 1) == 0;
	good = good && sh_vector_concat_array(&sv1, sdata2 + 1, 1) == 0;
	if (!good)
		goto exit_bad_concat_array;
	good = good && test_idata(&iv1, idata, 2) == 0;
	good = good && test_sdata(&sv1, sdata, 2) == 0;
	if (!good)
		goto exit_test_fail_both_one;

	/* {} + v/2 */
	sh_vector_clear(&iv1);
	sh_vector_clear(&sv1);
	good = good && sh_vector_concat_array(&iv1, idata2, idata_count / 2) == 0;
	good = good && sh_vector_concat_array(&sv1, sdata2, sdata_count / 2) == 0;
	if (!good)
		goto exit_bad_concat_array;
	good = good && test_idata(&iv1, idata, idata_count / 2) == 0;
	good = good && test_sdata(&sv1, sdata, sdata_count / 2) == 0;
	if (!good)
		goto exit_test_fail_empty_left;

	/* v + {} */
	good = good && sh_vector_concat_array(&iv1, idata2, 0) == 0;
	good = good && sh_vector_concat_array(&sv1, sdata2, 0) == 0;
	if (!good)
		goto exit_bad_concat_array;
	good = good && test_idata(&iv1, idata, idata_count / 2) == 0;
	good = good && test_sdata(&sv1, sdata, sdata_count / 2) == 0;
	if (!good)
		goto exit_test_fail_empty_right;

	/* v + v */
	good = good && sh_vector_concat_array(&iv1, idata2 + idata_count / 2, idata_count - idata_count / 2) == 0;
	good = good && sh_vector_concat_array(&sv1, sdata2 + sdata_count / 2, sdata_count - sdata_count / 2) == 0;
	if (!good)
		goto exit_bad_concat_array;
	good = good && test_idata(&iv1, idata, idata_count) == 0;
	good = good && test_sdata(&sv1, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_fail;

	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "concat array: out of memory\n");
	goto exit_fail;
exit_bad_concat_array:
	fprintf(stderr, "concat array: bad concat array\n");
	goto exit_fail;
exit_test_fail_empty_right:
	fprintf(stderr, "concat array: bad concatenation with empty vector on the right\n");
	goto exit_fail;
exit_test_fail_empty_left:
	fprintf(stderr, "concat array: bad concatenation with empty array on the left\n");
	goto exit_fail;
exit_test_fail_empty_left_one_right:
	fprintf(stderr, "concat array: bad concatenation with empty array on the left and one element on right\n");
	goto exit_fail;
exit_test_fail_both_empty:
	fprintf(stderr, "concat array: bad concatenation with empty vector and empty array\n");
	goto exit_fail;
exit_test_fail_both_one:
	fprintf(stderr, "concat array: bad concatenation with single-element array on the left and one element on right\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "concat array: bad concatenation with non-empty vector and array\n");
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_vector_free(&iv1);
	sh_vector_free(&sv1);
	free(idata2);
	free(sdata2);
	return good?0:-1;
}

int test_insert(void)
{
	bool good = true;
	size_t i;
	int *idata2 = NULL;
	char **sdata2 = NULL;
	size_t remove_index;
	size_t iv2_size, sv2_size;
	shVector iv1, sv1, iv2, sv2;

	sh_vector_init(&iv1, _intptrcpy, sizeof(int *), ipcmp);
	sh_vector_init(&sv1, _charptrcpy, sizeof(char *), scmp);
	sh_vector_init(&iv2, _intptrcpy, sizeof(int *), ipcmp);
	sh_vector_init(&sv2, _charptrcpy, sizeof(char *), scmp);
	idata2 = malloc(idata_count * sizeof *idata2);
	sdata2 = malloc(sdata_count * sizeof *sdata2);
	if (idata2 == NULL || sdata2 == NULL)
		goto exit_no_mem;

	memcpy(idata2, idata, idata_count / 8 * sizeof *idata2);
	memcpy(sdata2, sdata, sdata_count / 8 * sizeof *sdata2);
	for (i = 0; i < idata_count / 8; ++i)
		if (sh_vector_insert_sorted(&iv1, &idata[i], false))
			goto exit_bad_insert;
	for (i = 0; i < sdata_count / 8; ++i)
		if (sh_vector_insert_sorted(&sv1, sdata[i], false))
			goto exit_bad_insert;
	qsort(idata2, idata_count / 8, sizeof *idata2, icmp);
	qsort(sdata2, sdata_count / 8, sizeof *sdata2, scmp);
	good = good && test_idata(&iv1, idata2, idata_count / 8) == 0;
	good = good && test_sdata(&sv1, sdata2, sdata_count / 8) == 0;
	if (!good)
		goto exit_test_fail;

	for (i = 1; i < idata_count / 8; ++i)
		if (**(int **)sh_vector_at(&iv1, i - 1) > **(int **)sh_vector_at(&iv1, i))
			goto exit_not_sorted;
	for (i = 1; i < sdata_count / 8; ++i)
		if (strcmp(*(char **)sh_vector_at(&sv1, i - 1), *(char **)sh_vector_at(&sv1, i)) > 0)
			goto exit_not_sorted;

	remove_index = 0;
	for (i = 1; i < idata_count / 8; ++i)
		if (idata2[i - 1] == idata2[i])
			++remove_index;
		else
			idata2[i - remove_index] = idata2[i];
	iv2_size = idata_count / 8 - remove_index;
	remove_index = 0;
	for (i = 1; i < sdata_count / 8; ++i)
		if (strcmp(sdata2[i - 1], sdata2[i]) == 0)
			++remove_index;
		else
			sdata2[i - remove_index] = sdata2[i];
	sv2_size = sdata_count / 8 - remove_index;
	for (i = 0; i < idata_count / 8; ++i)
		sh_vector_insert_sorted(&iv2, &idata[i], true);
	for (i = 0; i < sdata_count / 8; ++i)
		sh_vector_insert_sorted(&sv2, sdata[i], true);
	good = good && test_idata(&iv2, idata2, iv2_size) == 0;
	good = good && test_sdata(&sv2, sdata2, sv2_size) == 0;
	if (!good)
		goto exit_test_fail_unique;

	goto exit_cleanup;
exit_bad_insert:
	fprintf(stderr, "insert_sorted: insert error (not unique)\n");
	goto exit_fail;
exit_not_sorted:
	fprintf(stderr, "insert_sorted: not ascending\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "insert_sorted: bad insert (not unique)\n");
	goto exit_fail;
exit_test_fail_unique:
	fprintf(stderr, "insert_sorted: bad insert (unique)\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "insert_sorted: out of memory\n");
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	free(idata2);
	free(sdata2);
	sh_vector_free(&iv1);
	sh_vector_free(&sv1);
	sh_vector_free(&iv2);
	sh_vector_free(&sv2);
	return good?0:-1;
}

int test_sort(void)
{
	bool good = true;
	size_t i;

	qsort(idata, idata_count, sizeof *idata, icmp);
	qsort(sdata, sdata_count, sizeof *sdata, scmp);
	sh_vector_sort(&iv);
	sh_vector_sort(&sv);
	good = good && test_idata(&iv, idata, idata_count) == 0;
	good = good && test_sdata(&sv, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_fail;
	for (i = 1; i < idata_count; ++i)
		if (**(int **)sh_vector_at(&iv, i - 1) > **(int **)sh_vector_at(&iv, i))
			goto exit_not_sorted;
	for (i = 1; i < sdata_count; ++i)
		if (strcmp(*(char **)sh_vector_at(&sv, i - 1), *(char **)sh_vector_at(&sv, i)) > 0)
			goto exit_not_sorted;

	return 0;
exit_not_sorted:
	fprintf(stderr, "sort: not ascending\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "sort: bad sort\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_bsearch(void)
{
	size_t i;
	int *iq;
	const char *sq;
	int index;
	int itemp;

	for (i = 0; i < idata_count; ++i)
	{
		iq = &idata[i];
		index = sh_vector_bsearch(&iv, &iq);
		if (index == -1)
			goto exit_not_found;
		if (idata[index] != **(int **)sh_vector_at(&iv, index))
			goto exit_bad_find;
	}
	for (i = 0; i < sdata_count; ++i)
	{
		sq = sdata[i];
		index = sh_vector_bsearch(&sv, &sq);
		if (index == -1)
			goto exit_not_found;
		if (strcmp(sdata[index], *(char **)sh_vector_at(&sv, index)) != 0)
			goto exit_bad_find;
	}
	iq = &itemp;
	itemp = -1;
	if (sh_vector_bsearch(&iv, &iq) != -1)
		goto exit_extra_found;
	itemp = INT_RANGE;
	if (sh_vector_bsearch(&iv, &iq) != -1)
		goto exit_extra_found;
	itemp = INT_RANGE + 10;
	if (sh_vector_bsearch(&iv, &iq) != -1)
		goto exit_extra_found;
	sq = "";
	if (sh_vector_bsearch(&sv, &sq) != -1)
		goto exit_extra_found;
	sq = "z";
	if (sh_vector_bsearch(&sv, &sq) != -1)
		goto exit_extra_found;
	sq = "Az";
	if (sh_vector_bsearch(&sv, &sq) != -1)
		goto exit_extra_found;
	sq = "abababababa";
	if (sh_vector_bsearch(&sv, &sq) != -1)
		goto exit_extra_found;
	sq = "abbaecbabde";
	if (sh_vector_bsearch(&sv, &sq) != -1)
		goto exit_extra_found;

	return 0;
exit_not_found:
	fprintf(stderr, "binary search: didn't find an existing element\n");
	goto exit_fail;
exit_bad_find:
	fprintf(stderr, "binary search: found at wrong index\n");
	goto exit_fail;
exit_extra_found:
	fprintf(stderr, "binary search: found non existing element\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_compare(void)
{
	shVector iv1;
	shVector sv1;
	bool good = true;

	sh_vector_init(&iv1, _intptrcpy, sizeof(int *), ipcmp);
	sh_vector_init(&sv1, _charptrcpy, sizeof(char *), scmp);

	if (sh_vector_compare(&iv, &iv) != 0 || sh_vector_compare(&sv, &sv) != 0
		|| sh_vector_compare_lexic(&iv, &iv) != 0 || sh_vector_compare_lexic(&sv, &sv) != 0)
		goto exit_bad_equal;

	if (sh_vector_concat(&iv1, &iv) || sh_vector_concat(&sv1, &sv))
		goto exit_no_mem;
	if (sh_vector_append(&iv1, &idata[0]) || sh_vector_append(&sv1, sdata[0]))
		goto exit_no_mem;

	if (sh_vector_compare(&iv, &iv1) == 0 || sh_vector_compare(&sv, &sv1) == 0)
		goto exit_bad_inequal;
	if (sh_vector_compare_lexic(&iv, &iv1) != -1 || sh_vector_compare_lexic(&iv1, &iv) != 1
		|| sh_vector_compare_lexic(&sv, &sv1) != -1 || sh_vector_compare_lexic(&sv1, &sv) != 1)
		goto exit_bad_lexic;

	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "compare: out of memory\n");
	goto exit_fail;
exit_bad_equal:
	fprintf(stderr, "compare: failed to recognize equal vectors\n");
	goto exit_fail;
exit_bad_inequal:
	fprintf(stderr, "compare: failed to recognize inequal vectors\n");
	goto exit_fail;
exit_bad_lexic:
	fprintf(stderr, "compare: incorrect lexicographical comparison\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_vector_free(&iv1);
	sh_vector_free(&sv1);
	return good?0:-1;
}

int test_iteration(void)
{
	shNode c;
	size_t i;

	sh_vector_get_first(&iv, &c);
	if (!sh_vector_get_prev(&c, &c))
		goto exit_first_has_prev;

	sh_vector_get_last(&iv, &c);
	if (!sh_vector_get_next(&c, &c))
		goto exit_last_has_next;

	for (sh_vector_get_first(&iv, &c), i = 0; i < iv.size; ++i)
	{
		shNode p, n;

		if (!sh_vector_get_prev(&c, &p) && (sh_vector_get_next(&p, &n) || n.data != c.data))
			goto exit_bad_iteration;
		if (!sh_vector_get_next(&c, &n) && (sh_vector_get_prev(&n, &p) || p.data != c.data))
			goto exit_bad_iteration;

		if (c.data != sh_vector_at(&iv, i))
			goto exit_mismatch;
		if (sh_vector_get_next(&c, &c) && i + 1 != iv.size)
			goto exit_early_end;
	}

	for (sh_vector_get_last(&sv, &c), i = 0; i < sv.size; ++i)
	{
		shNode p, n;

		if (!sh_vector_get_prev(&c, &p) && (sh_vector_get_next(&p, &n) || n.data != c.data))
			goto exit_bad_iteration;
		if (!sh_vector_get_next(&c, &n) && (sh_vector_get_prev(&n, &p) || p.data != c.data))
			goto exit_bad_iteration;

		if (c.data != sh_vector_at(&sv, sv.size - 1 - i))
			goto exit_mismatch;
		if (sh_vector_get_prev(&c, &c) && i + 1 != sv.size)
			goto exit_early_start;
	}

	return 0;
exit_first_has_prev:
	fprintf(stderr, "iterations: first has prev\n");
	goto exit_fail;
exit_last_has_next:
	fprintf(stderr, "iterations: last has next\n");
	goto exit_fail;
exit_early_end:
	fprintf(stderr, "iterations: get next finished too early\n");
	goto exit_fail;
exit_early_start:
	fprintf(stderr, "iterations: get prev finished too early\n");
	goto exit_fail;
exit_mismatch:
	fprintf(stderr, "iterations: mismatch between pointers of shNode's and actual data\n");
	goto exit_fail;
exit_bad_iteration:
	fprintf(stderr, "iterations: mismatch between node and node->prev->next or node->next->prev\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_remove_last(void)
{
	size_t i;

	for (i = 0; i < iv.size; ++i)
	{
		if (sh_vector_remove_last(&iv))
			goto exit_bad_remove;

		/* checking all the cases takes too long */
		if (i % 689 == 0 || i == iv.size - 1)
			if (test_idata(&iv, idata, idata_count - i - 1))
				goto exit_remove_corrupts;
	}
	for (i = 0; i < sv.size; ++i)
	{
		if (sh_vector_remove_last(&sv))
			goto exit_bad_remove;

		/* checking all the cases takes too long */
		if (i % 954 == 0 || i == sv.size - 1)
			if (test_sdata(&sv, sdata, sdata_count - i - 1))
				goto exit_remove_corrupts;
	}

	/* rewrite the data in the arrays for clear to be tested */
	if (test_init() != 0)
		goto exit_bad_init;

	return 0;
exit_bad_remove:
	fprintf(stderr, "remove last: remove failed\n");
	goto exit_fail;
exit_remove_corrupts:
	fprintf(stderr, "remove last: remove corrupted data\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
exit_bad_init:
	return -1;
}

int test_clear(void)
{
	bool good = true;
	int i = 10;
	char s[] = "abfda";

	sh_vector_clear(&iv);
	sh_vector_clear(&sv);
	if (iv.size != 0 || sv.size != 0)
		goto exit_test_fail;
	good = good && sh_vector_append(&iv, &i) == 0;
	good = good && sh_vector_append(&sv, s) == 0;
	if (!good)
		goto exit_test_fail;
	sh_vector_clear(&iv);
	sh_vector_clear(&sv);
	good = good && sh_vector_init(&iv, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_vector_init(&sv, _charptrcpy, sizeof(char *)) == 0;
	if (!good)
		goto exit_not_reinitializable;

	return 0;
exit_test_fail:
	fprintf(stderr, "clear: clear did not return object to a usable state\n");
	goto exit_fail;
exit_not_reinitializable:
	fprintf(stderr, "clear: clear did not return object to a reinitializable state\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_free(void)
{
	sh_vector_free(&iv);
	sh_vector_free(&sv);
	/* multiple frees should not cause a problem */
	sh_vector_free(&iv);
	sh_vector_free(&sv);
	sh_vector_free(&iv);
	sh_vector_free(&sv);

	return 0;
}

int main(int argc, char **argv)
{
	bool good = true;
	srand(time(NULL));

	if (argc < 2)
	{
		if (generate_data(RANDOM_SIZE))
			goto exit_no_mem;
	}
	else
	{
		if (read_data(argv[1]))
			goto exit_bad_input;
	}

	good = good && test_init() == 0;
	good = good && test_append() == 0;
	good = good && test_duplicate() == 0;
	good = good && test_minimalize() == 0;
	good = good && test_concat() == 0;
	good = good && test_concat_array() == 0;
	good = good && test_insert() == 0;
	good = good && test_sort() == 0;
	good = good && test_bsearch() == 0;
	good = good && test_compare() == 0;
	good = good && test_iteration() == 0;
	good = good && test_remove_last() == 0;
	good = good && test_clear() == 0;
	good = good && test_free() == 0;

	cleanup_data();

	return good?EXIT_SUCCESS:EXIT_FAILURE;
exit_bad_input:
exit_no_mem:
	cleanup_data();
	return EXIT_FAILURE;
}
