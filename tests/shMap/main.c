/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <shds.h>
#include "io.h"

#define DUMP_FILE "error_case.out"

int *idata = NULL;
size_t idata_count = 0;
char **sdata = NULL;
size_t sdata_count = 0;

/*
 * im: map of int i.e. copies of idata[.] to char * i.e. copies of sdata[.]
 * sm: map of char * i.e. copies of sdata[.] to int i.e. copies of idata[.]
 */
shMap im = {0}, sm = {0};

static void _intcpy(void *to, void *from)
{
	*(int *)to = *(int *)from;
}

static void _charptrcpy(void *to, void *from)
{
	*(char **)to = (char *)from;
}

int icmp(const void *i1, const void *i2)
{
	int i = *(const int *)i1;
	int j = *(const int *)i2;

	if (i == j)
		return 0;
	if (i < j)
		return -1;

	return 1;
}

int scmp(const void *s1, const void *s2)
{
	return strcmp(*(char * const *)s1, *(char * const *)s2);
}

typedef struct imap_content
{
	int key;
	char *data;
} imap_content;

typedef struct smap_content
{
	char *key;
	int data;
} smap_content;

int imccmp(const void *i1, const void *i2)
{
	int i = ((const imap_content *)i1)->key;
	int j = ((const imap_content *)i2)->key;

	if (i == j)
		return 0;
	if (i < j)
		return -1;

	return 1;
}

int smccmp(const void *s1, const void *s2)
{
	return strcmp(((const smap_content *)s1)->key, ((const smap_content *)s2)->key);
}

imap_content *imc = NULL;
size_t imc_count = 0;
smap_content *smc = NULL;
size_t smc_count = 0;

/* initialize structure */
int test_init(void)
{
	bool good = true;

	sh_map_free(&im);
	sh_map_free(&sm);
	good = good && sh_map_init(&im, _intcpy, sizeof(int), _charptrcpy, sizeof(char *), icmp) == 0;
	good = good && sh_map_init(&sm, _charptrcpy, sizeof(char *), _intcpy, sizeof(int), scmp) == 0;
	/* it's ok to reinitialize with these attributes */
	good = good && sh_map_init(&im, _intcpy, sizeof(int), _charptrcpy, sizeof(char *), icmp) == 0;
	good = good && sh_map_init(&sm, _charptrcpy, sizeof(char *), _intcpy, sizeof(int), scmp) == 0;
	good = good && sh_map_init(&im, _intcpy, sizeof(int), _charptrcpy, sizeof(char *), icmp) == 0;
	good = good && sh_map_init(&sm, _charptrcpy, sizeof(char *), _intcpy, sizeof(int), scmp) == 0;
	good = good && sh_map_init(&im, _intcpy, sizeof(int), _charptrcpy, sizeof(char *), icmp) == 0;
	good = good && sh_map_init(&sm, _charptrcpy, sizeof(char *), _intcpy, sizeof(int), scmp) == 0;

	if (!good)
		dump_data(DUMP_FILE);

	return good?0:-1;
}

typedef struct test_append_data
{
	void *array;
	size_t size;
} test_append_data;

bool test_idata_append(shNode *n, void *extra)
{
	imap_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	array[size].key = *(int *)n->key;
	array[size].data = *(char **)n->data;
	++((test_append_data *)extra)->size;

	return true;
}

bool test_sdata_append(shNode *n, void *extra)
{
	smap_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	array[size].key = *(char **)n->key;
	array[size].data = *(int *)n->data;
	++((test_append_data *)extra)->size;

	return true;
}

size_t append_size_i = 0;
size_t append_size_s = 0;
int visited_at_false = 0;

bool test_idata_append_half(shNode *n, void *extra)
{
	imap_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	if (size >= append_size_i)
	{
		++visited_at_false;
		return false;
	}

	array[size].key = *(int *)n->key;
	array[size].data = *(char **)n->data;
	++((test_append_data *)extra)->size;

	return true;
}

bool test_sdata_append_half(shNode *n, void *extra)
{
	smap_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	if (size >= append_size_s)
	{
		++visited_at_false;
		return false;
	}

	array[size].key = *(char **)n->key;
	array[size].data = *(int *)n->data;
	++((test_append_data *)extra)->size;

	return true;
}

/*
 * test whether structure data match the provided data (for integer test)
 * d is the expected results and n is its count
 */
int test_idata(shMap *m, imap_content *d, size_t n, int (*traverse)(shMap *, sh_callback, void *, ...), bool needs_sort)
{
	size_t i;
	test_append_data ad;
	imap_content *imc2;

	if (m->size != n)
		return -1;
	imc2 = malloc(n * sizeof *imc2);
	if (imc2 == NULL)
		goto exit_no_mem;

	ad.array = imc2;
	ad.size = 0;
	if (traverse(m, test_idata_append, &ad) != 0)
		goto exit_bad_full_traverse;
	if (ad.size != n)
		goto exit_fail;
	if (needs_sort)
		qsort(imc2, n, sizeof *imc2, imccmp);
	for (i = 0; i < n; ++i)
		if (d[i].key != imc2[i].key || strcmp(d[i].data, imc2[i].data) != 0)
			goto exit_fail;

	/* if it needs sort, it doesn't make sense to only read half the array */
	if (!needs_sort)
	{
		visited_at_false = 0;
		ad.size = 0;
		if (traverse(m, test_idata_append_half, &ad) == 0 && ad.size >= append_size_i)
			goto exit_bad_half_traverse;
		if (append_size_i > n)
			append_size_i = n;
		for (i = 0; i < append_size_i; ++i)
			if (d[i].key != imc2[i].key || strcmp(d[i].data, imc2[i].data) != 0)
				goto exit_fail;

		/* this is done after, so it would affect next test */
		append_size_i = n / 2;
		if (visited_at_false > 1)
			goto exit_fail;
	}
	free(imc2);
	return 0;
exit_bad_full_traverse:
	fprintf(stderr, "test_idata for each returned nonzero for full traversal\n");
	goto exit_fail;
exit_bad_half_traverse:
	fprintf(stderr, "test_idata for each returned zero for incomplete traversal\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
exit_fail:
	free(imc2);
	return -1;
}

/*
 * test whether structure data match the provided data (for string test)
 * d is the expected results and n is its count
 */
int test_sdata(shMap *m, smap_content *d, size_t n, int (*traverse)(shMap *, sh_callback, void *, ...), bool needs_sort)
{
	size_t i;
	test_append_data ad;
	smap_content *smc2;

	if (m->size != n)
		return -1;
	smc2 = malloc(n * sizeof *smc2);
	if (smc2 == NULL)
		goto exit_no_mem;

	ad.array = smc2;
	ad.size = 0;
	if (traverse(m, test_sdata_append, &ad) != 0)
		goto exit_bad_full_traverse;
	if (ad.size != n)
		goto exit_fail;
	if (needs_sort)
		qsort(smc2, n, sizeof *smc2, smccmp);
	for (i = 0; i < n; ++i)
		if (strcmp(d[i].key, smc2[i].key) != 0 || d[i].data != smc2[i].data)
			goto exit_fail;

	/* if it needs sort, it doesn't make sense to only read half the array */
	if (!needs_sort)
	{
		visited_at_false = 0;
		ad.size = 0;
		if (traverse(m, test_sdata_append_half, &ad) == 0 && ad.size >= append_size_s)
			goto exit_bad_half_traverse;
		if (append_size_s > n)
			append_size_s = n;
		qsort(smc2, append_size_s, sizeof *smc2, smccmp);
		for (i = 0; i < append_size_s && i < n; ++i)
			if (strcmp(d[i].key, smc2[i].key) != 0 || d[i].data != smc2[i].data)
				goto exit_fail;

		/* this is done after, so it would affect next test */
		append_size_s = n / 2;
		if (visited_at_false > 1)
			goto exit_fail;
	}
	free(smc2);
	return 0;
exit_bad_full_traverse:
	fprintf(stderr, "test_sdata for each returned nonzero for full traversal\n");
	goto exit_fail;
exit_bad_half_traverse:
	fprintf(stderr, "test_sdata for each returned zero for incomplete traversal\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
exit_fail:
	free(smc2);
	return -1;
}

int test_insert_exists_find_and_replace(void)
{
	size_t i;
	bool good = true;

	for (i = 0; i < idata_count; ++i)
	{
		shNode n;

		if (sh_map_exists(&im, &idata[i]))
		{
			size_t in_imc;

			if (!sh_map_find(&im, &idata[i], &n))
				goto exit_exist_find_mismatch;
			if (*(int *)n.key != idata[i])
				goto exit_bad_find;
			if (sh_map_insert(&im, &idata[i], sdata[i]) != 1)
				goto exit_bad_insert;
			for (in_imc = 0; in_imc < imc_count; ++in_imc)
				if (imc[in_imc].key == idata[i])
					break;
			if (in_imc == imc_count && imc_count > 0)
				goto exit_internal_error;	/* shouldn't happen */
			if (strcmp(*(char **)n.data, imc[in_imc].data) != 0)
				goto exit_bad_insert;
			if (sh_map_replace(&im, &idata[i], sdata[i]))
				goto exit_bad_replace;
			if (!sh_map_find(&im, &idata[i], &n))
				goto exit_exist_find_mismatch;
			if (strcmp(*(char **)n.data, sdata[i]) != 0)
				goto exit_bad_replace;
			imc[in_imc].data = sdata[i];
		}
		else
		{
			if (sh_map_find(&im, &idata[i], &n))
				goto exit_exist_find_mismatch;
			if (sh_map_insert(&im, &idata[i], sdata[i]))
				goto exit_bad_insert;
			if (!sh_map_exists(&im, &idata[i]))
				goto exit_bad_exists;
			imc[imc_count].key = idata[i];
			imc[imc_count++].data = sdata[i];
		}
	}
	for (i = 0; i < sdata_count; ++i)
	{
		shNode n;

		if (sh_map_exists(&sm, &sdata[i]))
		{
			size_t in_smc;

			if (!sh_map_find(&sm, &sdata[i], &n))
				goto exit_exist_find_mismatch;
			if (strcmp(*(char **)n.key, sdata[i]) != 0)
				goto exit_bad_find;
			if (sh_map_insert(&sm, sdata[i], &idata[i]) != 1)
				goto exit_bad_insert;
			for (in_smc = 0; in_smc < smc_count; ++in_smc)
				if (strcmp(smc[in_smc].key, sdata[i]) == 0)
					break;
			if (in_smc == smc_count && smc_count > 0)
				goto exit_internal_error;	/* shouldn't happen */
			if (*(int *)n.data != smc[in_smc].data)
				goto exit_bad_insert;
			if (sh_map_replace(&sm, sdata[i], &idata[i], &n))
				goto exit_bad_replace;
			if (*(int *)n.data != idata[i])
				goto exit_bad_replace;
			smc[in_smc].data = idata[i];
		}
		else
		{
			if (sh_map_find(&sm, &sdata[i], &n))
				goto exit_exist_find_mismatch;
			if (sh_map_insert(&sm, sdata[i], &idata[i]))
				goto exit_bad_insert;
			if (!sh_map_exists(&sm, &sdata[i]))
				goto exit_bad_exists;
			smc[smc_count].key = sdata[i];
			smc[smc_count++].data = idata[i];
		}
	}

	qsort(imc, imc_count, sizeof *imc, imccmp);
	qsort(smc, smc_count, sizeof *smc, smccmp);
	good = good && test_idata(&im, imc, imc_count, sh_map_inorder, false) == 0;
	good = good && test_sdata(&sm, smc, smc_count, sh_map_inorder, false) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_exist_find_mismatch:
	fprintf(stderr, "insert exist find replace: exist and find outputs mismatch\n");
	goto exit_fail;
exit_bad_find:
	fprintf(stderr, "insert exist find replace: find found the wrong node\n");
	goto exit_fail;
exit_internal_error:
	fprintf(stderr, "insert exist find replace: internal test error: bad update of imc or smc\n");
	goto exit_fail;
exit_bad_insert:
	fprintf(stderr, "insert exist find replace: insert failed\n");
	goto exit_fail;
exit_bad_exists:
	fprintf(stderr, "insert exist find replace: exist did not find inserted node\n");
	goto exit_fail;
exit_bad_replace:
	fprintf(stderr, "insert exist find replace: replace could not replace existing node\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "insert exist find replace: mismatch between real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_duplicate(void)
{
	bool good = true;
	shMap im1, sm1, im2, sm2;

	/* test empty copy */
	sh_map_init(&im1, _intcpy, sizeof(int), _charptrcpy, sizeof(char *), icmp);
	sh_map_init(&sm1, _charptrcpy, sizeof(char *), _intcpy, sizeof(int), scmp);
	good = good && sh_map_duplicate(&im1, &im2) == 0;
	good = good && sh_map_duplicate(&sm1, &sm2) == 0;
	if (!good)
		goto exit_bad_empty_duplicate;

	good = good && test_idata(&im2, NULL, 0, sh_map_for_each, false) == 0;
	good = good && test_sdata(&sm2, NULL, 0, sh_map_for_each, false) == 0;
	if (!good)
		goto exit_test_empty;

	sh_map_clear(&im2);
	sh_map_clear(&sm2);

	/* test full copy */
	good = good && sh_map_duplicate(&im, &im2) == 0;
	good = good && sh_map_duplicate(&sm, &sm2) == 0;
	if (!good)
		goto exit_bad_full_duplicate;

	good = good && test_idata(&im2, imc, imc_count, sh_map_inorder, false) == 0;
	good = good && test_sdata(&sm2, smc, smc_count, sh_map_inorder, false) == 0;
	if (!good)
		goto exit_test_full;

	goto exit_cleanup;
exit_bad_empty_duplicate:
	fprintf(stderr, "duplicate: bad duplication of empty map\n");
	goto exit_fail;
exit_test_empty:
	fprintf(stderr, "duplicate: duplicated empty array data mismatch\n");
	goto exit_fail;
exit_bad_full_duplicate:
	fprintf(stderr, "duplicate: bad duplication of non-empty map\n");
	goto exit_fail;
exit_test_full:
	fprintf(stderr, "duplicate: duplicated full array data mismatch\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
exit_cleanup:
	sh_map_free(&im1);
	sh_map_free(&sm1);
	sh_map_free(&im2);
	sh_map_free(&sm2);

	return good?0:-1;
}

int test_for_each(void)
{
	bool good = true;

	good = good && test_idata(&im, imc, imc_count, sh_map_for_each, true) == 0;
	good = good && test_sdata(&sm, smc, smc_count, sh_map_for_each, true) == 0;
	if (!good)
	{
		fprintf(stderr, "for each: mismatch between real data and the iterated ones\n");
		dump_data(DUMP_FILE);
	}

	return good?0:-1;
}

int test_balance(void)
{
#ifndef NDEBUG
	bool good = true;

	good = good && sh_map_internal_balance_test(&im) == 0;
	good = good && sh_map_internal_balance_test(&sm) == 0;
	if (!good)
	{
		fprintf(stderr, "balance test: tree is not correctly balanced\n");
		dump_data(DUMP_FILE);
	}

	return good?0:-1;
#else
	return 0;
#endif
}

int test_iteration(void)
{
	shNode c;
	size_t i;
	int i_prev = 0;
	char *s_prev = NULL;

	sh_map_get_first(&im, &c);
	if (!sh_map_get_prev(&c, &c))
		goto exit_first_has_prev;

	sh_map_get_last(&im, &c);
	if (!sh_map_get_next(&c, &c))
		goto exit_last_has_next;

	for (sh_map_get_first(&im, &c), i = 0; i < im.size; ++i)
	{
		shNode p, n;

		if (!sh_map_get_prev(&c, &p) && (sh_map_get_next(&p, &n) || n.key != c.key || n.data != c.data))
			goto exit_bad_iteration;
		if (!sh_map_get_next(&c, &n) && (sh_map_get_prev(&n, &p) || p.key != c.key || p.data != c.data))
			goto exit_bad_iteration;

		if (i > 0 && i_prev > *(int *)c.key)
			goto exit_not_in_order;
		i_prev = *(int *)c.key;
		if (sh_map_get_next(&c, &c) && i + 1 != im.size)
			goto exit_early_end;
	}

	for (sh_map_get_last(&sm, &c), i = 0; i < sm.size; ++i)
	{
		shNode p, n;

		if (!sh_map_get_prev(&c, &p) && (sh_map_get_next(&p, &n) || n.key != c.key || n.data != c.data))
			goto exit_bad_iteration;
		if (!sh_map_get_next(&c, &n) && (sh_map_get_prev(&n, &p) || p.key != c.key || p.data != c.data))
			goto exit_bad_iteration;

		if (i > 0 && strcmp(s_prev, *(char **)c.key) < 0)
			goto exit_not_in_order;
		s_prev = *(char **)c.key;
		if (sh_map_get_prev(&c, &c) && i + 1 != sm.size)
			goto exit_early_start;
	}

	return 0;
exit_first_has_prev:
	fprintf(stderr, "iterations: first has prev\n");
	goto exit_fail;
exit_last_has_next:
	fprintf(stderr, "iterations: last has next\n");
	goto exit_fail;
exit_early_end:
	fprintf(stderr, "iterations: get next finished too early\n");
	goto exit_fail;
exit_early_start:
	fprintf(stderr, "iterations: get prev finished too early\n");
	goto exit_fail;
exit_not_in_order:
	fprintf(stderr, "iterations: iteration not in order\n");
	goto exit_fail;
exit_bad_iteration:
	fprintf(stderr, "iterations: mismatch between node and node->prev->next or node->next->prev\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

static bool _dup_int_map(shNode *n, void *m)
{
	return sh_map_insert(m, n->key, *(char **)n->data) == 0;
}

static bool _dup_str_map(shNode *n, void *m)
{
	return sh_map_insert(m, *(char **)n->key, n->data) == 0;
}

static int duplicate_map(shMap *im1, shMap *im2, shMap *sm1, shMap *sm2)
{
	sh_map_clear(im1);
	sh_map_clear(sm1);

	return sh_map_for_each(im2, _dup_int_map, im1)
		+ sh_map_for_each(sm2, _dup_str_map, sm1);
}

int test_compare(void)
{
	shMap im1;
	shMap sm1;
	bool good = true;
	int newint = INT_RANGE + 10;
	char newstr[] = "zzzzzz";

	sh_map_init(&im1, _intcpy, sizeof(int), _charptrcpy, sizeof(char *), icmp);
	sh_map_init(&sm1, _charptrcpy, sizeof(char *), _intcpy, sizeof(int), scmp);

	if (sh_map_compare(&im, &im) != 0 || sh_map_compare(&sm, &sm) != 0
		|| sh_map_compare_lexic(&im, &im) != 0 || sh_map_compare_lexic(&sm, &sm) != 0)
		goto exit_bad_equal;

	if (duplicate_map(&im1, &im, &sm1, &sm))
		goto exit_no_mem;
	if (sh_map_insert(&im1, &newint, newstr) || sh_map_insert(&sm1, newstr, &newint))
		goto exit_no_mem;

	if (sh_map_compare(&im, &im1) == 0 || sh_map_compare(&sm, &sm1) == 0)
		goto exit_bad_inequal;
	if (sh_map_compare_lexic(&im, &im1) != -1 || sh_map_compare_lexic(&im1, &im) != 1
		|| sh_map_compare_lexic(&sm, &sm1) != -1 || sh_map_compare_lexic(&sm1, &sm) != 1)
		goto exit_bad_lexic;

	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "compare: out of memory\n");
	goto exit_fail;
exit_bad_equal:
	fprintf(stderr, "compare: failed to recognize equal maps\n");
	goto exit_fail;
exit_bad_inequal:
	fprintf(stderr, "compare: failed to recognize inequal maps\n");
	goto exit_fail;
exit_bad_lexic:
	fprintf(stderr, "compare: incorrect lexicographical comparison\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_map_free(&im1);
	sh_map_free(&sm1);
	return good?0:-1;
}

int test_delete(void)
{
	size_t i;

	imap_content *imc2 = malloc(imc_count * sizeof *imc2);
	smap_content *smc2 = malloc(smc_count * sizeof *smc2);
	if (imc2 == NULL || smc2 == NULL)
		goto exit_no_mem;

	/* delete from middle element to first */
	for (i = 0; i < imc_count / 2; ++i)
	{
		if (sh_map_delete_key(&im, &imc[imc_count / 2 - 1 - i].key))
			goto exit_bad_delete_key;

		/* note: checking all the cases takes too long */
		if (i % 924 == 0 || i == imc_count / 2 - 1)
		{
			memcpy(imc2, imc, (imc_count / 2 - 1 - i) * sizeof *imc2);
			memcpy(imc2 + (imc_count / 2 - 1 - i), imc + imc_count / 2, (imc_count - imc_count / 2) * sizeof *imc2);
			if (test_idata(&im, imc2, imc_count - 1 - i, sh_map_inorder, false))
				goto exit_bad_delete;
		}
	}

	/* delete from middle to last */
	memcpy(smc2, smc, smc_count / 2 * sizeof *smc2);
	for (i = 0; i + smc_count / 2 < smc_count; ++i)
	{
		if (sh_map_delete_key(&sm, &smc[smc_count / 2 + i].key))
			goto exit_bad_delete_key;

		/* note: checking all the cases takes too long */
		if (i % 1103 == 0 || i + smc_count / 2 == smc_count - 1)
		{
			memcpy(smc2 + smc_count / 2, smc + smc_count / 2 + i + 1, (smc_count - smc_count / 2 - 1 - i) * sizeof *smc2);
			if (test_sdata(&sm, smc2, smc_count - 1 - i, sh_map_inorder, false))
				goto exit_bad_delete;
		}
	}
	free(imc);
	free(smc);
	imc = imc2;
	smc = smc2;
	imc_count -= imc_count / 2;
	smc_count -= smc_count / 2;
	return 0;
exit_no_mem:
	fprintf(stderr, "delete: out of memory\n");
	goto exit_fail;
exit_bad_delete_key:
	fprintf(stderr, "delete: failed to find key in map\n");
	goto exit_fail;
exit_bad_delete:
	fprintf(stderr, "delete: mismatch between data without deleted key and updated tree\n");
	goto exit_fail;
exit_fail:
	free(imc2);
	free(smc2);
	dump_data(DUMP_FILE);
	return -1;
}

int test_clear(void)
{
	bool good = true;
	int i = 10;
	char *s = "abfda";

	sh_map_clear(&im);
	sh_map_clear(&sm);

	if (im.size != 0 || sm.size != 0)
		goto exit_test_fail;
	good = good && sh_map_insert(&im, &i, s) == 0;
	good = good && sh_map_insert(&sm, s, &i) == 0;
	if (!good)
		goto exit_test_fail;
	sh_map_clear(&im);
	sh_map_clear(&sm);
	good = good && sh_map_init(&im, _intcpy, sizeof(int), _charptrcpy, sizeof(char *), icmp) == 0;
	good = good && sh_map_init(&sm, _charptrcpy, sizeof(char *), _intcpy, sizeof(int), scmp) == 0;
	if (!good)
		goto exit_not_reinitializable;

	return 0;
exit_test_fail:
	fprintf(stderr, "clear: clear did not return object to a usable state\n");
	goto exit_fail;
exit_not_reinitializable:
	fprintf(stderr, "clear: clear did not return object to a reinitializable state\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_free(void)
{
	sh_map_free(&im);
	sh_map_free(&sm);
	/* multiple frees should not cause a problem */
	sh_map_free(&im);
	sh_map_free(&sm);
	sh_map_free(&im);
	sh_map_free(&sm);

	return 0;
}

int main(int argc, char **argv)
{
	bool good = true;

	srand(time(NULL));
	if (argc < 2)
	{
		if (generate_data(EQUAL_SIZE))
			goto exit_no_mem;
	}
	else
	{
		if (read_data(argv[1]))
			goto exit_bad_input;
	}

	imc = malloc(idata_count * sizeof *imc);
	smc = malloc(sdata_count * sizeof *smc);
	if (imc == NULL || smc == NULL)
		goto exit_no_mem;

	good = good && test_init() == 0;
	good = good && test_insert_exists_find_and_replace() == 0;
	good = good && test_duplicate() == 0;
	good = good && test_for_each() == 0;
	good = good && test_balance() == 0;
	good = good && test_iteration() == 0;
	good = good && test_compare() == 0;
	good = good && test_delete() == 0;
	good = good && test_balance() == 0;
	good = good && test_iteration() == 0;
	good = good && test_clear() == 0;
	good = good && test_free() == 0;

	cleanup_data();
	free(imc);
	free(smc);

	return good?EXIT_SUCCESS:EXIT_FAILURE;
exit_bad_input:
exit_no_mem:
	cleanup_data();
	free(imc);
	free(smc);
	return EXIT_FAILURE;
}
