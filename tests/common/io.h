/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IO_H
#define IO_H

#define MAX_ELEMS 10000
#define MIN_ELEMS 5000
#define INT_RANGE 1000
#define STR_MAX 6
#define STR_MIN 1

#define RANDOM_SIZE 0
#define EQUAL_SIZE 1

int generate_data(char equal_size);		/* returns -1 if no memory or 0 otherwise */
int read_data(const char *file);		/* returns -1 if bad input file or 0 otherwise */
void dump_data(const char *file);
void cleanup_data(void);

#endif
