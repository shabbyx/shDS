/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include "read_str.h"

#define READ_STEP 50
#define READ_STEP_STR "50"
#define MORE_STEP_STR "51" /* READ_STEP + 1 */

char *read_str(FILE *fin)
{
        char *result = NULL;
        char *str = NULL;
        int max_length = 0;
        int length = 0;

        while (length >= max_length)
        {
                /* get a larger buffer */
                int new_length = max_length + READ_STEP;
                char *enlarged = realloc(str, (new_length + 1) * sizeof *str);

                if (enlarged == NULL)
                {
                        free(str);
                        return NULL;
                }
                str = enlarged;
                max_length = new_length;

		str[length] = '\0';

		/* if first time, just read from buffer */
                if (length == 0)
                        fscanf(fin, "%"READ_STEP_STR"s", str);
                else
                {
			/* else, unget one character and read/append */
                        ungetc(str[length - 1], stdin);
                        fscanf(fin, "%"MORE_STEP_STR"s", str + length - 1);
                }
                length += strlen(str + length);

                if (feof(fin) || ferror(fin))
                {
                        free(str);
                        return NULL;
                }
        }

        /* strip extra memory */
        result = realloc(str, (max_length + 1) * sizeof *result);
        if (result == NULL)
                return str;

        return result;
}
