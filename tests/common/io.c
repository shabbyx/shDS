/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"
#include "read_str.h"

extern int *idata;
extern size_t idata_count;
extern char **sdata;
extern size_t sdata_count;

int generate_data(char equal_size)
{
	size_t i, j;

	idata_count = rand() % (MAX_ELEMS - MIN_ELEMS + 1) + MIN_ELEMS;
	if (equal_size)
		sdata_count = idata_count;
	else
		sdata_count = rand() % (MAX_ELEMS - MIN_ELEMS + 1) + MIN_ELEMS;

	idata = malloc(idata_count * sizeof *idata);
	sdata = malloc(sdata_count * sizeof *sdata);
	if (idata == NULL || sdata == NULL)
		goto exit_no_mem;

	for (i = 0; i < idata_count; ++i)
		idata[i] = rand() % INT_RANGE;
	memset(sdata, 0, sdata_count * sizeof *sdata);

	for (i = 0; i < sdata_count; ++i)
	{
		size_t len = rand() % (STR_MAX - STR_MIN + 1) + STR_MIN;

		sdata[i] = malloc((len + 1) * sizeof *sdata[i]);
		if (sdata[i] == NULL)
			goto exit_no_mem;

		for (j = 0; j < len; ++j)
			sdata[i][j] = rand() % 5 + (rand() % 2?'a':'A');

		sdata[i][len] = '\0';
	}

	return 0;
exit_no_mem:
	printf("out of memory\n");
	cleanup_data();
	return -1;
}

int read_data(const char *file)
{
	size_t i;
	FILE *fin = fopen(file, "r");
	if (fin == NULL)
		goto exit_no_file;

	fscanf(fin, "%zu", &idata_count);
	if (feof(fin) || ferror(fin))
		goto exit_bad_file;

	idata = malloc(idata_count * sizeof *idata);
	if (idata == NULL)
		goto exit_no_mem;

	for (i = 0; i < idata_count; ++i)
		fscanf(fin, "%d", &idata[i]);
	if (feof(fin) || ferror(fin))
		goto exit_bad_file;

	fscanf(fin, "%zu", &sdata_count);
	if (feof(fin) || ferror(fin))
		goto exit_bad_file;

	sdata = malloc(sdata_count * sizeof *sdata);
	if (sdata == NULL)
		goto exit_no_mem;

	for (i = 0; i < sdata_count; ++i)
	{
		sdata[i] = read_str(fin);

		/* could also be no memory, but chances of this is higher */
		if (sdata[i] == NULL)
			goto exit_bad_file;
	}

	fclose(fin);

	return 0;
exit_no_file:
	printf("no such file: \"%s\"\n", file);
	goto exit_fail;
exit_bad_file:
	printf("bad file format\n");
	goto exit_yes_file;
exit_no_mem:
	printf("out of memory\n");
exit_yes_file:
	fclose(fin);
exit_fail:
	cleanup_data();
	return -1;
}

void dump_data(const char *file)
{
	size_t i;
	FILE *fout = fopen(file, "w");

	if (fout == NULL)
		goto exit_no_file;

	fprintf(fout, "%zu\n", idata_count);
	for (i = 0; i < idata_count; ++i)
		fprintf(fout, "%d%c", idata[i], (i == idata_count - 1 || i % 20 == 19)?'\n':' ');
	fprintf(fout, "%zu\n", sdata_count);
	for (i = 0; i < sdata_count; ++i)
		fprintf(fout, "%s%c", sdata[i], (i == sdata_count - 1 || i % 20 == 19)?'\n':' ');

	fclose(fout);

	return;
exit_no_file:
	printf("could not create file: \"%s\"\n", file);
}

void cleanup_data(void)
{
	size_t i;

	if (sdata)
		for (i = 0; i < sdata_count; ++i)
			free(sdata[i]);
	free(idata);
	free(sdata);
	idata = NULL;
	sdata = NULL;
	idata_count = 0;
	sdata_count = 0;
}
