/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <shds.h>
#include "io.h"

#define DUMP_FILE "error_case.out"

int *idata = NULL;
size_t idata_count = 0;
char **sdata = NULL;
size_t sdata_count = 0;

/*
 * il: list of int * i.e. pointers to idata[.]
 * sl: list of char * i.e. copies of sdata[.]
 */
shList il = {0}, sl = {0};

static void _intptrcpy(void *to, void *from)
{
	*(int **)to = (int *)from;
}

static void _charptrcpy(void *to, void *from)
{
	*(char **)to = (char *)from;
}

int icmp(const void *i1, const void *i2)
{
	int i = *(const int *)i1;
	int j = *(const int *)i2;

	if (i == j)
		return 0;
	if (i < j)
		return -1;

	return 1;
}

int ipcmp(const void *i1, const void *i2)
{
	int *i = *(int * const *)i1;
	int *j = *(int * const *)i2;

	return icmp(i, j);
}

int scmp(const void *s1, const void *s2)
{
	return strcmp(*(char * const *)s1, *(char * const *)s2);
}

/* initialize structure */
int test_init(void)
{
	bool good = true;
	shNode c;

	sh_list_free(&il);
	sh_list_free(&sl);

	good = good && sh_list_init(&il, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_list_init(&sl, _charptrcpy, sizeof(char *)) == 0;
	/* it's ok to reinitialize with these attributes */
	good = good && sh_list_init(&il, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_list_init(&sl, _charptrcpy, sizeof(char *)) == 0;
	good = good && sh_list_init(&il, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_list_init(&sl, _charptrcpy, sizeof(char *)) == 0;
	good = good && sh_list_init(&il, _intptrcpy, sizeof(int *), ipcmp) == 0;
	good = good && sh_list_init(&sl, _charptrcpy, sizeof(char *), scmp) == 0;

	if (!sh_list_get_first(&il, &c) || !sh_list_get_last(&il, &c))
		goto exit_not_empty;

	if (!sh_list_get_first(&sl, &c) || !sh_list_get_last(&sl, &c))
		goto exit_not_empty;

	if (!good)
		goto exit_fail;

	return 0;
exit_not_empty:
	fprintf(stderr, "init: initialized list is not empty");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

/*
 * test whether structure data match the provided data (for integer test)
 * d is the expected results and n is its count
 */
int test_idata(shList *l, int *d, size_t n)
{
	size_t i;
	shNode c;

	if (sh_list_get_first(l, &c) && l->size > 0)
		return -1;
	if (l->size != n)
		return -1;
	if (n == 0)
		return 0;

	i = 0;
	while (true)
	{
		if (i >= n)
			return -1;
		if (**(int **)c.data != d[i])
			return -1;
		++i;
		if (sh_list_get_next(&c, &c))
			break;
	}
	if (i != n)
		return -1;

	return 0;
}

/*
 * test whether structure data match the provided data (for string test)
 * d is the expected results and n is its count
 */
int test_sdata(shList *l, char **d, size_t n)
{
	size_t i;
	shNode c;

	/*
	 * to test both directions, let this one go from last to first
	 * while test_idata goes from first to last
	 */
	if (sh_list_get_last(l, &c) && l->size > 0)
		return -1;
	if (l->size != n)
		return -1;
	if (n == 0)
		return 0;

	i = 0;
	while (true)
	{
		if (i >= n)
			return -1;
		if (strcmp(*(char **)c.data, d[n - 1 - i]) != 0)
			return -1;
		++i;
		if (sh_list_get_prev(&c, &c))
			break;
	}
	if (i != n)
		return -1;

	return 0;
}

int test_append(void)
{
	size_t i;
	bool good = true;

	for (i = 0; i < idata_count; ++i)
		if (sh_list_append(&il, &idata[i]))
			goto exit_bad_append;

	for (i = 0; i < sdata_count; ++i)
		if (sh_list_append(&sl, sdata[i]))
			goto exit_bad_append;

	good = good && test_idata(&il, idata, idata_count) == 0;
	good = good && test_sdata(&sl, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_bad_append:
	fprintf(stderr, "append: could not append\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "append: mismatch between real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_duplicate(void)
{
	bool good = true;
	shList il1, sl1, il2, sl2;

	/* test empty copy */
	sh_list_init(&il1, _intptrcpy, sizeof(int *));
	sh_list_init(&sl1, _charptrcpy, sizeof(char *));
	good = good && sh_list_duplicate(&il1, &il2) == 0;
	good = good && sh_list_duplicate(&sl1, &sl2) == 0;
	if (!good)
		goto exit_bad_empty_duplicate;

	good = good && test_idata(&il2, NULL, 0) == 0;
	good = good && test_sdata(&sl2, NULL, 0) == 0;
	if (!good)
		goto exit_test_empty;

	sh_list_clear(&il2);
	sh_list_clear(&sl2);

	/* test full copy */
	good = good && sh_list_duplicate(&il, &il2) == 0;
	good = good && sh_list_duplicate(&sl, &sl2) == 0;
	if (!good)
		goto exit_bad_full_duplicate;

	good = good && test_idata(&il2, idata, idata_count) == 0;
	good = good && test_sdata(&sl2, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_full;

	goto exit_cleanup;
exit_bad_empty_duplicate:
	fprintf(stderr, "duplicate: bad duplication of empty list\n");
	goto exit_fail;
exit_test_empty:
	fprintf(stderr, "duplicate: duplicated empty array data mismatch\n");
	goto exit_fail;
exit_bad_full_duplicate:
	fprintf(stderr, "duplicate: bad duplication of non-empty list\n");
	goto exit_fail;
exit_test_full:
	fprintf(stderr, "duplicate: duplicated full array data mismatch\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
exit_cleanup:
	sh_list_free(&il1);
	sh_list_free(&sl1);
	sh_list_free(&il2);
	sh_list_free(&sl2);

	return good?0:-1;
}

int test_prepend(void)
{
	size_t i;
	bool good = true;

	sh_list_clear(&il);
	sh_list_clear(&sl);

	for (i = 0; i < idata_count; ++i)
		if (sh_list_prepend(&il, &idata[idata_count - 1 - i]))
			goto exit_bad_prepend;

	for (i = 0; i < sdata_count; ++i)
		if (sh_list_prepend(&sl, sdata[sdata_count - 1 - i]))
			goto exit_bad_prepend;

	good = good && test_idata(&il, idata, idata_count) == 0;
	good = good && test_sdata(&sl, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_bad_prepend:
	fprintf(stderr, "prepend: could not append\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "prepend: mismatch between real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_links(void)
{
	shNode c;

	sh_list_get_first(&il, &c);
	if (!sh_list_get_prev(&c, &c))
		goto exit_first_has_prev;

	sh_list_get_last(&il, &c);
	if (!sh_list_get_next(&c, &c))
		goto exit_last_has_next;

	sh_list_get_first(&il, &c);
	while (true)
	{
		shNode p, n;

		if (!sh_list_get_prev(&c, &p) && (sh_list_get_next(&p, &n) || n.data != c.data))
			goto exit_bad_link;
		if (!sh_list_get_next(&c, &n) && (sh_list_get_prev(&n, &p) || p.data != c.data))
			goto exit_bad_link;
		if (sh_list_get_next(&c, &c))
			break;
	}

	return 0;
exit_first_has_prev:
	fprintf(stderr, "links: first->prev is not NULL\n");
	goto exit_fail;
exit_last_has_next:
	fprintf(stderr, "links: last->next is not NULL\n");
	goto exit_fail;
exit_bad_link:
	fprintf(stderr, "links: mismatch between node and node->prev->next or node->next->prev\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

void duplicate_list(shList *il1, shList *il2, shList *sl1, shList *sl2)
{
	sh_list_clear(il1);
	sh_list_clear(sl1);
	sh_list_duplicate(il2, il1);
	sh_list_duplicate(sl2, sl1);
}

int concat_and_test(shList *il1, shList *il2, shList *sl1, shList *sl2, int *id, size_t in, char **sd, size_t sn)
{
	bool good = true;

	good = good && sh_list_concat(il1, il2) == 0;
	good = good && sh_list_concat(sl1, sl2) == 0;
	good = good && test_idata(il1, id, in) == 0;
	good = good && test_sdata(sl1, sd, sn) == 0;
	if (!good)
		return -1;

	return 0;
}

int test_concat(void)
{
	shList il1, il2, il3, il4;
	shList sl1, sl2, sl3, sl4;
	shList itemp, stemp;
	int ret;
	size_t i;
	int *ext_idata = NULL;
	size_t ext_idata_count;
	char **ext_sdata = NULL;
	size_t ext_sdata_count;
	bool good = true;

	sh_list_init(&il1, _intptrcpy, sizeof(int *));
	sh_list_init(&il2, _intptrcpy, sizeof(int *));
	sh_list_init(&il3, _intptrcpy, sizeof(int *));
	sh_list_init(&il4, _intptrcpy, sizeof(int *));
	sh_list_init(&sl1, _charptrcpy, sizeof(char *));
	sh_list_init(&sl2, _charptrcpy, sizeof(char *));
	sh_list_init(&sl3, _charptrcpy, sizeof(char *));
	sh_list_init(&sl4, _charptrcpy, sizeof(char *));
	sh_list_init(&itemp, _intptrcpy, sizeof(int *));
	sh_list_init(&stemp, _charptrcpy, sizeof(char *));

	/* {} + {} */
	ret = concat_and_test(&il1, &il2, &sl1, &sl2, NULL, 0, NULL, 0);
	if (ret == -1)
		goto exit_test_fail_both_empty;

	/* l + {} */
	ret = concat_and_test(&il, &il1, &sl, &sl1, idata, idata_count, sdata, sdata_count);
	if (ret == -1)
		goto exit_test_fail_empty_right;

	/* {} + l */
	ret = concat_and_test(&il2, &il, &sl2, &sl, idata, idata_count, sdata, sdata_count);
	if (ret == -1)
		goto exit_test_fail_empty_left;
	sh_list_concat(&il, &il2);
	sh_list_concat(&sl, &sl2);

	/* l + l/2 */
	ext_idata_count = idata_count / 2;
	ext_sdata_count = sdata_count / 2;
	ext_idata = malloc((idata_count + ext_idata_count) * sizeof *ext_idata);
	ext_sdata = malloc((sdata_count + ext_sdata_count) * sizeof *ext_sdata);
	if (ext_idata == NULL || ext_sdata == NULL)
		goto exit_no_mem;

	memcpy(ext_idata, idata, idata_count * sizeof *idata);
	memcpy(ext_sdata, sdata, sdata_count * sizeof *sdata);
	for (i = 0; i < ext_idata_count; ++i)
	{
		ext_idata[i + idata_count] = idata[i];
		sh_list_append(&il3, &idata[i]);
	}
	for (i = 0; i < ext_sdata_count; ++i)
	{
		ext_sdata[i + sdata_count] = sdata[i];
		sh_list_append(&sl3, sdata[i]);
	}
	duplicate_list(&itemp, &il, &stemp, &sl);
	ret = concat_and_test(&itemp, &il3, &stemp, &sl3, ext_idata, idata_count + ext_idata_count, ext_sdata, sdata_count + ext_sdata_count);
	if (ret == -1)
		goto exit_test_fail_smaller_right;

	/* l/2 + l */
	memcpy(ext_idata + ext_idata_count, idata, idata_count * sizeof *idata);
	memcpy(ext_sdata + ext_sdata_count, sdata, sdata_count * sizeof *sdata);
	for (i = 0; i < ext_idata_count; ++i)
	{
		ext_idata[i] = idata[i];
		sh_list_append(&il4, &idata[i]);
	}
	for (i = 0; i < ext_sdata_count; ++i)
	{
		ext_sdata[i] = sdata[i];
		sh_list_append(&sl4, sdata[i]);
	}
	duplicate_list(&itemp, &il, &stemp, &sl);
	ret = concat_and_test(&il4, &itemp, &sl4, &stemp, ext_idata, idata_count + ext_idata_count, ext_sdata, sdata_count + ext_sdata_count);
	if (ret == -1)
		goto exit_test_fail_smaller_left;

	/* l/2 + l/2 */
	memcpy(ext_idata + ext_idata_count, ext_idata, ext_idata_count * sizeof *ext_idata);
	memcpy(ext_sdata + ext_sdata_count, ext_sdata, ext_sdata_count * sizeof *ext_sdata);
	for (i = 0; i < ext_idata_count; ++i)
		sh_list_append(&il3, &idata[i]);
	for (i = 0; i < ext_sdata_count; ++i)
		sh_list_append(&sl3, sdata[i]);
	duplicate_list(&itemp, &il3, &stemp, &sl3);
	ret = concat_and_test(&il3, &itemp, &sl3, &stemp, ext_idata, ext_idata_count * 2, ext_sdata, ext_sdata_count * 2);
	if (ret == -1)
		goto exit_test_fail_equal_size;
	if (!sh_list_concat(&il3, &il3) || !sh_list_concat(&sl3, &sl3))
		goto exit_accepted_duplicate;

	good = true;
	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "concat: out of memory\n");
	goto exit_fail;
exit_test_fail_empty_right:
	fprintf(stderr, "concat: bad concatenation with empty list on the right\n");
	goto exit_fail;
exit_test_fail_empty_left:
	fprintf(stderr, "concat: bad concatenation with empty list on the left\n");
	goto exit_fail;
exit_test_fail_smaller_right:
	fprintf(stderr, "concat: bad concatenation with smaller list on the right\n");
	goto exit_fail;
exit_test_fail_smaller_left:
	fprintf(stderr, "concat: bad concatenation with smaller list on the left\n");
	goto exit_fail;
exit_test_fail_both_empty:
	fprintf(stderr, "concat: bad concatenation with two empty lists\n");
	goto exit_fail;
exit_test_fail_equal_size:
	fprintf(stderr, "concat: bad concatenation with lists of same size\n");
	goto exit_fail;
exit_accepted_duplicate:
	fprintf(stderr, "concat: accepted concatenation with itself (causes loop)\n");
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_list_free(&il1);
	sh_list_free(&il2);
	sh_list_free(&il3);
	sh_list_free(&il4);
	sh_list_free(&sl1);
	sh_list_free(&sl2);
	sh_list_free(&sl3);
	sh_list_free(&sl4);
	sh_list_free(&itemp);
	sh_list_free(&stemp);
	free(ext_idata);
	free(ext_sdata);
	return good?0:-1;
}

int test_break(void)
{
	shList il2, sl2;
	shNode in, sn;
	int ii, si;
	bool good = true;

	/* break after last */
	if (sh_list_get_last(&il, &in) || sh_list_get_last(&sl, &sn))
		goto exit_bad_get_last;
	if (sh_list_break_after(&in, &il) == 0 || sh_list_break_after(&sn, &sl) == 0)
		goto exit_accepted_self;
	if (sh_list_break_after(&in, &il2) || sh_list_break_after(&sn, &sl2))
		goto exit_bad_break_after;
	good = good && test_idata(&il, idata, idata_count) == 0;
	good = good && test_sdata(&sl, sdata, sdata_count) == 0;
	if (!good)
		goto exit_break_after_bad_left;
	good = good && test_idata(&il2, NULL, 0) == 0;
	good = good && test_sdata(&sl2, NULL, 0) == 0;
	if (!good)
		goto exit_break_after_bad_right;

	/* break before first */
	if (sh_list_get_first(&il, &in) || sh_list_get_first(&sl, &sn))
		goto exit_bad_get_first;
	if (sh_list_break_before(&in, &il) == 0 || sh_list_break_before(&sn, &sl) == 0)
		goto exit_accepted_self;
	if (sh_list_break_before(&in, &il2) || sh_list_break_before(&sn, &sl2))
		goto exit_bad_break_before;
	good = good && test_idata(&il2, idata, idata_count) == 0;
	good = good && test_sdata(&sl2, sdata, sdata_count) == 0;
	if (!good)
		goto exit_break_before_bad_right;
	good = good && test_idata(&il, NULL, 0) == 0;
	good = good && test_sdata(&sl, NULL, 0) == 0;
	if (!good)
		goto exit_break_before_bad_left;
	if (sh_list_concat(&il, &il2) || sh_list_concat(&sl, &sl2))
		goto exit_bad_concat;

	/* break before last */
	if (sh_list_get_last(&il, &in) || sh_list_get_last(&sl, &sn))
		goto exit_bad_get_last;
	if (sh_list_break_before(&in, &il2) || sh_list_break_before(&sn, &sl2))
		goto exit_bad_break_before;
	good = good && test_idata(&il, idata, idata_count - 1) == 0;
	good = good && test_sdata(&sl, sdata, sdata_count - 1) == 0;
	if (!good)
		goto exit_break_before_bad_left;
	good = good && test_idata(&il2, idata + idata_count - 1, 1) == 0;
	good = good && test_sdata(&sl2, sdata + sdata_count - 1, 1) == 0;
	if (!good)
		goto exit_break_before_bad_right;
	if (sh_list_concat(&il, &il2) || sh_list_concat(&sl, &sl2))
		goto exit_bad_concat;

	/* break after first */
	if (sh_list_get_first(&il, &in) || sh_list_get_first(&sl, &sn))
		goto exit_bad_get_first;
	if (sh_list_break_after(&in, &il2) || sh_list_break_after(&sn, &sl2))
		goto exit_bad_break_after;
	good = good && test_idata(&il2, idata + 1, idata_count - 1) == 0;
	good = good && test_sdata(&sl2, sdata + 1, sdata_count - 1) == 0;
	if (!good)
		goto exit_break_after_bad_right;
	good = good && test_idata(&il, idata, 1) == 0;
	good = good && test_sdata(&sl, sdata, 1) == 0;
	if (!good)
		goto exit_break_after_bad_left;
	if (sh_list_concat(&il, &il2) || sh_list_concat(&sl, &sl2))
		goto exit_bad_concat;

	/* break after in the middle */
	if (sh_list_get_first(&il, &in) || sh_list_get_first(&sl, &sn))
		goto exit_bad_get_first;
	for (ii = 0; ii < idata_count / 2; ++ii)
		sh_list_get_next(&in, &in);
	for (si = 0; si < sdata_count / 2; ++si)
		sh_list_get_next(&sn, &sn);
	if (sh_list_break_after(&in, &il2) || sh_list_break_after(&sn, &sl2))
		goto exit_bad_break_after;
	good = good && test_idata(&il2, idata + ii + 1, idata_count - ii - 1) == 0;
	good = good && test_sdata(&sl2, sdata + si + 1, sdata_count - si - 1) == 0;
	if (!good)
		goto exit_break_after_bad_right;
	good = good && test_idata(&il, idata, ii + 1) == 0;
	good = good && test_sdata(&sl, sdata, si + 1) == 0;
	if (!good)
		goto exit_break_after_bad_left;
	if (sh_list_concat(&il, &il2) || sh_list_concat(&sl, &sl2))
		goto exit_bad_concat;

	/* break before in the middle */
	if (sh_list_get_first(&il, &in) || sh_list_get_first(&sl, &sn))
		goto exit_bad_get_first;
	for (ii = 0; ii < idata_count / 2; ++ii)
		sh_list_get_next(&in, &in);
	for (si = 0; si < sdata_count / 2; ++si)
		sh_list_get_next(&sn, &sn);
	if (sh_list_break_before(&in, &il2) || sh_list_break_before(&sn, &sl2))
		goto exit_bad_break_before;
	good = good && test_idata(&il2, idata + ii, idata_count - ii) == 0;
	good = good && test_sdata(&sl2, sdata + si, sdata_count - si) == 0;
	if (!good)
		goto exit_break_before_bad_right;
	good = good && test_idata(&il, idata, ii) == 0;
	good = good && test_sdata(&sl, sdata, si) == 0;
	if (!good)
		goto exit_break_before_bad_left;
	if (sh_list_concat(&il, &il2) || sh_list_concat(&sl, &sl2))
		goto exit_bad_concat;

	return 0;
exit_bad_get_last:
	fprintf(stderr, "break: get last failed\n");
	goto exit_fail;
exit_bad_get_first:
	fprintf(stderr, "break: get first failed\n");
	goto exit_fail;
exit_accepted_self:
	fprintf(stderr, "break: accepted second argument equal to self\n");
	goto exit_fail;
exit_bad_concat:
	fprintf(stderr, "break: concat failed\n");
	goto exit_fail;
exit_bad_break_after:
	fprintf(stderr, "break: break after failed\n");
	goto exit_fail;
exit_bad_break_before:
	fprintf(stderr, "break: break before failed\n");
	goto exit_fail;
exit_break_after_bad_left:
	fprintf(stderr, "break: break after messed up left half\n");
	goto exit_fail;
exit_break_after_bad_right:
	fprintf(stderr, "break: break after messed up right half\n");
	goto exit_fail;
exit_break_before_bad_left:
	fprintf(stderr, "break: break before messed up left half\n");
	goto exit_fail;
exit_break_before_bad_right:
	fprintf(stderr, "break: break before messed up right half\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_insert(void)
{
	shNode in, sn;
	size_t i;
	size_t ib, sb;
	shList il2, sl2;
	bool good = true;

	if (sh_list_get_first(&il, &in) || sh_list_get_first(&sl, &sn))
		goto exit_bad_get_first;
	for (i = 0; i < idata_count; i += 2)
		if (sh_list_get_next(&in, &in))
			goto exit_bad_next;
	ib = i / 2;
	for (i = 0; i < sdata_count; i += 2)
		if (sh_list_get_next(&sn, &sn))
			goto exit_bad_next;
	sb = i / 2;
	if (sh_list_break_before(&in, &il2) || sh_list_break_before(&sn, &sl2))
		goto exit_bad_break;
	good = good && test_idata(&il, idata, ib) == 0;
	good = good && test_sdata(&sl, sdata, sb) == 0;
	good = good && test_idata(&il2, idata + ib, idata_count - ib) == 0;
	good = good && test_sdata(&sl2, sdata + sb, sdata_count - sb) == 0;
	if (!good)
		goto exit_wrong_break;

	/* insert after last */
	if (sh_list_get_last(&il, &in) || sh_list_get_last(&sl, &sn))
		goto exit_bad_get_last;
	if (sh_list_insert_after(&in, &idata[ib]) || sh_list_insert_after(&sn, sdata[sb]))
		goto exit_bad_insert_after;
	good = good && test_idata(&il, idata, ib + 1) == 0;
	good = good && test_sdata(&sl, sdata, sb + 1) == 0;
	if (!good)
		goto exit_wrong_insert_after;

	/* insert before first */
	if (sh_list_get_first(&il2, &in) || sh_list_get_first(&sl2, &sn))
		goto exit_bad_get_first;
	if (sh_list_insert_before(&in, &idata[ib - 1]) || sh_list_insert_before(&sn, sdata[sb - 1]))
		goto exit_bad_insert_before;
	good = good && test_idata(&il2, idata + ib - 1, idata_count - ib + 1) == 0;
	good = good && test_sdata(&sl2, sdata + sb - 1, sdata_count - sb + 1) == 0;
	if (!good)
		goto exit_wrong_insert_before;

	/* insert after in the middle */
	if (sh_list_get_last(&il, &in) || sh_list_get_last(&sl, &sn))
		goto exit_bad_get_last;
	for (i = 0; i < idata_count - ib - 1; ++i)
		if (sh_list_insert_after(&in, &idata[idata_count - i - 1]))
			goto exit_bad_insert_after;
	for (i = 0; i < sdata_count - sb - 1; ++i)
		if (sh_list_insert_after(&sn, sdata[sdata_count - i - 1]))
			goto exit_bad_insert_after;
	good = good && test_idata(&il, idata, idata_count) == 0;
	good = good && test_sdata(&sl, sdata, sdata_count) == 0;
	if (!good)
		goto exit_wrong_insert_after;

	/* insert before in the middle */
	if (sh_list_get_first(&il2, &in) || sh_list_get_first(&sl2, &sn))
		goto exit_bad_get_first;
	for (i = 0; i < ib - 1; ++i)
		if (sh_list_insert_before(&in, &idata[i]))
			goto exit_bad_insert_before;
	for (i = 0; i < sb - 1; ++i)
		if (sh_list_insert_before(&sn, sdata[i]))
			goto exit_bad_insert_before;
	good = good && test_idata(&il2, idata, idata_count) == 0;
	good = good && test_sdata(&sl2, sdata, sdata_count) == 0;
	if (!good)
		goto exit_wrong_insert_before;

	goto exit_cleanup;
exit_bad_get_first:
	fprintf(stderr, "insert: get first failed\n");
	goto exit_fail;
exit_bad_get_last:
	fprintf(stderr, "insert: get last failed\n");
	goto exit_fail;
exit_bad_next:
	fprintf(stderr, "insert: next failed\n");
	goto exit_fail;
exit_bad_break:
	fprintf(stderr, "insert: break failed\n");
	goto exit_fail;
exit_wrong_break:
	fprintf(stderr, "insert: wrong break\n");
	goto exit_fail;
exit_bad_insert_after:
	fprintf(stderr, "insert: insert after failed\n");
	goto exit_fail;
exit_bad_insert_before:
	fprintf(stderr, "insert: insert before failed\n");
	goto exit_fail;
exit_wrong_insert_after:
	fprintf(stderr, "insert: wrong insert after\n");
	goto exit_fail;
exit_wrong_insert_before:
	fprintf(stderr, "insert: wrong insert before\n");
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_list_free(&il2);
	sh_list_free(&sl2);
	return 0;
}

int test_compare(void)
{
	shList il1;
	shList sl1;
	bool good = true;

	sh_list_init(&il1, _intptrcpy, sizeof(int *), ipcmp);
	sh_list_init(&sl1, _charptrcpy, sizeof(char *), scmp);

	if (sh_list_compare(&il, &il) != 0 || sh_list_compare(&sl, &sl) != 0
		|| sh_list_compare_lexic(&il, &il) != 0 || sh_list_compare_lexic(&sl, &sl) != 0)
		goto exit_bad_equal;

	duplicate_list(&il1, &il, &sl1, &sl);
	if (sh_list_append(&il1, &idata[0]) || sh_list_append(&sl1, sdata[0]))
		goto exit_no_mem;

	if (sh_list_compare(&il, &il1) == 0 || sh_list_compare(&sl, &sl1) == 0)
		goto exit_bad_inequal;
	if (sh_list_compare_lexic(&il, &il1) != -1 || sh_list_compare_lexic(&il1, &il) != 1
		|| sh_list_compare_lexic(&sl, &sl1) != -1 || sh_list_compare_lexic(&sl1, &sl) != 1)
		goto exit_bad_lexic;

	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "compare: out of memory\n");
	goto exit_fail;
exit_bad_equal:
	fprintf(stderr, "compare: failed to recognize equal lists\n");
	goto exit_fail;
exit_bad_inequal:
	fprintf(stderr, "compare: failed to recognize inequal lists\n");
	goto exit_fail;
exit_bad_lexic:
	fprintf(stderr, "compare: incorrect lexicographical comparison\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_list_free(&il1);
	sh_list_free(&sl1);
	return good?0:-1;
}

int test_delete(void)
{
	shNode in, sn;
	size_t i;
	bool good = true;

	/* delete first */
	if (sh_list_get_first(&il, &in) || sh_list_get_first(&sl, &sn))
		goto exit_bad_get_first;
	if (sh_list_delete(&in, &in) || sh_list_delete(&sn, &sn))
		goto exit_bad_delete_first;
	good = good && test_idata(&il, idata + 1, idata_count - 1) == 0;
	good = good && test_sdata(&sl, sdata + 1, sdata_count - 1) == 0;
	if (!good)
		goto exit_wrong_delete_first;

	/* delete last */
	if (sh_list_get_last(&il, &in) || sh_list_get_last(&sl, &sn))
		goto exit_bad_get_last;
	if (sh_list_delete(&in, &in) == 0 || sh_list_delete(&sn, &sn) == 0)
		goto exit_bad_delete_last;
	good = good && test_idata(&il, idata + 1, idata_count - 2) == 0;
	good = good && test_sdata(&sl, sdata + 1, sdata_count - 2) == 0;
	if (!good)
		goto exit_wrong_delete_last;

	/* delete from middle */
	if (sh_list_get_first(&il, &in) || sh_list_get_first(&sl, &sn))
		goto exit_bad_get_first;
	for (i = 0; i < idata_count - 2; ++i)
	{
		/* insert fake */
		if (sh_list_insert_after(&in, &i, &in))
			goto exit_bad_insert;
		/* delete it and go to next */
		if ((sh_list_delete(&in, &in) == 0) ^ (i != idata_count - 3))
			goto exit_bad_delete_middle;
		/* note: checking all the cases takes too long */
		if (i % 836 == 0 || i == idata_count - 3)
			if (test_idata(&il, idata + 1, idata_count - 2))
				goto exit_wrong_delete_middle;
	}
	for (i = 0; i < sdata_count - 2; ++i)
	{
		/* insert fake */
		if (sh_list_insert_after(&sn, NULL, &sn))
			goto exit_bad_insert;
		/* delete it and go to next */
		if ((sh_list_delete(&sn, &sn) == 0) ^ (i != sdata_count - 3))
			goto exit_bad_delete_middle;
		/* note: checking all the cases takes too long */
		if (i % 1092 == 0 || i == sdata_count - 3)
			if (test_sdata(&sl, sdata + 1, sdata_count - 2))
				goto exit_wrong_delete_middle;
	}
	return 0;
exit_bad_get_first:
	fprintf(stderr, "delete: get first failed\n");
	goto exit_fail;
exit_bad_get_last:
	fprintf(stderr, "delete: get last failed\n");
	goto exit_fail;
exit_bad_insert:
	fprintf(stderr, "delete: bad insert\n");
	goto exit_fail;
exit_bad_delete_first:
	fprintf(stderr, "delete: delete first failed\n");
	goto exit_fail;
exit_bad_delete_last:
	fprintf(stderr, "delete: delete last failed\n");
	goto exit_fail;
exit_bad_delete_middle:
	fprintf(stderr, "delete: delete middle failed\n");
	goto exit_fail;
exit_wrong_delete_first:
	fprintf(stderr, "delete: wrong delete first\n");
	goto exit_fail;
exit_wrong_delete_last:
	fprintf(stderr, "delete: wrong delete last\n");
	goto exit_fail;
exit_wrong_delete_middle:
	fprintf(stderr, "delete: wrong delete middle\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_clear(void)
{
	bool good = true;
	int i = 10;
	char *s = "abfda";

	sh_list_clear(&il);
	sh_list_clear(&sl);

	if (il.size != 0 || sl.size != 0)
		goto exit_test_fail;
	good = good && sh_list_append(&il, &i) == 0;
	good = good && sh_list_append(&sl, s) == 0;
	if (!good)
		goto exit_test_fail;
	sh_list_clear(&il);
	sh_list_clear(&sl);
	good = good && sh_list_init(&il, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_list_init(&sl, _charptrcpy, sizeof(char *)) == 0;
	if (!good)
		goto exit_not_reinitializable;

	return 0;
exit_test_fail:
	fprintf(stderr, "clear: clear did not return object to a usable state\n");
	goto exit_fail;
exit_not_reinitializable:
	fprintf(stderr, "clear: clear did not return object to a reinitializable state\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_free(void)
{
	sh_list_free(&il);
	sh_list_free(&sl);
	/* multiple frees should not cause a problem */
	sh_list_free(&il);
	sh_list_free(&sl);
	sh_list_free(&il);
	sh_list_free(&sl);

	return 0;
}

int main(int argc, char **argv)
{
	bool good = true;

	srand(time(NULL));
	if (argc < 2)
	{
		if (generate_data(RANDOM_SIZE))
			goto exit_no_mem;
	}
	else
	{
		if (read_data(argv[1]))
			goto exit_bad_input;
	}

	good = good && test_init() == 0;
	good = good && test_append() == 0;
	good = good && test_duplicate() == 0;
	good = good && test_links() == 0;
	good = good && test_prepend() == 0;
	good = good && test_links() == 0;
	good = good && test_concat() == 0;
	good = good && test_links() == 0;
	good = good && test_break() == 0;
	good = good && test_links() == 0;
	good = good && test_insert() == 0;
	good = good && test_links() == 0;
	good = good && test_compare() == 0;
	good = good && test_delete() == 0;
	good = good && test_links() == 0;
	good = good && test_clear() == 0;
	good = good && test_free() == 0;

	cleanup_data();

	return good?EXIT_SUCCESS:EXIT_FAILURE;
exit_bad_input:
exit_no_mem:
	cleanup_data();
	return EXIT_FAILURE;
}
