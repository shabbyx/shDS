/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the isplied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <shds.h>
#include "io.h"

#define DUMP_FILE "error_case.out"

int *idata = NULL;
size_t idata_count = 0;
char **sdata = NULL;
size_t sdata_count = 0;

/*
 * is: set of int * i.e. pointers to idata[.]
 * ss: set of char * i.e. copies of sdata[.]
 */
shSet is = {0}, ss = {0};

static void _intptrcpy(void *to, void *from)
{
	*(int **)to = (int *)from;
}

static void _charptrcpy(void *to, void *from)
{
	*(char **)to = (char *)from;
}

int icmp(const void *i1, const void *i2)
{
	int i = **(int * const *)i1;
	int j = **(int * const *)i2;

	if (i == j)
		return 0;
	if (i < j)
		return -1;

	return 1;
}

int scmp(const void *s1, const void *s2)
{
	return strcmp(*(char * const *)s1, *(char * const *)s2);
}

typedef struct iset_content
{
	int *key;
} iset_content;

typedef struct sset_content
{
	char *key;
} sset_content;

int isccmp(const void *i1, const void *i2)
{
	int i = *((const iset_content *)i1)->key;
	int j = *((const iset_content *)i2)->key;

	if (i == j)
		return 0;
	if (i < j)
		return -1;

	return 1;
}

int ssccmp(const void *s1, const void *s2)
{
	return strcmp(((const sset_content *)s1)->key, ((const sset_content *)s2)->key);
}

iset_content *isc = NULL;
size_t isc_count = 0;
sset_content *ssc = NULL;
size_t ssc_count = 0;

/* initialize structure */
int test_init(void)
{
	bool good = true;

	sh_set_free(&is);
	sh_set_free(&ss);
	good = good && sh_set_init(&is, _intptrcpy, sizeof(int *), icmp) == 0;
	good = good && sh_set_init(&ss, _charptrcpy, sizeof(char *), scmp) == 0;
	/* it's ok to reinitialize with these attributes */
	good = good && sh_set_init(&is, _intptrcpy, sizeof(int *), icmp) == 0;
	good = good && sh_set_init(&ss, _charptrcpy, sizeof(char *), scmp) == 0;
	good = good && sh_set_init(&is, _intptrcpy, sizeof(int *), icmp) == 0;
	good = good && sh_set_init(&ss, _charptrcpy, sizeof(char *), scmp) == 0;
	good = good && sh_set_init(&is, _intptrcpy, sizeof(int *), icmp) == 0;
	good = good && sh_set_init(&ss, _charptrcpy, sizeof(char *), scmp) == 0;
	if (!good)
		dump_data(DUMP_FILE);

	return good?0:-1;
}

typedef struct test_append_data
{
	void *array;
	size_t size;
} test_append_data;

/* TODO: Same as shMap, test with returning false also */
bool test_idata_append(shNode *n, void *extra)
{
	iset_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	array[size].key = *(int **)n->key;
	++((test_append_data *)extra)->size;

	return true;
}

bool test_sdata_append(shNode *n, void *extra)
{
	sset_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	array[size].key = *(char **)n->key;
	++((test_append_data *)extra)->size;

	return true;
}

size_t append_size_i = 0;
size_t append_size_s = 0;
int visited_at_false = 0;

bool test_idata_append_half(shNode *n, void *extra)
{
	iset_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	if (size >= append_size_i)
	{
		++visited_at_false;
		return false;
	}
	array[size].key = *(int **)n->key;
	++((test_append_data *)extra)->size;

	return true;
}

bool test_sdata_append_half(shNode *n, void *extra)
{
	sset_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	if (size >= append_size_s)
	{
		++visited_at_false;
		return false;
	}
	array[size].key = *(char **)n->key;
	++((test_append_data *)extra)->size;

	return true;
}

/*
 * test whether structure data match the provided data (for integer test)
 * d is the expected results and n is its count
 */
int test_idata(shSet *s, iset_content *d, size_t n, int (*traverse)(shSet *, sh_callback, void *, ...), bool needs_sort)
{
	size_t i;
	test_append_data ad;
	iset_content *isc2;

	if (s->size != n)
		return -1;
	isc2 = malloc(n * sizeof *isc2);
	if (isc2 == NULL)
		goto exit_no_mem;

	ad.array = isc2;
	ad.size = 0;
	if (traverse(s, test_idata_append, &ad) != 0)
		goto exit_bad_full_traverse;
	if (ad.size != n)
		goto exit_fail;
	if (needs_sort)
		qsort(isc2, n, sizeof *isc2, isccmp);
	for (i = 0; i < n; ++i)
		if (*d[i].key != *isc2[i].key)
			goto exit_fail;

	/* if it needs sort, it doesn't make sense to only read half the array */
	if (!needs_sort)
	{
		visited_at_false = 0;
		ad.size = 0;
		if (traverse(s, test_idata_append_half, &ad) == 0 && ad.size >= append_size_i)
			goto exit_bad_half_traverse;
		for (i = 0; i < append_size_i && i < n; ++i)
			if (*d[i].key != *isc2[i].key)
				goto exit_fail;

		/* this is done after, so it would affect next test */
		append_size_i = n / 2;
		if (visited_at_false > 1)
			goto exit_fail;
	}
	free(isc2);
	return 0;
exit_bad_full_traverse:
	fprintf(stderr, "test_idata for each returned nonzero for full traversal\n");
	goto exit_fail;
exit_bad_half_traverse:
	fprintf(stderr, "test_idata for each returned zero for incomplete traversal\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
exit_fail:
	free(isc2);
	return -1;
}

/*
 * test whether structure data match the provided data (for string test)
 * d is the expected results and n is its count
 */
int test_sdata(shSet *s, sset_content *d, size_t n, int (*traverse)(shSet *, sh_callback, void *, ...), bool needs_sort)
{
	size_t i;
	test_append_data ad;
	sset_content *ssc2;

	if (s->size != n)
		return -1;
	ssc2 = malloc(n * sizeof *ssc2);
	if (ssc2 == NULL)
		goto exit_no_mem;

	ad.array = ssc2;
	ad.size = 0;
	if (traverse(s, test_sdata_append, &ad) != 0)
		goto exit_bad_full_traverse;
	if (ad.size != n)
		goto exit_fail;
	if (needs_sort)
		qsort(ssc2, n, sizeof *ssc2, ssccmp);
	for (i = 0; i < n; ++i)
		if (strcmp(d[i].key, ssc2[i].key) != 0)
			goto exit_fail;

	/* if it needs sort, it doesn't make sense to only read half the array */
	if (!needs_sort)
	{
		visited_at_false = 0;
		ad.size = 0;
		if (traverse(s, test_sdata_append_half, &ad) == 0 && ad.size >= append_size_s)
			goto exit_bad_half_traverse;
		for (i = 0; i < append_size_s && i < n; ++i)
			if (strcmp(d[i].key, ssc2[i].key) != 0)
				goto exit_fail;

		/* this is done after, so it would affect next test */
		append_size_s = n / 2;
		if (visited_at_false > 1)
			goto exit_fail;
	}
	free(ssc2);
	return 0;
exit_bad_full_traverse:
	fprintf(stderr, "test_sdata for each returned nonzero for full traversal\n");
	goto exit_fail;
exit_bad_half_traverse:
	fprintf(stderr, "test_sdata for each returned zero for incomplete traversal\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
exit_fail:
	free(ssc2);
	return -1;
}

int test_insert_exists_and_find(void)
{
	size_t i;
	bool good = true;

	for (i = 0; i < idata_count; ++i)
	{
		shNode n;
		int *to_find = &idata[i];

		if (sh_set_exists(&is, &to_find))
		{
			if (!sh_set_find(&is, &to_find, &n))
				goto exit_exist_find_mismatch;
			if (**(int **)n.key != idata[i])
				goto exit_bad_find;
			if (!sh_set_insert(&is, &idata[i]))
				goto exit_bad_insert;
		}
		else
		{
			if (sh_set_find(&is, &to_find, &n))
				goto exit_exist_find_mismatch;
			if (sh_set_insert(&is, &idata[i]))
				goto exit_bad_insert;
			if (!sh_set_exists(&is, &to_find))
				goto exit_bad_exists;
			isc[isc_count++].key = &idata[i];
		}
	}
	for (i = 0; i < sdata_count; ++i)
	{
		shNode n;

		if (sh_set_exists(&ss, &sdata[i]))
		{
			if (!sh_set_find(&ss, &sdata[i], &n))
				goto exit_exist_find_mismatch;
			if (strcmp(*(char **)n.key, sdata[i]) != 0)
				goto exit_bad_find;
			if (!sh_set_insert(&ss, sdata[i]))
				goto exit_bad_insert;
		}
		else
		{
			if (sh_set_find(&ss, &sdata[i], &n))
				goto exit_exist_find_mismatch;
			if (sh_set_insert(&ss, sdata[i]))
				goto exit_bad_insert;
			if (!sh_set_exists(&ss, &sdata[i]))
				goto exit_bad_exists;
			ssc[ssc_count++].key = sdata[i];
		}
	}

	qsort(isc, isc_count, sizeof *isc, isccmp);
	qsort(ssc, ssc_count, sizeof *ssc, ssccmp);
	good = good && test_idata(&is, isc, isc_count, sh_set_inorder, false) == 0;
	good = good && test_sdata(&ss, ssc, ssc_count, sh_set_inorder, false) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_exist_find_mismatch:
	fprintf(stderr, "insert exist find: exist and find outputs mismatch\n");
	goto exit_fail;
exit_bad_find:
	fprintf(stderr, "insert exist find: find found the wrong node\n");
	goto exit_fail;
exit_bad_insert:
	fprintf(stderr, "insert exist: insert failed\n");
	goto exit_fail;
exit_bad_exists:
	fprintf(stderr, "insert exist: exist did not find inserted node\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "insert exist: mismatch between real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_for_each(void)
{
	bool good = true;

	good = good && test_idata(&is, isc, isc_count, sh_set_for_each, true) == 0;
	good = good && test_sdata(&ss, ssc, ssc_count, sh_set_for_each, true) == 0;
	if (!good)
	{
		fprintf(stderr, "for each: mismatch between real data and the iterated ones\n");
		dump_data(DUMP_FILE);
	}

	return good?0:-1;
}

int test_to_vector(void)
{
	shVector iv = {0}, sv = {0};
	bool good = true;
	size_t i;

	if (sh_set_to_vector(&is, &iv))
		goto exit_bad_to_vector;
	if (sh_set_to_vector(&ss, &sv))
		goto exit_bad_to_vector;

	good = good && test_idata(&is, sh_vector_data(&iv), iv.size, sh_set_for_each, true) == 0;
	good = good && test_sdata(&ss, sh_vector_data(&sv), sv.size, sh_set_for_each, true) == 0;
	if (!good)
		goto exit_test_fail;

	for (i = 1; i < iv.size; ++i)
		if (**(int **)sh_vector_at(&iv, i - 1) > **(int **)sh_vector_at(&iv, i))
			goto exit_not_sorted;
	for (i = 1; i < sv.size; ++i)
		if (strcmp(*(char **)sh_vector_at(&sv, i - 1), *(char **)sh_vector_at(&sv, i)) > 0)
			goto exit_not_sorted;

	sh_vector_free(&iv);
	sh_vector_free(&sv);

	return 0;
exit_bad_to_vector:
	fprintf(stderr, "to vector: could not convert\n");
	goto exit_fail;
exit_not_sorted:
	fprintf(stderr, "to vector: not sorted\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "to vector: bad conversion\n");
exit_fail:
	dump_data(DUMP_FILE);
	sh_vector_free(&iv);
	sh_vector_free(&sv);
	return -1;
}

int test_iteration(void)
{
	shNode c;
	size_t i;
	int i_prev = 0;
	char *s_prev = NULL;

	sh_set_get_first(&is, &c);
	if (!sh_set_get_prev(&c, &c))
		goto exit_first_has_prev;

	sh_set_get_last(&is, &c);
	if (!sh_set_get_next(&c, &c))
		goto exit_last_has_next;

	for (sh_set_get_first(&is, &c), i = 0; i < is.size; ++i)
	{
		shNode p, n;

		if (!sh_set_get_prev(&c, &p) && (sh_set_get_next(&p, &n) || n.key != c.key))
			goto exit_bad_iteration;
		if (!sh_set_get_next(&c, &n) && (sh_set_get_prev(&n, &p) || p.key != c.key))
			goto exit_bad_iteration;

		if (i > 0 && i_prev > **(int **)c.key)
			goto exit_not_in_order;
		i_prev = **(int **)c.key;
		if (sh_set_get_next(&c, &c) && i + 1 != is.size)
			goto exit_early_end;
	}

	for (sh_set_get_last(&ss, &c), i = 0; i < ss.size; ++i)
	{
		shNode p, n;

		if (!sh_set_get_prev(&c, &p) && (sh_set_get_next(&p, &n) || n.key != c.key))
			goto exit_bad_iteration;
		if (!sh_set_get_next(&c, &n) && (sh_set_get_prev(&n, &p) || p.key != c.key))
			goto exit_bad_iteration;

		if (i > 0 && strcmp(s_prev, *(char **)c.key) < 0)
			goto exit_not_in_order;
		s_prev = *(char **)c.key;
		if (sh_set_get_prev(&c, &c) && i + 1 != ss.size)
			goto exit_early_start;
	}

	return 0;
exit_first_has_prev:
	fprintf(stderr, "iterations: first has prev\n");
	goto exit_fail;
exit_last_has_next:
	fprintf(stderr, "iterations: last has next\n");
	goto exit_fail;
exit_early_end:
	fprintf(stderr, "iterations: get next finished too early\n");
	goto exit_fail;
exit_early_start:
	fprintf(stderr, "iterations: get prev finished too early\n");
	goto exit_fail;
exit_not_in_order:
	fprintf(stderr, "iterations: iteration not in order\n");
	goto exit_fail;
exit_bad_iteration:
	fprintf(stderr, "iterations: mismatch between node and node->prev->next or node->next->prev\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

static bool _dup_int_set(shNode *n, void *s)
{
	return sh_set_insert(s, *(int **)n->key) == 0;
}

static bool _dup_str_set(shNode *n, void *s)
{
	return sh_set_insert(s, *(char **)n->key) == 0;
}

static int duplicate_set(shSet *is1, shSet *is2, shSet *ss1, shSet *ss2)
{
	sh_set_clear(is1);
	sh_set_clear(ss1);

	return sh_set_for_each(is2, _dup_int_set, is1)
		+ sh_set_for_each(ss2, _dup_str_set, ss1);
}

int test_compare(void)
{
	shSet is1;
	shSet ss1;
	bool good = true;
	int newint = INT_RANGE + 10;
	char newstr[] = "zzzzzz";

	sh_set_init(&is1, _intptrcpy, sizeof(int *), icmp);
	sh_set_init(&ss1, _charptrcpy, sizeof(char *), scmp);

	if (sh_set_compare(&is, &is) != 0 || sh_set_compare(&ss, &ss) != 0
		|| sh_set_compare_lexic(&is, &is) != 0 || sh_set_compare_lexic(&ss, &ss) != 0)
		goto exit_bad_equal;

	if (duplicate_set(&is1, &is, &ss1, &ss))
		goto exit_no_mem;
	if (sh_set_insert(&is1, &newint) || sh_set_insert(&ss1, newstr))
		goto exit_no_mem;

	if (sh_set_compare(&is, &is1) == 0 || sh_set_compare(&ss, &ss1) == 0)
		goto exit_bad_inequal;
	if (sh_set_compare_lexic(&is, &is1) != -1 || sh_set_compare_lexic(&is1, &is) != 1
		|| sh_set_compare_lexic(&ss, &ss1) != -1 || sh_set_compare_lexic(&ss1, &ss) != 1)
		goto exit_bad_lexic;

	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "compare: out of memory\n");
	goto exit_fais;
exit_bad_equal:
	fprintf(stderr, "compare: faised to recognize equal sets\n");
	goto exit_fais;
exit_bad_inequal:
	fprintf(stderr, "compare: faised to recognize inequal sets\n");
	goto exit_fais;
exit_bad_lexic:
	fprintf(stderr, "compare: incorrect lexicographical comparison\n");
	goto exit_fais;
exit_fais:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_set_free(&is1);
	sh_set_free(&ss1);
	return good?0:-1;
}

int test_delete(void)
{
	size_t i;

	iset_content *isc2 = malloc(isc_count * sizeof *isc2);
	sset_content *ssc2 = malloc(ssc_count * sizeof *ssc2);
	if (isc2 == NULL || ssc2 == NULL)
		goto exit_no_mem;

	/* delete from middle element to first */
	for (i = 0; i < isc_count / 2; ++i)
	{
		if (sh_set_delete_key(&is, &isc[isc_count / 2 - 1 - i].key))
			goto exit_bad_delete_key;

		/* note: checking all the cases takes too long */
		if (i % 987 == 0 || i == isc_count / 2 - 1)
		{
			memcpy(isc2, isc, (isc_count / 2 - 1 - i) * sizeof *isc2);
			memcpy(isc2 + (isc_count / 2 - 1 - i), isc + isc_count / 2, (isc_count - isc_count / 2) * sizeof *isc2);
			if (test_idata(&is, isc2, isc_count - 1 - i, sh_set_inorder, false))
				goto exit_bad_delete;
		}
	}

	/* delete from middle to last */
	memcpy(ssc2, ssc, ssc_count / 2 * sizeof *ssc2);
	for (i = 0; i + ssc_count / 2 < ssc_count; ++i)
	{
		if (sh_set_delete_key(&ss, &ssc[ssc_count / 2 + i].key))
			goto exit_bad_delete_key;

		/* note: checking all the cases takes too long */
		if (i % 1052 == 0 || i + ssc_count / 2 == ssc_count - 1)
		{
			memcpy(ssc2 + ssc_count / 2, ssc + ssc_count / 2 + i + 1, (ssc_count - ssc_count / 2 - 1 - i) * sizeof *ssc2);
			if (test_sdata(&ss, ssc2, ssc_count - 1 - i, sh_set_inorder, false))
				goto exit_bad_delete;
		}
	}
	free(isc);
	free(ssc);
	isc = isc2;
	ssc = ssc2;
	isc_count -= isc_count / 2;
	ssc_count -= ssc_count / 2;
	return 0;
exit_no_mem:
	fprintf(stderr, "delete: out of memory\n");
	goto exit_fail;
exit_bad_delete_key:
	fprintf(stderr, "delete: failed to find key in set\n");
	goto exit_fail;
exit_bad_delete:
	fprintf(stderr, "delete: mismatch between data without deleted key and updated tree\n");
	goto exit_fail;
exit_fail:
	free(isc2);
	free(ssc2);
	dump_data(DUMP_FILE);
	return -1;
}

int test_clear(void)
{
	bool good = true;
	int i = 10;
	char *s = "abfda";

	sh_set_clear(&is);
	sh_set_clear(&ss);

	if (is.size != 0 || ss.size != 0)
		goto exit_test_fail;
	good = good && sh_set_insert(&is, &i) == 0;
	good = good && sh_set_insert(&ss, s) == 0;
	if (!good)
		goto exit_test_fail;
	sh_set_clear(&is);
	sh_set_clear(&ss);
	good = good && sh_set_init(&is, _intptrcpy, sizeof(int *), isccmp) == 0;
	good = good && sh_set_init(&ss, _charptrcpy, sizeof(char *), ssccmp) == 0;
	if (!good)
		goto exit_not_reinitializable;

	return 0;
exit_test_fail:
	fprintf(stderr, "clear: clear did not return object to a usable state\n");
	goto exit_fail;
exit_not_reinitializable:
	fprintf(stderr, "clear: clear did not return object to a reinitializable state\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_free(void)
{
	sh_set_free(&is);
	sh_set_free(&ss);
	/* multiple frees should not cause a problem */
	sh_set_free(&is);
	sh_set_free(&ss);
	sh_set_free(&is);
	sh_set_free(&ss);
	return 0;
}

int main(int argc, char **argv)
{
	bool good = true;

	srand(time(NULL));
	if (argc < 2)
	{
		if (generate_data(RANDOM_SIZE))
			goto exit_no_mem;
	}
	else
	{
		if (read_data(argv[1]))
			goto exit_bad_input;
	}

	isc = malloc(idata_count * sizeof *isc);
	ssc = malloc(sdata_count * sizeof *ssc);
	if (isc == NULL || ssc == NULL)
		goto exit_no_mem;

	good = good && test_init() == 0;
	good = good && test_insert_exists_and_find() == 0;
	good = good && test_for_each() == 0;
	good = good && test_to_vector() == 0;
	good = good && test_iteration() == 0;
	good = good && test_compare() == 0;
	good = good && test_delete() == 0;
	good = good && test_iteration() == 0;
	good = good && test_clear() == 0;
	good = good && test_free() == 0;

	cleanup_data();
	free(isc);
	free(ssc);

	return good?EXIT_SUCCESS:EXIT_FAILURE;
exit_bad_input:
exit_no_mem:
	cleanup_data();
	free(isc);
	free(ssc);
	return EXIT_FAILURE;
}
