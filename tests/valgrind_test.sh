#! /bin/bash

# first, run the program and make sure it succeeds
# this has the side effect that .libs/lt-test is created
./test || echo $?

# then run valgrind on the executable itself
[ -x ./.libs/lt-test ] || exit 1
result=$(valgrind --leak-check=full ./.libs/lt-test 2>&1 | grep --color=never 'in use at exit:\|== Invalid')

# write the result to be logged
echo "$result"

# the result is in the shape: ==PID==     in use at exit: X bytes in Y blocks
[ "$(echo "$result" | sed 's/==[0-9]*==\s*//')" = "in use at exit: 0 bytes in 0 blocks" ]
