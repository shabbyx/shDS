/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <shds.h>
#include "io.h"

#define DUMP_FILE "error_case.out"

int *idata = NULL;
size_t idata_count = 0;
char **sdata = NULL;
size_t sdata_count = 0;

/*
 * is: stack of int * i.e. pointers to idata[.]
 * ss: stack of char * i.e. copies of sdata[.]
 */
shStack is = {0}, ss = {0};

static void _intptrcpy(void *to, void *from)
{
	*(int **)to = (int *)from;
}

static void _charptrcpy(void *to, void *from)
{
	*(char **)to = (char *)from;
}

/* initialize structure */
int test_init(void)
{
	bool good = true;

	sh_stack_free(&is);
	sh_stack_free(&ss);

	good = good && sh_stack_init(&is, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_stack_init(&ss, _charptrcpy, sizeof(char *)) == 0;
	/* it's ok to reinitialize with these attributes */
	good = good && sh_stack_init(&is, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_stack_init(&ss, _charptrcpy, sizeof(char *)) == 0;
	good = good && sh_stack_init(&is, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_stack_init(&ss, _charptrcpy, sizeof(char *)) == 0;
	good = good && sh_stack_init(&is, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_stack_init(&ss, _charptrcpy, sizeof(char *)) == 0;

	if (!good)
		dump_data(DUMP_FILE);

	return good?0:-1;
}

/*
 * test whether structure data match the provided data (for integer test)
 * d is the expected results and n is its count
 */
int test_idata(shStack *s, int *d, size_t n)
{
	size_t i;

	if (s->size != n)
		return -1;
	for (i = 0; i < n; ++i)
		if (**(int **)sh_vector_at(s, i) != d[i])
			return -1;

	return 0;
}

/*
 * test whether structure data match the provided data (for string test)
 * d is the expected results and n is its count
 */
int test_sdata(shStack *s, char **d, size_t n)
{
	size_t i;

	if (s->size != n)
		return -1;
	for (i = 0; i < n; ++i)
		if (strcmp(*(char **)sh_vector_at(s, i), d[i]) != 0)
			return -1;

	return 0;
}

int test_push(void)
{
	size_t i;
	bool good = true;

	for (i = 0; i < idata_count; ++i)
		if (sh_stack_push(&is, &idata[i]))
			goto exit_no_mem;
	for (i = 0; i < sdata_count; ++i)
		if (sh_stack_push(&ss, sdata[i]))
			goto exit_no_mem;
	if (is.size != idata_count || ss.size != sdata_count)
		goto exit_bad_push;
	good = good && test_idata(&is, idata, idata_count) == 0;
	good = good && test_sdata(&ss, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_no_mem:
	fprintf(stderr, "push: out of memory\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "push: mismatch between real data and those in structure\n");
	goto exit_fail;
exit_bad_push:
	fprintf(stderr, "push: mismatch between size of real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_top_and_pop(void)
{
	size_t i;
	int **it;
	char **st;

	for (i = 0; i < idata_count; ++i)
	{
		if (is.size == 0)
			goto exit_bad_pop;
		it = sh_stack_top(&is);
		if (it == NULL)
			goto exit_test_internal_error;
		if (idata[idata_count - 1 - i] != **it)
			goto exit_test_fail;
		sh_stack_pop(&is);
	}
	for (i = 0; i < sdata_count; ++i)
	{
		if (ss.size == 0)
			goto exit_bad_pop;
		st = sh_stack_top(&ss);
		if (st == NULL)
			goto exit_test_internal_error;
		if (strcmp(sdata[sdata_count - 1 - i], *st) != 0)
			goto exit_test_fail;
		sh_stack_pop(&ss);
	}

	if (is.size != 0 || ss.size != 0)
		goto exit_bad_pop;

	return 0;
exit_test_internal_error:
	fprintf(stderr, "top and pop: error in the internals of the test\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "top and pop: mismatch between real data and those in structure\n");
	goto exit_fail;
exit_bad_pop:
	fprintf(stderr, "top and pop: pop did not clear the stack properly\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_clear(void)
{
	bool good = true;
	int i = 10;
	char *s = "abfda";

	sh_stack_clear(&is);
	sh_stack_clear(&ss);
	if (is.size != 0 || ss.size != 0)
		goto exit_test_fail;

	good = good && sh_stack_push(&is, &i) == 0;
	good = good && sh_stack_push(&ss, &s) == 0;
	if (!good)
		goto exit_test_fail;

	sh_stack_clear(&is);
	sh_stack_clear(&ss);
	good = good && sh_stack_init(&is, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_stack_init(&ss, _charptrcpy, sizeof(char *)) == 0;
	if (!good)
		goto exit_not_reinitializable;

	return 0;
exit_test_fail:
	fprintf(stderr, "clear: clear did not return object to a usable state\n");
	goto exit_fail;
exit_not_reinitializable:
	fprintf(stderr, "clear: clear did not return object to a reinitializable state\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_free(void)
{
	sh_stack_free(&is);
	sh_stack_free(&ss);
	/* multiple frees should not cause a problem */
	sh_stack_free(&is);
	sh_stack_free(&ss);
	sh_stack_free(&is);
	sh_stack_free(&ss);

	return 0;
}

int main(int argc, char **argv)
{
	bool good = true;

	srand(time(NULL));
	if (argc < 2)
	{
		if (generate_data(RANDOM_SIZE))
			goto exit_no_mem;
	}
	else
	{
		if (read_data(argv[1]))
			goto exit_bad_input;
	}

	good = good && test_init() == 0;
	good = good && test_push() == 0;
	good = good && test_top_and_pop() == 0;
	good = good && test_clear() == 0;
	good = good && test_free() == 0;

	cleanup_data();

	return good?EXIT_SUCCESS:EXIT_FAILURE;
exit_bad_input:
exit_no_mem:
	cleanup_data();
	return EXIT_FAILURE;
}
