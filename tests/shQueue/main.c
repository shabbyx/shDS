/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <shds.h>
#include "io.h"

#define DUMP_FILE "error_case.out"

int *idata = NULL;
size_t idata_count = 0;
char **sdata = NULL;
size_t sdata_count = 0;

/*
 * iq: queue of int * i.e. pointers to idata[.]
 * sq: queue of char * i.e. copies of sdata[.]
 */
shQueue iq = {0}, sq = {0};

static void _intptrcpy(void *to, void *from)
{
	*(int **)to = (int *)from;
}

static void _charptrcpy(void *to, void *from)
{
	*(char **)to = (char *)from;
}

/* initialize structure */
int test_init(void)
{
	bool good = true;

	sh_queue_free(&iq);
	sh_queue_free(&sq);

	good = good && sh_queue_init(&iq, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_queue_init(&sq, _charptrcpy, sizeof(char *)) == 0;
	/* it's ok to reinitialize with these attributes */
	good = good && sh_queue_init(&iq, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_queue_init(&sq, _charptrcpy, sizeof(char *)) == 0;
	good = good && sh_queue_init(&iq, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_queue_init(&sq, _charptrcpy, sizeof(char *)) == 0;
	good = good && sh_queue_init(&iq, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_queue_init(&sq, _charptrcpy, sizeof(char *)) == 0;

	if (!good)
		dump_data(DUMP_FILE);

	return good?0:-1;
}

/*
 * test whether structure data match the provided data (for integer test)
 * d is the expected results and n iq its count
 */
int test_idata(shQueue *q, int *d, size_t n)
{
	size_t i;
	shNode c;

	if (sh_list_get_first(q, &c) && q->size > 0)
		return -1;
	if (q->size != n)
		return -1;
	if (n == 0)
		return 0;

	i = 0;
	while (true)
	{
		if (i >= n)
			return -1;
		if (**(int **)c.data != d[i])
			return -1;
		++i;
		if (sh_list_get_next(&c, &c))
			break;
	}
	if (i != n)
		return -1;

	return 0;
}

/*
 * test whether structure data match the provided data (for string test)
 * d is the expected results and n iq its count
 */
int test_sdata(shQueue *q, char **d, size_t n)
{
	size_t i;
	shNode c;

	/*
	 * to test both directions, let this one go from tail to root
	 * while test_idata goes from root to tail
	 */
	if (sh_list_get_last(q, &c) && q->size > 0)
		return -1;
	if (q->size != n)
		return -1;
	if (n == 0)
		return 0;

	i = 0;
	while (true)
	{
		if (i >= n)
			return -1;
		if (strcmp(*(char **)c.data, d[n - 1 - i]) != 0)
			return -1;
		++i;
		if (sh_list_get_prev(&c, &c))
			break;
	}
	if (i != n)
		return -1;

	return 0;
}

int test_push(void)
{
	size_t i;
	bool good = true;

	for (i = 0; i < idata_count; ++i)
		if (sh_queue_push(&iq, &idata[i]))
			goto exit_no_mem;
	for (i = 0; i < sdata_count; ++i)
		if (sh_queue_push(&sq, sdata[i]))
			goto exit_no_mem;
	if (iq.size != idata_count || sq.size != sdata_count)
		goto exit_bad_push;
	good = good && test_idata(&iq, idata, idata_count) == 0;
	good = good && test_sdata(&sq, sdata, sdata_count) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_no_mem:
	fprintf(stderr, "push: out of memory\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "push: mismatch between real data and those in structure\n");
	goto exit_fail;
exit_bad_push:
	fprintf(stderr, "push: mismatch between size of real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_top_and_pop(void)
{
	size_t i;
	int **it;
	char **st;

	for (i = 0; i < idata_count; ++i)
	{
		if (iq.size == 0)
			goto exit_bad_pop;
		it = sh_queue_top(&iq);
		if (it == NULL)
			goto exit_test_internal_error;
		if (idata[i] != **it)
			goto exit_test_fail;
		sh_queue_pop(&iq);
	}
	for (i = 0; i < sdata_count; ++i)
	{
		if (sq.size == 0)
			goto exit_bad_pop;
		st = sh_queue_top(&sq);
		if (st == NULL)
			goto exit_test_internal_error;
		if (strcmp(sdata[i], *st) != 0)
			goto exit_test_fail;
		sh_queue_pop(&sq);
	}

	if (iq.size != 0 || sq.size != 0)
		goto exit_bad_pop;

	return 0;
exit_test_internal_error:
	fprintf(stderr, "top and pop: error in the internals of the test\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "top and pop: mismatch between real data and those in structure\n");
	goto exit_fail;
exit_bad_pop:
	fprintf(stderr, "top and pop: pop did not clear the queue properly\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_clear(void)
{
	bool good = true;
	int i = 10;
	char *s = "abfda";

	sh_queue_clear(&iq);
	sh_queue_clear(&sq);
	if (iq.size != 0 || sq.size != 0)
		goto exit_test_fail;

	good = good && sh_queue_push(&iq, &i) == 0;
	good = good && sh_queue_push(&sq, &s) == 0;
	if (!good)
		goto exit_test_fail;

	sh_queue_clear(&iq);
	sh_queue_clear(&sq);
	good = good && sh_queue_init(&iq, _intptrcpy, sizeof(int *)) == 0;
	good = good && sh_queue_init(&sq, _charptrcpy, sizeof(char *)) == 0;
	if (!good)
		goto exit_not_reinitializable;

	return 0;
exit_test_fail:
	fprintf(stderr, "clear: clear did not return object to a usable state\n");
	goto exit_fail;
exit_not_reinitializable:
	fprintf(stderr, "clear: clear did not return object to a reinitializable state\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_free(void)
{
	sh_queue_free(&iq);
	sh_queue_free(&sq);
	/* multiple frees should not cause a problem */
	sh_queue_free(&iq);
	sh_queue_free(&sq);
	sh_queue_free(&iq);
	sh_queue_free(&sq);

	return 0;
}

int main(int argc, char **argv)
{
	bool good = true;

	srand(time(NULL));
	if (argc < 2)
	{
		if (generate_data(RANDOM_SIZE))
			goto exit_no_mem;
	}
	else
	{
		if (read_data(argv[1]))
			goto exit_bad_input;
	}

	good = good && test_init() == 0;
	good = good && test_push() == 0;
	good = good && test_top_and_pop() == 0;
	good = good && test_clear() == 0;
	good = good && test_free() == 0;

	cleanup_data();

	return good?EXIT_SUCCESS:EXIT_FAILURE;
exit_bad_input:
exit_no_mem:
	cleanup_data();
	return EXIT_FAILURE;
}
