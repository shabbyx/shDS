/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the isplied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <shds.h>
#include "io.h"

#define DUMP_FILE "error_case.out"

int *idata = NULL;
size_t idata_count = 0;
char **sdata = NULL;
size_t sdata_count = 0;

/*
 * ih: heap of int * i.e. pointers to idata[.]
 * sh: heap of char * i.e. copies of sdata[.]
 */
shHeap ih = {0}, sh = {0};

static void _intptrcpy(void *to, void *from)
{
	*(int **)to = (int *)from;
}

static void _charptrcpy(void *to, void *from)
{
	*(char **)to = (char *)from;
}

int icmp(const void *i1, const void *i2)
{
	int i = **(int * const *)i1;
	int j = **(int * const *)i2;

	if (i == j)
		return 0;
	if (i < j)
		return -1;

	return 1;
}

int scmp(const void *s1, const void *s2)
{
	return strcmp(*(char * const *)s1, *(char * const *)s2);
}

typedef struct iheap_content
{
	int *key;
} iheap_content;

typedef struct sheap_content
{
	char *key;
} sheap_content;

int ihccmp(const void *i1, const void *i2)
{
	int i = *((const iheap_content *)i1)->key;
	int j = *((const iheap_content *)i2)->key;

	if (i == j)
		return 0;
	if (i < j)
		return -1;

	return 1;
}

int shccmp(const void *s1, const void *s2)
{
	return strcmp(((const sheap_content *)s1)->key, ((const sheap_content *)s2)->key);
}

iheap_content *ihc = NULL;
size_t ihc_count = 0;
sheap_content *shc = NULL;
size_t shc_count = 0;

/* initialize structure */
int test_init(void)
{
	bool good = true;

	sh_heap_free(&ih);
	sh_heap_free(&sh);
	good = good && sh_heap_init(&ih, _intptrcpy, sizeof(int *), icmp) == 0;
	good = good && sh_heap_init(&sh, _charptrcpy, sizeof(char *), scmp) == 0;
	/* it's ok to reinitialize with these attributes */
	good = good && sh_heap_init(&ih, _intptrcpy, sizeof(int *), icmp) == 0;
	good = good && sh_heap_init(&sh, _charptrcpy, sizeof(char *), scmp) == 0;
	good = good && sh_heap_init(&ih, _intptrcpy, sizeof(int *), icmp) == 0;
	good = good && sh_heap_init(&sh, _charptrcpy, sizeof(char *), scmp) == 0;
	good = good && sh_heap_init(&ih, _intptrcpy, sizeof(int *), icmp) == 0;
	good = good && sh_heap_init(&sh, _charptrcpy, sizeof(char *), scmp) == 0;
	if (!good)
		dump_data(DUMP_FILE);

	return good?0:-1;
}

typedef struct test_append_data
{
	void *array;
	size_t size;
} test_append_data;

/* TODO: Same as shMap, test with returning false also */
bool test_idata_append(shNode *n, void *extra)
{
	iheap_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	array[size].key = *(int **)n->key;
	++((test_append_data *)extra)->size;

	return true;
}

bool test_sdata_append(shNode *n, void *extra)
{
	sheap_content *array = ((test_append_data *)extra)->array;
	size_t size = ((test_append_data *)extra)->size;

	array[size].key = *(char **)n->key;
	++((test_append_data *)extra)->size;

	return true;
}

/*
 * test whether structure data match the provided data (for integer test)
 * d is the expected results and n is its count
 */
int test_idata(shHeap *h, iheap_content *d, size_t n, bool use_for_each)
{
	size_t i;
	test_append_data ad;
	iheap_content *isc2;

	if (h->size != n)
		return -1;
	isc2 = malloc(n * sizeof *isc2);
	if (isc2 == NULL)
		goto exit_no_mem;

	ad.array = isc2;
	ad.size = 0;
	if (use_for_each)
	{
		if (sh_heap_for_each(h, test_idata_append, &ad) != 0)
			goto exit_bad_for_each;
	}
	else
	{
		shNode n;
		int ret;
		for (ret = sh_heap_get_first(h, &n); !ret; ret = sh_heap_get_next(&n, &n))
			test_idata_append(&n, &ad);
	}
	if (ad.size != n)
		goto exit_fail;
	qsort(isc2, n, sizeof *isc2, ihccmp);
	for (i = 0; i < n; ++i)
		if (*d[i].key != *isc2[i].key)
			goto exit_fail;

	free(isc2);
	return 0;
exit_bad_for_each:
	fprintf(stderr, "test_idata for each returned nonzero\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
exit_fail:
	free(isc2);
	return -1;
}

/*
 * test whether structure data match the provided data (for string test)
 * d is the expected results and n is its count
 */
int test_sdata(shHeap *h, sheap_content *d, size_t n, bool use_for_each)
{
	size_t i;
	test_append_data ad;
	sheap_content *ssc2;

	if (h->size != n)
		return -1;
	ssc2 = malloc(n * sizeof *ssc2);
	if (ssc2 == NULL)
		goto exit_no_mem;

	ad.array = ssc2;
	ad.size = 0;
	if (use_for_each)
	{
		if (sh_heap_for_each(h, test_sdata_append, &ad) != 0)
			goto exit_bad_for_each;
	}
	else
	{
		shNode n;
		int ret;
		for (ret = sh_heap_get_last(h, &n); !ret; ret = sh_heap_get_prev(&n, &n))
			test_sdata_append(&n, &ad);
	}
	if (ad.size != n)
		goto exit_fail;
	qsort(ssc2, n, sizeof *ssc2, shccmp);
	for (i = 0; i < n; ++i)
		if (strcmp(d[i].key, ssc2[i].key) != 0)
			goto exit_fail;

	free(ssc2);
	return 0;
exit_bad_for_each:
	fprintf(stderr, "test_sdata for each returned nonzero\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
exit_fail:
	free(ssc2);
	return -1;
}

int test_insert(void)
{
	size_t i;
	bool good = true;

	ihc_count = 0;
	shc_count = 0;
	for (i = 0; i < idata_count; ++i)
	{
		shNode n;

		if (sh_heap_insert(&ih, &idata[i], &n))
			goto exit_bad_insert;
		if (*(int **)n.key != &idata[i])
			goto exit_bad_ref;
		ihc[ihc_count++].key = &idata[i];
	}
	for (i = 0; i < sdata_count; ++i)
	{
		shNode n;

		if (sh_heap_insert(&sh, sdata[i], &n))
			goto exit_bad_insert;
		if (*(char **)n.key != sdata[i])
			goto exit_bad_ref;
		shc[shc_count++].key = sdata[i];
	}

	qsort(ihc, ihc_count, sizeof *ihc, ihccmp);
	qsort(shc, shc_count, sizeof *shc, shccmp);
	good = good && test_idata(&ih, ihc, ihc_count, true) == 0;
	good = good && test_sdata(&sh, shc, shc_count, false) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_bad_insert:
	fprintf(stderr, "insert: insert failed\n");
	goto exit_fail;
exit_bad_ref:
	fprintf(stderr, "insert: reference returned by insert did not match inserted node\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "insert: mismatch between real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_duplicate(void)
{
	bool good = true;
	shHeap ih1, sh1, ih2, sh2;

	/* test empty copy */
	sh_heap_init(&ih1, _intptrcpy, sizeof(int *), icmp);
	sh_heap_init(&sh1, _charptrcpy, sizeof(char *), scmp);
	good = good && sh_heap_duplicate(&ih1, &ih2) == 0;
	good = good && sh_heap_duplicate(&sh1, &sh2) == 0;
	if (!good)
		goto exit_bad_empty_duplicate;

	good = good && test_idata(&ih2, NULL, 0, true) == 0;
	good = good && test_sdata(&sh2, NULL, 0, false) == 0;
	if (!good)
		goto exit_test_empty;

	sh_heap_clear(&ih2);
	sh_heap_clear(&sh2);

	/* test full copy */
	good = good && sh_heap_duplicate(&ih, &ih2) == 0;
	good = good && sh_heap_duplicate(&sh, &sh2) == 0;
	if (!good)
		goto exit_bad_full_duplicate;

	good = good && test_idata(&ih2, ihc, ihc_count, false) == 0;
	good = good && test_sdata(&sh2, shc, shc_count, true) == 0;
	if (!good)
		goto exit_test_full;

	goto exit_cleanup;
exit_bad_empty_duplicate:
	fprintf(stderr, "duplicate: bad duplication of empty heap\n");
	goto exit_fail;
exit_test_empty:
	fprintf(stderr, "duplicate: duplicated empty array data mismatch\n");
	goto exit_fail;
exit_bad_full_duplicate:
	fprintf(stderr, "duplicate: bad duplication of non-empty heap\n");
	goto exit_fail;
exit_test_full:
	fprintf(stderr, "duplicate: duplicated full array data mismatch\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
exit_cleanup:
	sh_heap_free(&ih1);
	sh_heap_free(&sh1);
	sh_heap_free(&ih2);
	sh_heap_free(&sh2);

	return good?0:-1;
}

int test_decrease_key(void)
{
	size_t i;
	bool good = true;
	shNode *inodes = NULL;
	shNode *snodes = NULL;

	inodes = malloc(idata_count * sizeof *inodes);
	snodes = malloc(sdata_count * sizeof *snodes);
	if (inodes == NULL || snodes == NULL)
		goto exit_no_mem;

	sh_heap_clear(&ih);
	sh_heap_clear(&sh);
	for (i = 0; i < idata_count; ++i)
		if (sh_heap_insert(&ih, &idata[i], &inodes[i]))
			goto exit_bad_insert;
	for (i = 0; i < sdata_count; ++i)
		if (sh_heap_insert(&sh, sdata[i], &snodes[i]))
			goto exit_bad_insert;

	for (i = 0; i < idata_count; ++i)
		if (sh_heap_decrease_key(&inodes[i], &idata[i]))
			goto exit_bad_decrease_key;
	for (i = 0; i < sdata_count; ++i)
	{
		size_t len = strlen(sdata[i]);
		unsigned int pos = rand() % len;

		if (sdata[i][pos] > 'a' && sdata[i][pos] <= 'z')
			sdata[i][pos] -= rand() % (sdata[i][pos] - 'a' + 1);
		else if (sdata[i][pos] > 'A' && sdata[i][pos] <= 'Z')
			sdata[i][pos] -= rand() % (sdata[i][pos] - 'A' + 1);

		if (sh_heap_decrease_key(&snodes[i], sdata[i]))
			goto exit_bad_decrease_key;
	}

	qsort(ihc, ihc_count, sizeof *ihc, ihccmp);
	qsort(shc, shc_count, sizeof *shc, shccmp);
	good = good && test_idata(&ih, ihc, ihc_count, false) == 0;
	good = good && test_sdata(&sh, shc, shc_count, true) == 0;
	if (!good)
		goto exit_test_fail;

	free(inodes);
	free(snodes);

	sh_heap_clear(&ih);
	sh_heap_clear(&sh);
	return test_insert();
exit_bad_insert:
	fprintf(stderr, "decrease_key: insert failed\n");
	goto exit_fail;
exit_bad_decrease_key:
	fprintf(stderr, "decrease_key: decrease_key failed\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "decrease_key: mismatch between real data and those in structure\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
exit_fail:
	dump_data(DUMP_FILE);
	free(inodes);
	free(snodes);
	return -1;
}

int test_delete(void)
{
	size_t i;
	bool good = true;
	shNode *inodes = NULL;
	shNode *snodes = NULL;

	inodes = malloc(idata_count * sizeof *inodes);
	snodes = malloc(sdata_count * sizeof *snodes);
	if (inodes == NULL || snodes == NULL)
		goto exit_no_mem;

	sh_heap_clear(&ih);
	sh_heap_clear(&sh);
	ihc_count = 0;
	shc_count = 0;
	for (i = 0; i < idata_count; ++i)
	{
		if (sh_heap_insert(&ih, &idata[i], &inodes[i]))
			goto exit_bad_insert;
		ihc[ihc_count++].key = &idata[i];
	}
	for (i = 0; i < sdata_count; ++i)
	{
		if (sh_heap_insert(&sh, sdata[i], &snodes[i]))
			goto exit_bad_insert;
		shc[shc_count++].key = sdata[i];
	}

	for (i = 0; i < idata_count / 2; ++i)
		if (sh_heap_delete(&inodes[i]))
			goto exit_bad_delete;
	for (i = 0; i < sdata_count / 2; ++i)
		if (sh_heap_delete(&snodes[i]))
			goto exit_bad_delete;

	if (ih.size != idata_count - idata_count / 2 || sh.size != sdata_count - sdata_count / 2)
		goto exit_bad_size;

	qsort(ihc + ihc_count / 2, ihc_count - ihc_count / 2, sizeof *ihc, ihccmp);
	qsort(shc + shc_count / 2, shc_count - shc_count / 2, sizeof *shc, shccmp);
	good = good && test_idata(&ih, ihc + ihc_count / 2, ihc_count - ihc_count / 2, false) == 0;
	good = good && test_sdata(&sh, shc + shc_count / 2, shc_count - shc_count / 2, true) == 0;
	if (!good)
		goto exit_test_fail;

	for (i = idata_count / 2; i < idata_count; ++i)
		if (sh_heap_delete(&inodes[i]))
			goto exit_bad_delete;
	for (i = sdata_count / 2; i < sdata_count; ++i)
		if (sh_heap_delete(&snodes[i]))
			goto exit_bad_delete;

	if (ih.size != 0 || sh.size != 0)
		goto exit_bad_size;

	free(inodes);
	free(snodes);

	return test_insert();
exit_bad_insert:
	fprintf(stderr, "delete: insert failed\n");
	goto exit_fail;
exit_bad_delete:
	fprintf(stderr, "delete: delete failed\n");
	goto exit_fail;
exit_bad_size:
	fprintf(stderr, "delete: incorrect size\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "delete: mismatch between real data and those in structure\n");
	goto exit_fail;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
exit_fail:
	dump_data(DUMP_FILE);
	free(inodes);
	free(snodes);
	return -1;
}


int test_delete_min(void)
{
	size_t i;

	for (i = 0; i < idata_count; ++i)
	{
		shNode m;

		if (sh_heap_find_min(&ih, &m))
			goto exit_bad_find_min;
		if (**(int **)m.key != *ihc[i].key)
			goto exit_bad_sort;
		sh_heap_delete_min(&ih);
		if (ih.size != idata_count - i - 1)
			goto exit_bad_size;
	}
	for (i = 0; i < sdata_count; ++i)
	{
		shNode m;

		if (sh_heap_find_min(&sh, &m))
			goto exit_bad_find_min;
		if (strcmp(*(char **)m.key, shc[i].key) != 0)
			goto exit_bad_sort;
		sh_heap_delete_min(&sh);
		if (sh.size != sdata_count - i - 1)
			goto exit_bad_size;
	}

	return test_insert();
exit_bad_find_min:
	fprintf(stderr, "delete_min: find_min failed\n");
	goto exit_fail;
exit_bad_sort:
	fprintf(stderr, "delete_min: data are not being drawn sorted\n");
	goto exit_fail;
exit_bad_size:
	fprintf(stderr, "delete_min: incorrect size\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_merge(void)
{
	size_t i;
	bool good = true;
	shHeap ih2;
	shHeap sh2;

	sh_heap_init(&ih2, _intptrcpy, sizeof(int *), icmp);
	sh_heap_init(&sh2, _charptrcpy, sizeof(char *), scmp);

	ihc_count = 0;
	shc_count = 0;
	sh_heap_clear(&ih);
	sh_heap_clear(&sh);

	/* empty and empty */
	if (sh_heap_merge(&ih, &ih2))
		goto exit_bad_empty_empty;
	if (sh_heap_merge(&sh, &sh2))
		goto exit_bad_empty_empty;

	for (i = 0; i < idata_count / 2; ++i)
	{
		shNode n;

		if (sh_heap_insert(&ih, &idata[i], &n))
			goto exit_bad_insert;
		if (*(int **)n.key != &idata[i])
			goto exit_bad_ref;
		ihc[ihc_count++].key = &idata[i];
	}
	for (i = 0; i < sdata_count / 2; ++i)
	{
		shNode n;

		if (sh_heap_insert(&sh, sdata[i], &n))
			goto exit_bad_insert;
		if (*(char **)n.key != sdata[i])
			goto exit_bad_ref;
		shc[shc_count++].key = sdata[i];
	}

	/* non empty and empty */
	if (sh_heap_merge(&ih, &ih2))
		goto exit_bad_nonempty_empty;
	if (sh_heap_merge(&sh, &sh2))
		goto exit_bad_nonempty_empty;

	qsort(ihc, ihc_count, sizeof *ihc, ihccmp);
	qsort(shc, shc_count, sizeof *shc, shccmp);
	good = good && test_idata(&ih, ihc, ihc_count, true) == 0;
	good = good && test_sdata(&sh, shc, shc_count, false) == 0;
	if (!good)
		goto exit_test_fail;

	/* empty and non empty */
	if (sh_heap_merge(&ih2, &ih))
		goto exit_bad_empty_nonempty;
	if (sh_heap_merge(&sh2, &sh))
		goto exit_bad_empty_nonempty;

	good = good && test_idata(&ih2, ihc, ihc_count, false) == 0;
	good = good && test_sdata(&sh2, shc, shc_count, true) == 0;
	if (!good)
		goto exit_test_fail;

	for (i = idata_count / 2; i < idata_count; ++i)
	{
		shNode n;

		if (sh_heap_insert(&ih, &idata[i], &n))
			goto exit_bad_insert;
		if (*(int **)n.key != &idata[i])
			goto exit_bad_ref;
		ihc[ihc_count++].key = &idata[i];
	}
	for (i = sdata_count / 2; i < sdata_count; ++i)
	{
		shNode n;

		if (sh_heap_insert(&sh, sdata[i], &n))
			goto exit_bad_insert;
		if (*(char **)n.key != sdata[i])
			goto exit_bad_ref;
		shc[shc_count++].key = sdata[i];
	}

	/* non empty and non empty */
	if (sh_heap_merge(&ih, &ih2))
		goto exit_bad_nonempty_nonempty;
	if (sh_heap_merge(&sh, &sh2))
		goto exit_bad_nonempty_nonempty;

	qsort(ihc, ihc_count, sizeof *ihc, ihccmp);
	qsort(shc, shc_count, sizeof *shc, shccmp);
	good = good && test_idata(&ih, ihc, ihc_count, true) == 0;
	good = good && test_sdata(&sh, shc, shc_count, false) == 0;
	if (!good)
		goto exit_test_fail;

	return 0;
exit_bad_insert:
	fprintf(stderr, "merge: insert failed\n");
	goto exit_fail;
exit_bad_ref:
	fprintf(stderr, "merge: reference returned by insert did not match inserted node\n");
	goto exit_fail;
exit_bad_empty_empty:
	fprintf(stderr, "merge: empty-empty merge failed\n");
	goto exit_fail;
exit_bad_nonempty_empty:
	fprintf(stderr, "merge: nonempty-empty merge failed\n");
	goto exit_fail;
exit_bad_empty_nonempty:
	fprintf(stderr, "merge: empty-nonempty merge failed\n");
	goto exit_fail;
exit_bad_nonempty_nonempty:
	fprintf(stderr, "merge: nonempty-nonempty merge failed\n");
	goto exit_fail;
exit_test_fail:
	fprintf(stderr, "merge: mismatch between real data and those in structure\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

static bool _dup_int_heap(shNode *n, void *m)
{
	return sh_heap_insert(m, *(int **)n->key) == 0;
}

static bool _dup_str_heap(shNode *n, void *m)
{
	return sh_heap_insert(m, *(char **)n->key) == 0;
}

static int duplicate_heap(shHeap *ih1, shHeap *ih2, shHeap *sh1, shHeap *sh2)
{
	sh_heap_clear(ih1);
	sh_heap_clear(sh1);

	return sh_heap_for_each(ih2, _dup_int_heap, ih1)
		+ sh_heap_for_each(sh2, _dup_str_heap, sh1);
}

int test_compare(void)
{
	shHeap ih1;
	shHeap sh1;
	bool good = true;
	int newint = INT_RANGE + 10;
	char newstr[] = "zzzzzz";

	sh_heap_init(&ih1, _intptrcpy, sizeof(int *), icmp);
	sh_heap_init(&sh1, _charptrcpy, sizeof(char *), scmp);

	if (sh_heap_compare(&ih, &ih) != 0 || sh_heap_compare(&sh, &sh) != 0
		|| sh_heap_compare_lexic(&ih, &ih) != 0 || sh_heap_compare_lexic(&sh, &sh) != 0)
		goto exit_bad_equal;

	if (duplicate_heap(&ih1, &ih, &sh1, &sh))
		goto exit_no_mem;
	if (sh_heap_insert(&ih1, &newint) || sh_heap_insert(&sh1, newstr))
		goto exit_no_mem;

	if (sh_heap_compare(&ih, &ih1) == 0 || sh_heap_compare(&sh, &sh1) == 0)
		goto exit_bad_inequal;
	if (sh_heap_compare_lexic(&ih, &ih1) != -1 || sh_heap_compare_lexic(&ih1, &ih) != 1
		|| sh_heap_compare_lexic(&sh, &sh1) != -1 || sh_heap_compare_lexic(&sh1, &sh) != 1)
		goto exit_bad_lexic;

	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "compare: out of memory\n");
	goto exit_fail;
exit_bad_equal:
	fprintf(stderr, "compare: failed to recognize equal heaps\n");
	goto exit_fail;
exit_bad_inequal:
	fprintf(stderr, "compare: failed to recognize inequal heaps\n");
	goto exit_fail;
exit_bad_lexic:
	fprintf(stderr, "compare: incorrect lexicographical comparison\n");
	goto exit_fail;
exit_fail:
	dump_data(DUMP_FILE);
	good = false;
exit_cleanup:
	sh_heap_free(&ih1);
	sh_heap_free(&sh1);
	return good?0:-1;
}

int test_clear(void)
{
	bool good = true;
	int i = 10;
	char *s = "abfda";

	sh_heap_clear(&ih);
	sh_heap_clear(&sh);

	if (ih.size != 0 || sh.size != 0)
		goto exit_test_fail;
	good = good && sh_heap_insert(&ih, &i) == 0;
	good = good && sh_heap_insert(&sh, s) == 0;
	if (!good)
		goto exit_test_fail;
	sh_heap_clear(&ih);
	sh_heap_clear(&sh);
	good = good && sh_heap_init(&ih, _intptrcpy, sizeof(int *), ihccmp) == 0;
	good = good && sh_heap_init(&sh, _charptrcpy, sizeof(char *), shccmp) == 0;
	if (!good)
		goto exit_not_reinitializable;

	return 0;
exit_test_fail:
	fprintf(stderr, "clear: clear did not return object to a usable state\n");
	goto exit_fail;
exit_not_reinitializable:
	fprintf(stderr, "clear: clear did not return object to a reinitializable state\n");
exit_fail:
	dump_data(DUMP_FILE);
	return -1;
}

int test_free(void)
{
	sh_heap_free(&ih);
	sh_heap_free(&sh);
	/* multiple frees should not cause a problem */
	sh_heap_free(&ih);
	sh_heap_free(&sh);
	sh_heap_free(&ih);
	sh_heap_free(&sh);
	return 0;
}

int main(int argc, char **argv)
{
	bool good = true;

	srand(time(NULL));
	if (argc < 2)
	{
		if (generate_data(RANDOM_SIZE))
			goto exit_no_mem;
	}
	else
	{
		if (read_data(argv[1]))
			goto exit_bad_input;
	}

	ihc = malloc(idata_count * sizeof *ihc);
	shc = malloc(sdata_count * sizeof *shc);
	if (ihc == NULL || shc == NULL)
		goto exit_no_mem;

	good = good && test_init() == 0;
	good = good && test_insert() == 0;
	good = good && test_duplicate() == 0;
	good = good && test_decrease_key() == 0;
	good = good && test_delete() == 0;
	good = good && test_delete_min() == 0;
	good = good && test_merge() == 0;
	good = good && test_compare() == 0;
	good = good && test_clear() == 0;
	good = good && test_free() == 0;

	cleanup_data();
	free(ihc);
	free(shc);

	return good?EXIT_SUCCESS:EXIT_FAILURE;
exit_bad_input:
exit_no_mem:
	cleanup_data();
	free(ihc);
	free(shc);
	return EXIT_FAILURE;
}
