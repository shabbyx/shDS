/*
 * Copyright (C) 2013-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NDEBUG
# include <stdio.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "shheap.h"
#include "shvector.h"

/*
 * the pairing heap is implemented with circular linked list variant.
 * In this variant, each node has a reference to one of its children,
 * and the children are linked both forward and backward.  The two
 * directions are necessary for optimal decrease_key and delete operations.
 */
typedef struct shHeapNode
{
	struct shHeapNode *parent;
	struct shHeapNode *child;
	struct shHeapNode *next;
	struct shHeapNode *prev;
	char key[];
} shHeapNode;

int sh_heap_init(shHeap *h, sh_assign assign, size_t item_size, sh_compare cmp)
{
	if (h == NULL || assign == NULL || item_size == 0)
		return -1;

	*h = (shHeap){
		.assign = assign,
		.compare = cmp,
		.item_size = item_size,
	};

	return 0;
}

/*
 * The scheme for traversing the nodes is as follows:
 *
 *               A
 *              /
 *         _-> B -> E -> F -> G -_
 *        <___/______________/____>
 *           /               |
 *      _-> C -> D -_    _-> H -> I -_
 *     <_____________>  <_____________>
 *
 * Therefore:
 *
 * - first is A, i.e. the root
 * - last is the deepest last child of the last child
 * - next is the child if it has any, otherwise its next if it has any, if not the first parent that has next
 * - prev is the deepest last child of its prev if it has any, else its prev if it has any, else its parent
 */
int sh_heap_get_first(shHeap *h, shNode *n)
{
	if (h == NULL || h->root == NULL || n == NULL)
		return -1;

	*n = (shNode){
		.key = ((shHeapNode *)h->root)->key,
		.owner = h,
		.origin = h->root
	};

	return 0;
}

static void _descend_to_last_child(shHeap *h, shHeapNode *cur, shNode *n)
{
	/* move to cur's last child until reaching a leaf */
	while (cur->child != NULL)
		cur = cur->child->prev;

	*n = (shNode){
		.key = cur->key,
		.owner = h,
		.origin = cur
	};
}

static int _ascend_to_first_father_next(shHeap *h, shHeapNode *cur, shNode *n)
{
	/* move to cur's parent until it has a next sibling */
	while (cur->parent != NULL && cur->next == cur->parent->child)
		cur = cur->parent;

	if (cur->parent == NULL)
		return -1;

	*n = (shNode){
		.key = cur->next->key,
		.owner = h,
		.origin = cur->next
	};

	return 0;
}

int sh_heap_get_last(shHeap *h, shNode *n)
{
	if (h == NULL || h->root == NULL || n == NULL)
		return -1;

	_descend_to_last_child(h, h->root, n);

	return 0;
}

int sh_heap_get_next(shNode *n, shNode *next)
{
	shHeapNode *this;

	if (n == NULL || next == NULL)
		return -1;

	this = n->origin;

	/* if it has children, then next is first child, else _ascend_to_first_father_next */
	if (this->child != NULL)
		*next = (shNode){
			.key = this->child->key,
			.owner = n->owner,
			.origin = this->child
		};
	else
		return _ascend_to_first_father_next(n->owner, this, next);

	return 0;
}

int sh_heap_get_prev(shNode *n, shNode *prev)
{
	shHeapNode *this;

	if (n == NULL || prev == NULL)
		return -1;

	this = n->origin;
	if (this->parent == NULL)
		return -1;

	/* if the first of the children list, then prev is parent, else _descend_to_last_child of prev */
	if (this->parent->child == this)
		*prev = (shNode){
			.key = this->parent->key,
			.owner = n->owner,
			.origin = this->parent
		};
	else
		_descend_to_last_child(n->owner, this->prev, prev);

	return 0;
}

static void _add_child(shHeapNode *parent, shHeapNode *new)
{
	shHeapNode *child = parent->child;

	/* TODO: is this always correct? */
	assert(new->next == new);
	assert(new->prev == new);

	/* put new node in children's linked list */
	if (child == NULL)
	{
		new->next = new;
		new->prev = new;
		parent->child = new;
	}
	else
	{
		new->next = child;
		new->prev = child->prev;
		new->prev->next = new;
		new->next->prev = new;
	}

	new->parent = parent;
}

static shHeapNode *_merge_nodes(shHeap *h, shHeapNode *r1, shHeapNode *r2)
{
	assert(r1 != r2);

	/* add the not-smaller of r1 and r2 as the child of the other one */
	if (h->compare(r1->key, r2->key) < 0)
	{
		_add_child(r1, r2);
		return r1;
	}
	else
	{
		_add_child(r2, r1);
		return r2;
	}
}

static void _merge_no_check(shHeap *h, shHeap *other)
{
	/* if other is empty, merge is done */
	if (other->size == 0)
		goto clear_other;

	/* if h is empty, move other to h */
	if (h->size == 0)
	{
		h->root = other->root;
		h->size = other->size;
		goto clear_other;
	}

	/* add the not-smaller of h->root and other->root as the child of the other one */
	h->root = _merge_nodes(h, h->root, other->root);
	h->size += other->size;

clear_other:
	other->root = NULL;
	other->size = 0;
}

static shHeapNode *_create_node(shHeap *h, void *k)
{
	shHeapNode *n = malloc(sizeof *n + h->item_size);

	if (n == NULL)
		return NULL;

	*n = (shHeapNode){
		.next = n,
		.prev = n
	};
	h->assign(n->key, k);

	return n;
}

int (sh_heap_insert)(shHeap *h, void *item, shNode *ref, ...)
{
	shHeap temp;
	shHeapNode *new;

	if (h == NULL || h->assign == NULL || h->compare == NULL)
		return -1;

	new = _create_node(h, item);
	if (new == NULL)
		return -1;

	temp = (shHeap){
		.root = new,
		.size = 1
	};
	_merge_no_check(h, &temp);

	if (ref != NULL)
		*ref = (shNode){
			.key = new->key,
			.owner = h,
			.origin = new
		};

	return 0;
}

int sh_heap_find_min(shHeap *h, shNode *n)
{
	if (h == NULL || h->root == NULL || h->compare == NULL || n == NULL)
		return -1;

	*n = (shNode){
		.key = ((shHeapNode *)h->root)->key,
		.owner = h,
		.origin = h->root
	};

	return 0;
}

static shHeapNode *_delete_min(shHeap *h, shHeapNode *n)
{
	shHeapNode *new_root = NULL;
	shHeapNode *child;
	shHeapNode *cur;
	shHeapNode *next;

	assert(n->parent == NULL);
	assert(n->next == n);
	assert(n->prev == n);

	child = n->child;
	if (child == NULL)
		goto exit_done;

	/* TODO: try back-to-front and check timing to see if it makes any difference */
	cur = child;
	next = cur->next;

	/* if only one child, it becomes the root */
	if (cur == next)
	{
		new_root = cur;
		goto exit_done;
	}

	do
	{
		shHeapNode *pair;
		shHeapNode *after;

		after = next->next;

		/* put both cur and next on a list of their own */
		cur->next = cur->prev = cur;
		next->next = next->prev = next;

		pair = _merge_nodes(h, cur, next);
		new_root = new_root == NULL?pair:_merge_nodes(h, new_root, pair);

		cur = after;
		next = cur->next;
	} while (cur != child && cur->next != child);

	/* if cur is not child, but cur->next is, then the number of children where odd */
	if (cur != child && cur->next == child)
	{
		/* put it on a list of its own */
		cur->next = cur->prev = cur;
		new_root = _merge_nodes(h, new_root, cur);
	}

exit_done:
	free(n);
	if (new_root)
		new_root->parent = NULL;
	return new_root;
}

void sh_heap_delete_min(shHeap *h)
{
	if (h == NULL)
		return;

	h->root = _delete_min(h, h->root);
	--h->size;
}

int sh_heap_merge(shHeap *h, shHeap *other)
{
	if (h == NULL || other == NULL || h == other)
		return -1;

	if (h->item_size != other->item_size || h->compare == NULL)
		return -1;

	_merge_no_check(h, other);

	return 0;
}

static void _cut_from_parent(shHeapNode *n)
{
	shHeapNode *next = n->next;
	shHeapNode *prev = n->prev;

	if (n == next)
	{
		/* only child */
		assert(next == prev);
		n->parent->child = NULL;
	}
	else
	{
		/* make sure parent doesn't point to this node */
		n->parent->child = prev;
		/* remove from parent's list */
		next->prev = prev;
		prev->next = next;
		/* make it alone in a list by itself */
		n->next = n->prev = n;
	}
	n->parent = NULL;
}

int sh_heap_decrease_key(shNode *n, void *new_value)
{
	shHeap *h;
	shHeapNode *cur;

	if (n == NULL)
		return -1;

	h = n->owner;
	cur = n->origin;

	/* cut node from parent, if not root */
	if (h->root != cur)
		_cut_from_parent(cur);

	/* set the new value */
	h->assign(cur->key, new_value);

	/* merge the two sub-trees, if not root */
	if (h->root != cur)
		h->root = _merge_nodes(h, h->root, cur);

	return 0;
}

int sh_heap_delete(shNode *n)
{
	shHeap *h;
	shHeapNode *cur;

	if (n == NULL)
		return -1;

	h = n->owner;
	cur = n->origin;

	/* if root, then delete is the same as delete_min */
	if (h->root == cur)
	{
		h->root = _delete_min(h, h->root);
		goto exit_common;
	}

	/* cut node from parent */
	_cut_from_parent(cur);

	/* perform delete min on the new sub-tree */
	cur = _delete_min(h, cur);

	/* merge the two sub-trees, if there is a sub-tree at all */
	if (cur)
		h->root = _merge_nodes(h, h->root, cur);

exit_common:
	--h->size;
	return 0;
}

static int _for_each(shHeap *h, sh_callback c, void *extra)
{
	shNode n;
	int ret;

	if (h == NULL)
		return -1;
	if (h->root == NULL)
		return 0;

	for (ret = sh_heap_get_first(h, &n); ret == 0; ret = sh_heap_get_next(&n, &n))
		if (!c(&n, extra))
			return -1;

	return 0;
}

int (sh_heap_for_each)(shHeap *h, sh_callback c, void *extra, ...)
{
	/* instead of a queue, an array is used because the maximum size of the queue is known */
	shHeapNode **nodes;
	int current = 0;
	int end = 0;
	int ret = 0;

	if (h == NULL)
		return -1;
	if (h->root == NULL)
		return 0;

	nodes = malloc(h->size * sizeof *nodes);
	if (nodes == NULL)
		goto exit_no_mem;

	nodes[0] = h->root;
	end = 1;
	while (current < end)
	{
		shHeapNode *n = nodes[current++];
		shHeapNode *child = n->child;
		shHeapNode *cur;
		shNode node;

		if (child)
		{
			cur = child;
			do
			{
				nodes[end++] = cur;
				cur = cur->next;
			} while (cur != child);
		}

		node = (shNode){
			.key = n->key,
			.owner = h,
			.origin = n
		};
		if (!c(&node, extra))
		{
			ret = -1;
			break;
		}
	}

	free(nodes);

	return ret;
exit_no_mem:
	/* malloc failed try iterating using get_first/next */
	return _for_each(h, c, extra);
}

struct dup_data
{
	shHeapNode *cur;
	shHeapNode *dup_parent;
	shHeapNode *dup;
	bool last_child;
};

int sh_heap_duplicate(shHeap *h, shHeap *h2)
{
	struct dup_data *nodes;
	int current = 0;
	int i, end = 0;

	if (h == NULL || h2 == NULL)
		return -1;

	sh_heap_init_similar(h2, h);

	if (h->root == NULL)
		return 0;

	/* instead of a queue, an array is used because the maximum size of the queue is known */
	nodes = malloc(h->size * sizeof *nodes);
	if (nodes == NULL)
		goto exit_no_mem_for_bfs;

	nodes[0] = (struct dup_data){
		.cur = h->root,
	};
	end = 1;
	while (current < end)
	{
		struct dup_data *d = &nodes[current];
		shHeapNode *child = d->cur->child;
		shHeapNode *cur;
		shHeapNode *n = malloc(sizeof *n + h->item_size);

		if (n == NULL)
			goto exit_no_mem;
		/* store n in case above if later fails and cleanup is needed */
		d->dup = n;

		*n = (shHeapNode){
			.parent = d->dup_parent,
		};
		memcpy(n->key, d->cur->key, h->item_size);

		/* set it as parent's child, if first child */
		if (n->parent && n->parent->child == NULL)
			n->parent->child = n;

		/*
		 * if has children, put them in queue.
		 * The children, will take care of setting everything else.
		 */
		if (child)
		{
			cur = child;
			do
			{
				nodes[end++] = (struct dup_data){
					.cur = cur,
					.dup_parent = n,
					.last_child = cur->next == child,
				};
				cur = cur->next;
			} while (cur != child);
		}

		/*
		 * the siblings are placed consecutively in the BFS queue.
		 * That's how each node can find and set the pointers for its previous node.
		 * The first node identifies itself by being its parent's child.  The last
		 * node also fixes the link for the first node, i.e. its parent's child.
		 */

		if (n->parent == NULL)
			n->next = n->prev = n;
		else
		{
			if (n != n->parent->child)
			{
				struct dup_data *prev_sibling = &nodes[current - 1];

				assert(current > 0);

				prev_sibling->dup->next = n;
				n->prev = prev_sibling->dup;
			}

			if (d->last_child)
			{
				n->parent->child->prev = n;
				n->next = n->parent->child;
			}
		}

		++current;
	}

	h2->root = nodes[0].dup;
	h2->size = h->size;

	free(nodes);

	h2->size = h->size;

#ifndef NDEBUG
	sh_heap_internal_correctness_test(h2);
#endif

	return 0;
exit_no_mem:
	for (i = 0; i < current; ++i)
		free(nodes[i].dup);
exit_no_mem_for_bfs:
	return -1;
}

struct to_vector_item
{
	void *key;
	sh_compare original_cmp;
};

struct to_vector_data
{
	shVector v;
	sh_compare original_cmp;
	bool first;
};

static void _itemcpy(void *to, void *from)
{
	*(struct to_vector_item *)to = *(struct to_vector_item *)from;
}

static int _itemcmp(const void *a, const void *b)
{
	const struct to_vector_item *first = a;
	const struct to_vector_item *second = b;

	return first->original_cmp(first->key, second->key);
}

static bool _to_vector(shNode *n, void *extra)
{
	struct to_vector_data *vd = extra;
	struct to_vector_item item = {
		.key = n->key,
		.original_cmp = vd->original_cmp
	};

	if (!vd->first)
		sh_vector_append(&vd->v, &item);

	vd->first = false;
	return true;
}

static int _sort_and_compare_lexic(const shHeap *h, const shHeap *h2)
{
	int ret;
	struct to_vector_data hvd = {
		.first = true,
		.original_cmp = h->compare
	};
	struct to_vector_data h2vd = {
		.first = true,
		.original_cmp = h2->compare
	};

	/* quick empty or minimum check */
	if (h->size == 0)
		return h2->size == 0?0:1;
	ret = h->compare(((shHeapNode *)h->root)->key, ((shHeapNode *)h2->root)->key);
	if (ret)
		return ret;

	/* create vectors out of the heaps */
	sh_vector_init(&hvd.v, _itemcpy, sizeof(struct to_vector_item), _itemcmp);
	sh_vector_init_similar(&h2vd.v, &hvd.v);
	sh_heap_for_each((shHeap *)h, _to_vector, &hvd);
	sh_heap_for_each((shHeap *)h2, _to_vector, &h2vd);

	/* sort them and compare them */
	sh_vector_sort(&hvd.v);
	sh_vector_sort(&h2vd.v);
	ret = sh_vector_compare_lexic(&hvd.v, &h2vd.v);

	sh_vector_free(&hvd.v);
	sh_vector_free(&h2vd.v);

	return ret;
}

int sh_heap_compare(const void *h, const void *h2)
{
	const shHeap *a = h;
	const shHeap *b = h2;

	if (a->item_size != b->item_size || a->compare == NULL)
		return -2;

	if (a->size != b->size)
		return a->size < b->size?-1:1;

	return _sort_and_compare_lexic(a, b);
}

int sh_heap_compare_lexic(const void *h, const void *h2)
{
	const shHeap *a = h;
	const shHeap *b = h2;

	if (a->item_size != b->item_size || a->compare == NULL)
		return -2;

	return _sort_and_compare_lexic(a, b);
}

#ifndef NDEBUG
/* usage: _print_tree(h, h->root, 0); */
/*static void _print_tree(shHeap *h, shHeapNode *n, size_t cur_depth)
{
	size_t i;
	unsigned char *d;
	shHeapNode *child;
	shHeapNode *cur;

	for (i = 0; i < cur_depth; ++i)
		printf("  ");

	d = (unsigned char *)n->key;
	for (i = 0; i < h->item_size; ++i)
		printf("0x%02x ", d[i]);
	printf(" (%p: parent: %p, next: %p, prev: %p, child: %p)\n", (void *)n, (void *)n->parent, (void *)n->next, (void *)n->prev, (void *)n->child);

	child = n->child;
	cur = child;
	if (child)
	{
		do
		{
			_print_tree(h, cur, cur_depth + 1);
			cur = cur->next;
		} while (cur != child);
	}
}*/

static int _check_correctness(const shHeap *h, shHeapNode *n, size_t *count)
{
	shHeapNode *child;
	shHeapNode *cur;

	child = n->child;
	cur = child;
	if (child)
	{
		do
		{
			if (cur->parent != n)
				return -1;
			if (h->compare(n->key, cur->key) > 0)
				return -1;
			if (cur->next == NULL || cur->prev == NULL)
				return -1;
			if (cur->next->prev != cur || cur->prev->next != cur)
				return -1;
			if (_check_correctness(h, cur, count))
				return -1;
			++*count;
			cur = cur->next;
		} while (cur != child);
	}
	return 0;
}

int sh_heap_internal_correctness_test(const shHeap *h)
{
	size_t count = 1;

	if (h == NULL || h->root == NULL)
		return 0;

	if (((shHeapNode *)h->root)->parent != NULL)
		goto exit_fail;

	if (_check_correctness(h, h->root, &count))
		goto exit_fail;

	if (h->size != count)
		goto exit_fail;

	return 0;
exit_fail:
/*	_print_tree(h, h->root, 0); */
	return -1;
}
#endif

static void _clear_recursive(shHeapNode *n)
{
	shHeapNode *child = n->child;
	shHeapNode *cur;

	if (child)
	{
		cur = child;
		do
		{
			_clear_recursive(cur);
			cur = cur->next;
		} while (cur != child);
	}

	free(n);
}

void sh_heap_clear(shHeap *h)
{
	/* instead of a queue, an array is used because the maximum size of the queue is known */
	shHeapNode **nodes;
	int current = 0;
	int end = 0;

	if (h == NULL || h->root == NULL)
		return;

	nodes = malloc(h->size * sizeof *nodes);
	if (nodes == NULL)
		goto exit_no_mem;

	nodes[0] = h->root;
	end = 1;
	while (current < end)
	{
		shHeapNode *n = nodes[current++];
		shHeapNode *child = n->child;
		shHeapNode *cur;

		if (child)
		{
			cur = child;
			do
			{
				nodes[end++] = cur;
				cur = cur->next;
			} while (cur != child);
		}

		free(n);
	}

	free(nodes);

	goto exit_normal;
exit_no_mem:
	/* malloc failed, but let's hope there is enough memory on the stack to do it recursively */
	_clear_recursive(h->root);
exit_normal:
	h->size = 0;
	h->root = 0;
}

void sh_heap_free(shHeap *h)
{
	if (h == NULL)
		return;

	sh_heap_clear(h);
	*h = (shHeap){0};
}
