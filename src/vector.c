/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <shvector.h>
#include <shnode.h>

#define VECTOR_INLINE_SIZE (sizeof(((shVector *)0)->inline_data))
#define VECTOR_BEGIN_SIZE 32		/* the minimum size of data when allocated */

int (sh_vector_init)(shVector *v, sh_assign assign, size_t item_size, sh_compare cmp, ...)
{
	if (v == NULL || assign == NULL || item_size == 0)
		return -1;

	*v = (shVector){
		.assign = assign,
		.compare = cmp,
		.item_size = item_size,
	};

	return 0;
}

static int _allocate_mem(shVector *v, size_t size)
{
	void *enlarged;

	enlarged = realloc(v->data, size * v->item_size);
	if (enlarged == NULL)
		return -1;

	/*
	 * if v->data is NULL, then there were possibly inline data
	 * that need to be copied to newly allocated memory
	 */
	if (v->data == NULL)
		memcpy(enlarged, v->inline_data, v->size * v->item_size);

	v->data = enlarged;
	v->max_size = size;
	return 0;
}

int sh_vector_reserve(shVector *v, size_t size)
{
	if (v == NULL || size == 0 || v->item_size == 0 || (v->data != NULL && v->max_size >= size))
		return -1;

	/* if inline and size is small, no need for allocation */
	if (v->data == NULL && size <= VECTOR_INLINE_SIZE / v->item_size)
		return 0;

	return _allocate_mem(v, size);
}

int sh_vector_get_first(shVector *v, shNode *n)
{
	if (v == NULL || v->size == 0 || v == NULL)
		return -1;

	*n = (shNode){
		.data = sh_vector_at(v, 0),
		.owner = v
	};

	return 0;
}

int sh_vector_get_last(shVector *v, shNode *n)
{
	if (v == NULL || v->size == 0 || v == NULL)
		return -1;

	*n = (shNode){
		.data = sh_vector_at(v, v->size - 1),
		.owner = v
	};

	return 0;
}

static int _get_with_offset(shNode *n, int offset, shNode *other)
{
	shVector *v;
	size_t index;

	if (n == NULL || other == NULL)
		return -1;

	v = n->owner;
	index = ((char *)n->data - (char *)sh_vector_data(v)) / v->item_size;

	/* make sure new offset would be in range */
	if ((offset < 0 && index < (size_t)-offset)
		|| (offset > 0 && index + (size_t)offset >= v->size))
		return -1;

	if (offset < 0)
		index -= (size_t)-offset;
	else
		index += (size_t)offset;

	*other = (shNode){
		.data = sh_vector_at(v, index),
		.owner = v
	};

	return 0;
}

int sh_vector_get_next(shNode *n, shNode *next)
{
	return _get_with_offset(n, 1, next);
}

int sh_vector_get_prev(shNode *n, shNode *prev)
{
	return _get_with_offset(n, -1, prev);
}

static int _enlarge_common(shVector *v, size_t add)
{
	size_t new_size = v->size + add;

	/* if not allocating dynamically, just return */
	if (v->data == NULL && new_size <= VECTOR_INLINE_SIZE / v->item_size)
		return 0;

	if (v->data != NULL)
	{
		/* if allocation is unnecessary, skip */
		if (new_size <= v->max_size)
			return 0;

		/* try to double the size if add is too small */
		if (new_size < v->max_size * 2)
			new_size = v->max_size * 2;
		if (new_size < add * 2)
			new_size = add * 2;
	}

	/* while we are taking the time to allocate memory, at least allocate a good amount */
	if (new_size < VECTOR_BEGIN_SIZE)
		new_size = VECTOR_BEGIN_SIZE;

	return _allocate_mem(v, new_size);
}

int sh_vector_append(shVector *v, void *item)
{
	if (v == NULL || item == NULL || v->item_size == 0 || v->assign == NULL)
		return -1;

	if (_enlarge_common(v, 1))
		return -1;

	v->assign(sh_vector_at(v, v->size), item);
	++v->size;

	return 0;
}

int sh_vector_insert_sorted(shVector *v, void *item, bool unique)
{
	size_t i;
	void *temp_location;

	if (v == NULL || item == NULL || v->assign == NULL || v->compare == NULL)
		return -1;

	if (_enlarge_common(v, 1))
		return -1;

	/*
	 * If the data type is `T`, `item` could be given as `T`, `T *` or any other which is the second
	 * argument of `assign`, defined by the user.  However, the `compare` function takes `const T *`.
	 *
	 * To overcome this issue, the item is stored temporarily in the end of the vector.
	 */
	temp_location = sh_vector_at(v, v->size);
	v->assign(temp_location, item);

	for (i = v->size; i > 0; --i)
	{
		int res = v->compare(sh_vector_at(v, i - 1), temp_location);

		if (unique && res == 0)
			return 1;
		if (res <= 0)
			break;
	}

	memmove(sh_vector_at(v, i + 1), sh_vector_at(v, i), (v->size - i) * v->item_size);
	v->assign(sh_vector_at(v, i), item);
	++v->size;

	return 0;
}

int sh_vector_remove_lasts(shVector *v, size_t count)
{
	if (v == NULL || v->size < count)
		return -1;

	v->size -= count;

	return 0;
}

int sh_vector_concat(shVector *v, const shVector *v2)
{
	if (v2 == NULL)
		return 0;

	if (v->item_size != v2->item_size)
		return -1;

	return sh_vector_concat_array(v, sh_vector_data_const(v2), v2->size);
}

int sh_vector_concat_array(shVector *v, const void *items, size_t count)
{
	ptrdiff_t d;
	void *items_dup = NULL;

	if (v == NULL || v->item_size == 0)
		return -1;

	if (count == 0)
		return 0;

	if (items == NULL)
		return -1;

	/*
	 * make sure items doesn't point to v's own memory.  If so, make a backup of it before `realloc`ing
	 * and possibly invalidating items.  If `v` already has enough space (so `realloc` won't happen),
	 * then skip the backup.
	 */
	d = (const char *)items - (v->data == NULL?v->inline_data:(const char *)v->data);
	if (d >= 0 && d < v->item_size * v->size && (v->data == NULL || v->size + count > v->max_size))
	{
		items_dup = malloc(v->item_size * count);
		if (items_dup == NULL)
			return -1;
		memcpy(items_dup, items, v->item_size * count);
		items = items_dup;
	}

	if (_enlarge_common(v, count))
		return -1;

	memmove(sh_vector_at(v, v->size), items, count * v->item_size);
	v->size += count;

	free(items_dup);

	return 0;
}

void sh_vector_sort(shVector *v)
{
	if (v == NULL || v->compare == NULL || v->item_size == 0)
		return;

	qsort(sh_vector_data(v), v->size, v->item_size, v->compare);
}

int sh_vector_bsearch(const shVector *v, const void *d)
{
	void *res;

	if (v == NULL || v->compare == NULL || v->item_size == 0)
		return -1;

	res = bsearch(d, sh_vector_data_const(v), v->size, v->item_size, v->compare);
	if (res == NULL)
		return -1;

	return ((char *)res - (char *)sh_vector_data_const(v)) / v->item_size;
}

void sh_vector_minimalize(shVector *v)
{
	void *shrunk;

	if (v == NULL || v->data == NULL || v->item_size == 0 || v->size == 0)
		return;

	shrunk = realloc(v->data, v->size * v->item_size);
	if (shrunk == NULL)
		return;

	v->data = shrunk;
	v->max_size = v->size;
}

void *sh_vector_uninline(shVector *v)
{
	if (v == NULL || v->item_size == 0)
		return NULL;

	/* if inline, unline it */
	if (v->size > 0 && v->data == NULL)
		/* _allocate_mem would take care of everything */
		_allocate_mem(v, v->size);

	return v->data;
}

int (sh_vector_for_each)(shVector *v, sh_callback c, void *extra, ...)
{
	size_t i;

	if (v == NULL)
		return -1;

	for (i = 0; i < v->size; ++i)
	{
		shNode n = {
			.data = sh_vector_at(v, i),
			.owner = v
		};

		if (!c(&n, extra))
			return -1;
	}

	return 0;
}

int sh_vector_duplicate(shVector *v, shVector *v2)
{
	if (v == NULL || v2 == NULL)
		return -1;

	sh_vector_init_similar(v2, v);
	sh_vector_reserve(v2, v->size);
	return sh_vector_concat(v2, v);
}

static int _compare_lexic_common_size(const shVector *v, const shVector *v2)
{
	size_t i;
	int ret;

	for (i = 0; i < v->size && i < v2->size; ++i)
	{
		ret = v->compare(sh_vector_at_const(v, i), sh_vector_at_const(v2, i));
		if (ret)
			return ret;
	}

	return 0;
}

int sh_vector_compare(const void *v, const void *v2)
{
	const shVector *a = v;
	const shVector *b = v2;

	if (a->item_size != b->item_size || a->compare == NULL)
		return -2;

	if (a->size != b->size)
		return a->size < b->size?-1:1;

	return _compare_lexic_common_size(a, b);
}

int sh_vector_compare_lexic(const void *v, const void *v2)
{
	const shVector *a = v;
	const shVector *b = v2;
	int ret;

	if (a->item_size != b->item_size || a->compare == NULL)
		return -2;

	ret = _compare_lexic_common_size(a, b);
	if (ret || a->size == b->size)
		return ret;

	/* if reached here, the common part is equal.  Whoever has left-over is bigger */
	return a->size < b->size?-1:1;
}

void sh_vector_clear(shVector *v)
{
	if (v == NULL)
		return;

	free(v->data);
	v->data = NULL;
	v->size = 0;
	v->max_size = 0;
}

void sh_vector_free(shVector *v)
{
	if (v == NULL)
		return;

	sh_vector_clear(v);
	*v = (shVector){0};
}
