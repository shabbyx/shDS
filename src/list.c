/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "shlist.h"

typedef struct shListNode
{
	struct shListNode *next;
	struct shListNode *prev;
	char data[];
} shListNode;

int (sh_list_init)(shList *l, sh_assign assign, size_t item_size, sh_compare cmp, ...)
{
	if (l == NULL || assign == NULL || item_size == 0)
		return -1;

	*l = (shList){
		.assign = assign,
		.compare = cmp,
		.item_size = item_size,
	};

	return 0;
}

int sh_list_get_first(shList *l, shNode *n)
{
	if (l == NULL || l->size == 0 || n == NULL)
		return -1;

	*n = (shNode){
		.data = &((shListNode *)l->root)->data,
		.owner = l,
		.origin = l->root
	};

	return 0;
}

int sh_list_get_last(shList *l, shNode *n)
{
	if (l == NULL || l->size == 0 || n == NULL)
		return -1;

	*n = (shNode){
		.data = &((shListNode *)l->tail)->data,
		.owner = l,
		.origin = l->tail
	};

	return 0;
}

int sh_list_get_next(shNode *n, shNode *next)
{
	shListNode *this;

	if (n == NULL || next == NULL)
		return -1;

	this = n->origin;
	if (this->next == NULL)
		return -1;

	*next = (shNode){
		.data = &this->next->data,
		.owner = n->owner,
		.origin = this->next
	};

	return 0;
}

int sh_list_get_prev(shNode *n, shNode *prev)
{
	shListNode *this;

	if (n == NULL || prev == NULL)
		return -1;

	this = n->origin;
	if (this->prev == NULL)
		return -1;

	*prev = (shNode){
		.data = &this->prev->data,
		.owner = n->owner,
		.origin = this->prev
	};

	return 0;
}

int (sh_list_append)(shList *l, void *d, shNode *ref, ...)
{
	shListNode *n;

	if (l == NULL || l->item_size == 0 || l->assign == NULL)
		return -1;

	n = malloc(sizeof *n + l->item_size);
	if (n == NULL)
		return -1;

	n->next = NULL;
	n->prev = l->tail;
	l->assign(&n->data, d);

	if (l->root == NULL)
		l->root = n;
	else
		((shListNode *)l->tail)->next = n;
	l->tail = n;
	++l->size;

	if (ref != NULL)
		*ref = (shNode){
			.data = &n->data,
			.owner = l,
			.origin = n
		};

	return 0;
}

int (sh_list_prepend)(shList *l, void *d, shNode *ref, ...)
{
	shListNode *n;

	if (l == NULL || l->item_size == 0 || l->assign == NULL)
		return -1;

	n = malloc(sizeof *n + l->item_size);
	if (n == NULL)
		return -1;

	n->next = l->root;
	n->prev = NULL;
	l->assign(&n->data, d);

	if (l->root == NULL)
		l->tail = n;
	else
		((shListNode *)l->root)->prev = n;
	l->root = n;
	++l->size;

	if (ref != NULL)
		*ref = (shNode){
			.data = &n->data,
			.owner = l,
			.origin = n
		};

	return 0;
}

int sh_list_concat(shList *l, shList *l2)
{
	if (l == NULL || l2 == NULL)
		return -1;

	if (l2->root == NULL)
		return 0;

	/* can't concat list with itself */
	if (l->root == l2->root)
		return -1;

	if (l->root == NULL)
	{
		l->root = l2->root;
		l->item_size = l2->item_size;
		l->assign = l2->assign;
	}
	else
	{
		if (l->item_size != l2->item_size)
			return -1;
		((shListNode *)l->tail)->next = l2->root;
		((shListNode *)l2->root)->prev = l->tail;
	}
	l->tail = l2->tail;
	l->size += l2->size;

	l2->root = NULL;
	l2->tail = NULL;
	l2->size = 0;

	return 0;
}

int sh_list_break_after(shNode *n, shList *l2)
{
	shListNode *this, *cur;
	shList *l;

	if (n == NULL || l2 == NULL)
		return -1;

	this = n->origin;
	l = n->owner;

	/* even if the code could handle l == l2, there would be memory leak */
	if (l == l2)
		return -1;

	l2->root = this->next;
	if (l2->root != NULL)
		((shListNode *)l2->root)->prev = NULL;
	if (this->next == NULL)
		l2->tail = NULL;
	else
		l2->tail = l->tail;
	for (l2->size = 0, cur = l2->root; cur; cur = cur->next)
		++l2->size;
	this->next = NULL;
	l->tail = this;
	l->size -= l2->size;

	l2->item_size = l->item_size;
	l2->assign = l->assign;

	return 0;
}

int sh_list_break_before(shNode *n, shList *l2)
{
	shListNode *this, *cur;
	shList *l;

	if (n == NULL || l2 == NULL)
		return -1;

	this = n->origin;
	l = n->owner;

	/* same comment as sh_list_break_after */
	if (l == l2)
		return -1;

	l2->root = this;
	l2->tail = l->tail;
	l->tail = this->prev;
	if (this->prev == NULL)
	{
		l2->size = l->size;
		l->root = NULL;
		l->size = 0;
	}
	else
	{
		this->prev->next = NULL;
		this->prev = NULL;
		for (l2->size = 0, cur = l2->root; cur; cur = cur->next)
			++l2->size;
		l->size -= l2->size;
	}

	l2->item_size = l->item_size;
	l2->assign = l->assign;

	/* n now belongs to l2 */
	n->owner = l2;

	return 0;
}

int (sh_list_insert_after)(shNode *n, void *d, shNode *ref, ...)
{
	shListNode *new;
	shListNode *this;
	shList *l;

	if (n == NULL)
		return -1;

	this = n->origin;
	l = n->owner;
	if (this == NULL || l == NULL)
		return -1;

	new = malloc(sizeof *new + l->item_size);
	if (new == NULL)
		return -1;

	new->next = this->next;
	new->prev = this;
	l->assign(&new->data, d);

	this->next = new;
	if (new->next != NULL)
		new->next->prev = new;
	else
		l->tail = new;
	++l->size;

	if (ref != NULL)
		*ref = (shNode){
			.data = &new->data,
			.owner = l,
			.origin = new
		};

	return 0;
}

int (sh_list_insert_before)(shNode *n, void *d, shNode *ref, ...)
{
	shListNode *new;
	shListNode *this;
	shList *l;

	if (n == NULL)
		return -1;

	this = n->origin;
	l = n->owner;
	if (this == NULL || l == NULL)
		return -1;

	new = malloc(sizeof *new + l->item_size);
	if (new == NULL)
		return -1;

	new->next = this;
	new->prev = this->prev;
	l->assign(&new->data, d);

	this->prev = new;
	if (new->prev != NULL)
		new->prev->next = new;
	else
		l->root = new;
	++l->size;

	if (ref != NULL)
		*ref = (shNode){
			.data = &new->data,
			.owner = l,
			.origin = new
		};

	return 0;
}

int (sh_list_delete)(shNode *n, shNode *next, ...)
{
	shListNode *this;
	shListNode *this_next;
	shList *l;

	if (n == NULL)
		return -1;

	this = n->origin;
	l = n->owner;
	if (this == NULL || l == NULL)
		return -1;

	n->origin = NULL;
	if (this->prev == NULL)
		l->root = this->next;
	else
		this->prev->next = this->next;
	if (this->next == NULL)
		l->tail = this->prev;
	else
		this->next->prev = this->prev;
	this_next = this->next;
	free(this);
	--l->size;

	if (next)
	{
		if (this_next == NULL)
			return -1;
		else
			*next = (shNode){
				.data = &this_next->data,
				.owner = l,
				.origin = this_next
			};
	}
	return 0;
}

int (sh_list_for_each)(shList *l, sh_callback c, void *extra, ...)
{
	shListNode *cur;

	if (l == NULL)
		return -1;

	for (cur = l->root; cur; cur = cur->next)
	{
		shNode n = {
			.data = cur->data,
			.owner = l,
			.origin = cur
		};

		if (!c(&n, extra))
			return -1;
	}

	return 0;
}

int sh_list_duplicate(shList *l, shList *l2)
{
	shListNode *cur, *last = NULL;
	shListNode *n;

	if (l == NULL || l2 == NULL)
		return -1;

	sh_list_init_similar(l2, l);

	for (cur = l->tail; cur; cur = cur->prev)
	{
		n = malloc(sizeof *n + l->item_size);
		if (n == NULL)
			goto exit_no_mem;

		memcpy(n->data, cur->data, l->item_size);
		n->next = last;
		if (last)
			last->prev = n;
		else
			l2->tail = n;

		last = n;
	}

	if (last)
	{
		l2->root = last;
		last->prev = NULL;
	}

	l2->size = l->size;

	return 0;
exit_no_mem:
	l2->root = last;
	sh_list_free(l2);
	return -1;
}

static int _compare_lexic_common_size(const shList *l, const shList *l2)
{
	shListNode *cur, *cur2;
	int ret;

	for (cur = l->root, cur2 = l2->root; cur && cur2; cur = cur->next, cur2 = cur2->next)
	{
		ret = l->compare(cur->data, cur2->data);
		if (ret)
			return ret;
	}

	return 0;
}

int sh_list_compare(const void *l, const void *l2)
{
	const shList *a = l;
	const shList *b = l2;

	if (a->item_size != b->item_size || a->compare == NULL)
		return -2;

	if (a->size != b->size)
		return a->size < b->size?-1:1;

	return _compare_lexic_common_size(a, b);
}

int sh_list_compare_lexic(const void *l, const void *l2)
{
	const shList *a = l;
	const shList *b = l2;
	int ret;

	if (a->item_size != b->item_size || a->compare == NULL)
		return -2;

	ret = _compare_lexic_common_size(a, b);
	if (ret || a->size == b->size)
		return ret;

	/* if reached here, the common part is equal.  Whoever has left-over is bigger */
	return a->size < b->size?-1:1;
}

void sh_list_clear(shList *l)
{
	shListNode *cur, *next;

	if (l == NULL)
		return;

	cur = l->root;
	while (cur)
	{
		next = cur->next;
		free(cur);
		cur = next;
	}
	l->root = NULL;
	l->tail = NULL;
	l->size = 0;
}

void sh_list_free(shList *l)
{
	sh_list_clear(l);
	*l = (shList){0};
}
