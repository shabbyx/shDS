/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NDEBUG
# include <stdio.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <shmap.h>

typedef struct shMapNode
{
	size_t depth;			/* the depth of subtree rooted at this node.  A single node has depth 1 */
	struct shMapNode *left;
	struct shMapNode *right;
	struct shMapNode *parent;	/* parent of root is NULL */
	struct shMapNode *next;		/* next in-order node */
	struct shMapNode *prev;		/* previous in-order node */
	char key_data[];		/* memory for both key and data.  The map is sorted on key and each key points to some data */
} shMapNode;

/* return key_size padded to better align data */
static inline size_t key_size_aligned(size_t key_size, size_t data_size)
{
	size_t align, padded, aligned;

	if (data_size == 0)
		return key_size;

	align = data_size > 16?16:data_size;
	padded = key_size + align - 1;
	aligned = padded - padded % align;

	return aligned;
}

static inline void *_key(const shMap *m, shMapNode *n)
{
	return n->key_data;
}

static inline void *_data(const shMap *m, shMapNode *n)
{
	return n->key_data + key_size_aligned(m->key_size, m->data_size);
}

int sh_map_init(shMap *m, sh_assign key_assign, size_t key_size,
		sh_assign data_assign, size_t data_size, sh_compare cmp)
{
	if (m == NULL || key_assign == NULL || key_size == 0
		|| data_assign == NULL || cmp == NULL)
		return -1;

	*m = (shMap){
		.key_assign = key_assign,
		.data_assign = data_assign,
		.compare = cmp,
		.key_size = key_size,
		.data_size = data_size,
	};

	return 0;
}

int sh_map_get_first(shMap *m, shNode *n)
{
	if (m == NULL || m->size == 0 || n == NULL)
		return -1;

	*n = (shNode){
		.key = _key(m, m->first),
		.data = _data(m, m->first),
		.owner = m,
		.origin = m->first
	};

	return 0;
}

int sh_map_get_last(shMap *m, shNode *n)
{
	if (m == NULL || m->size == 0 || n == NULL)
		return -1;

	*n = (shNode){
		.key = _key(m, m->last),
		.data = _data(m, m->last),
		.owner = m,
		.origin = m->last
	};

	return 0;
}

int sh_map_get_next(shNode *n, shNode *next)
{
	shMap *m;
	shMapNode *this;

	if (n == NULL || next == NULL)
		return -1;

	m = n->owner;
	this = n->origin;
	if (this->next == NULL)
		return -1;

	*next = (shNode){
		.key = _key(m, this->next),
		.data = _data(m, this->next),
		.owner = m,
		.origin = this->next
	};

	return 0;
}

int sh_map_get_prev(shNode *n, shNode *prev)
{
	shMap *m;
	shMapNode *this;

	if (n == NULL || prev == NULL)
		return -1;

	m = n->owner;
	this = n->origin;
	if (this->prev == NULL)
		return -1;

	*prev = (shNode){
		.key = _key(m, this->prev),
		.data = _data(m, this->prev),
		.owner = m,
		.origin = this->prev
	};

	return 0;
}

bool sh_map_find(shMap *m, const void *k, shNode *n)
{
	shMapNode *cur;

	if (m == NULL || m->root == NULL || m->compare == NULL)
		return false;

	cur = m->root;
	while (true)
	{
		int res = m->compare(k, _key(m, cur));

		if (res == 0)
		{
			if (n != NULL)
				*n = (shNode){
					.data = _data(m, cur),
					.key = _key(m, cur),
					.owner = m,
					.origin = cur
				};
			return true;
		}

		if (res < 0 && cur->left)
			cur = cur->left;
		else if (res > 0 && cur->right)
			cur = cur->right;
		else
			return false;
	}
}

static shMapNode *_create_node(shMap *m, shMapNode *p, void *k, void *d, shNode *ref)
{
	shMapNode *n = malloc(sizeof *n + key_size_aligned(m->key_size, m->data_size) + m->data_size);

	if (n == NULL)
		return NULL;

	*n = (shMapNode){
		.depth = 1,
		.parent = p
	};
	m->key_assign(_key(m, n), k);
	m->data_assign(_data(m, n), d);

	if (ref != NULL)
		*ref = (shNode){
			.key = _key(m, n),
			.data = _data(m, n),
			.owner = m,
			.origin = n
		};

	return n;
}

static void _update_depth(shMapNode *n)
{
	int d_l, d_r;

	if (n == NULL)
		return;

	d_l = n->left?n->left->depth:0;
	d_r = n->right?n->right->depth:0;

	if (d_l > d_r)
		n->depth = d_l + 1;
	else
		n->depth = d_r + 1;
}

static void _left_rotate(shMap *m, shMapNode *n)
{
	shMapNode *parent = n->parent;
	shMapNode *newroot = n->right;

	n->right = newroot->left;
	if (n->right)
		n->right->parent = n;
	newroot->left = n;
	n->parent = newroot;
	newroot->parent = parent;
	if (parent)
		if (parent->left == n)
			parent->left = newroot;
		else
			parent->right = newroot;
	else
		m->root = newroot;

	_update_depth(newroot->left);
	_update_depth(newroot->right);
	_update_depth(newroot);
/*	_update_depth(newroot->parent);	*/	/* unnecessary as the next cycle of _rebalance would do it anyway */
}

static void _right_rotate(shMap *m, shMapNode *n)
{
	shMapNode *parent = n->parent;
	shMapNode *newroot = n->left;

	n->left = newroot->right;
	if (n->left)
		n->left->parent = n;
	newroot->right = n;
	n->parent = newroot;
	newroot->parent = parent;
	if (parent)
		if (parent->left == n)
			parent->left = newroot;
		else
			parent->right = newroot;
	else
		m->root = newroot;

	_update_depth(newroot->left);
	_update_depth(newroot->right);
	_update_depth(newroot);
/*	_update_depth(newroot->parent);	*/	/* unnecessary as the next cycle of _rebalance would do it anyway */
}

static void _rebalance(shMap *m, shMapNode *n)
{
	while (n)
	{
		int d_l = n->left?n->left->depth:0;
		int d_r = n->right?n->right->depth:0;
		int diff;

		_update_depth(n);

		diff = d_r - d_l;
		if (diff < -1)
		{
			int d_l_l = n->left->left?n->left->left->depth:0;
			int d_l_r = n->left->right?n->left->right->depth:0;
			int diff2 = d_l_r - d_l_l;

			if (diff2 > 0)		/* left-right case */
				_left_rotate(m, n->left);
			/* left-left case */
			_right_rotate(m, n);
		}
		else if (diff > 1)
		{
			int d_r_l = n->right->left?n->right->left->depth:0;
			int d_r_r = n->right->right?n->right->right->depth:0;
			int diff2 = d_r_r - d_r_l;

			if (diff2 < 0)		/* right-left case */
				_right_rotate(m, n->right);
			/* right-right case */
			_left_rotate(m, n);
		}

		n = n->parent;
	}
}

int _insert(shMap *m, void *k, void *d, bool replace, shNode *ref)
{
	shMapNode *cur;
	void *k_fixed;

	if (m == NULL || m->key_size == 0 || m->compare == NULL)
		return -1;

	if (m->root == NULL)
	{
		m->root = _create_node(m, NULL, k, d, ref);
		if (m->root == NULL)
			return -1;

		m->size = 1;
		m->first = m->last = m->root;

		return 0;
	}

	/*
	 * If the key type is `T`, the insert/replace functions may take the key as `T`, `T *` or any other
	 * which is the second argument of `assign`, defined by the user.  However, the map needs to compare
	 * those given keys with the ones stored in the map, i.e. it needs to send `const T *` to the compare
	 * function.
	 *
	 * To overcome this issue, the map keeps an auxiliary space where it immediately uses the `assign`
	 * function to copy the key to this area and then give a pointer to this area to the compare function.
	 */
	if (m->internal == NULL)
	{
		m->internal = malloc(m->key_size);
		if (m->internal == NULL)
			return -1;
	}
	k_fixed = m->internal;
	m->key_assign(k_fixed, k);

	cur = m->root;
	while (true)
	{
		int res = m->compare(k_fixed, _key(m, cur));

		if (res == 0)
		{
			/* node with this key already exists */
			if (replace)
				m->data_assign(_data(m, cur), d);
			if (ref != NULL)
				*ref = (shNode){
					.key = _key(m, cur),
					.data = _data(m, cur),
					.owner = m,
					.origin = m->root
				};
			return replace?0:1;
		}

		if (res < 0)
		{
			if (cur->left)
				cur = cur->left;
			else
			{
				shMapNode *new = _create_node(m, cur, k, d, ref);
				if (new == NULL)
					return -1;

				/* insert between cur and cur->prev in the embedded linked list */
				new->next = cur;
				new->prev = cur->prev;
				if (cur->prev)
					cur->prev->next = new;
				else
					m->first = new;
				cur->prev = new;

				cur->left = new;
				_rebalance(m, cur);

				break;
			}
		}
		else if (res > 0)
		{
			if (cur->right)
				cur = cur->right;
			else
			{
				shMapNode *new = _create_node(m, cur, k, d, ref);
				if (new == NULL)
					return -1;

				/* insert between cur and cur->next in the embedded linked list */
				new->next = cur->next;
				new->prev = cur;
				if (cur->next)
					cur->next->prev = new;
				else
					m->last = new;
				cur->next = new;

				cur->right = new;
				_rebalance(m, cur);

				break;
			}
		}
	}

	++m->size;

	return 0;
}

int (sh_map_insert)(shMap *m, void *k, void *d, shNode *ref, ...)
{
	return _insert(m, k, d, false, ref);
}

int (sh_map_replace)(shMap *m, void *k, void *d, shNode *ref, ...)
{
	return _insert(m, k, d, true, ref);
}

int (sh_map_delete)(shNode *n, shNode *next, ...)
{
	shMap *m;
	shMapNode *this;
	shMapNode *this_next;
	shMapNode *to_delete;
	shMapNode *to_delete_child;
	shMapNode *to_delete_parent;
	bool double_child;

	if (n == NULL)
		return -1;

	m = n->owner;
	this = n->origin;
	if (this == NULL || m == NULL)
		return -1;

	/* keep the node that comes next.  If deleting a node with two children, this_next will replace it */
	this_next = this->next;

	/* right in the beginning, fix all the next/prev links */
	if (this->prev)
		this->prev->next = this->next;
	else
		m->first = this->next;
	if (this->next)
		this->next->prev = this->prev;
	else
		m->last = this->prev;

	/*
	 * if a node with at most one child, then the node will be deleted and its child given to the parent.
	 * If a node with two children, this_next will replace it in the tree, and this_next's at most one
	 * child is given to this_next's parent.
	 *
	 * Before possibly moving this_next in the tree, first give the child of either this or this_next to
	 * its parent.  Then if required, place this_next where this currently is.
	 *
	 * In the process, if parent of a node is set to NULL, root of map is updated to be that node.
	 */
	double_child = this->left != NULL && this->right != NULL;
	to_delete = double_child?this_next:this;
	assert(to_delete->left == NULL || to_delete->right == NULL);
	to_delete_child = to_delete->left?to_delete->left:to_delete->right;
	to_delete_parent = to_delete->parent;

	/*
	 * in the special case that this_next->parent is this (for double_child), then to_delete_parent
	 * ends up being this, which is going to be deleted.  In that case, the rebalance should be
	 * done from this_next itself
	 */
	if (double_child && to_delete_parent == this)
		to_delete_parent = this_next;

	if (to_delete->parent)
	{
		if (to_delete->parent->left == to_delete)
			to_delete->parent->left = to_delete_child;
		else if (to_delete->parent->right == to_delete)
			to_delete->parent->right = to_delete_child;
	}
	if (to_delete_child)
	{
		to_delete_child->parent = to_delete->parent;
		if (to_delete_child->parent == NULL)
			m->root = to_delete_child;
	}

	if (double_child)
	{
		this_next->left = this->left;
		if (this_next->left)
			this_next->left->parent = this_next;
		this_next->right = this->right;
		if (this_next->right)
			this_next->right->parent = this_next;
		this_next->parent = this->parent;
		if (this->parent)
		{
			if (this->parent->left == this)
				this->parent->left = this_next;
			else if (this->parent->right == this)
				this->parent->right = this_next;
		}
		else
			m->root = this_next;
	}

	/* rebalance will take care of updating the depth fields */
	_rebalance(m, to_delete_parent);
	free(this);
	--m->size;

	if (next)
	{
		if (this_next == NULL)
			return -1;
		else
			*next = (shNode){
				.key = _key(m, this_next),
				.data = _data(m, this_next),
				.owner = m,
				.origin = this_next
			};
	}

	return 0;
}

int (sh_map_for_each)(shMap *m, sh_callback c, void *extra, ...)
{
	/* instead of a queue, an array is used because the maximum size of the queue is known */
	shMapNode **nodes;
	int current = 0;
	int end = 0;
	int ret = 0;

	if (m == NULL)
		return -1;
	if (m->root == NULL)
		return 0;

	nodes = malloc(m->size * sizeof *nodes);
	if (nodes == NULL)
		goto exit_no_mem;

	nodes[0] = m->root;
	end = 1;
	while (current < end)
	{
		shMapNode *n = nodes[current++];
		shNode node;

		if (n->left)
			nodes[end++] = n->left;

		if (n->right)
			nodes[end++] = n->right;

		node = (shNode){
			.key = _key(m, n),
			.data = _data(m, n),
			.owner = m,
			.origin = n
		};
		if (!c(&node, extra))
		{
			ret = -1;
			break;
		}
	}

	free(nodes);

	return ret;
exit_no_mem:
	/* malloc failed, do the constant memory in-order traversal */
	return sh_map_inorder(m, c, extra);
}

int (sh_map_inorder)(shMap *m, sh_callback c, void *extra, ...)
{
	shMapNode *cur;

	if (m == NULL || m->first == NULL)
		return -1;

	for (cur = m->first; cur; cur = cur->next)
	{
		shNode node = {
			.key = _key(m, cur),
			.data = _data(m, cur),
			.owner = m,
			.origin = cur
		};

		if (!c(&node, extra))
			return -1;
	}

	return 0;
}

struct dup_data
{
	shMapNode *cur;
	shMapNode *dup_parent;
	shMapNode **dup_parent_child;
	shMapNode *dup_prev;
	shMapNode *dup_next;

	/* for failure cleanup */
	shMapNode *dup;
};

int sh_map_duplicate(shMap *m, shMap *m2)
{
	shMapNode *new_root = NULL;
	struct dup_data *nodes;
	int current = 0;
	int i, end = 0;
	size_t item_data_size;

	if (m == NULL || m2 == NULL)
		return -1;

	sh_map_init_similar(m2, m);

	if (m->root == NULL)
		return 0;

	item_data_size = key_size_aligned(m->key_size, m->data_size) + m->data_size;

	/* instead of a queue, an array is used because the maximum size of the queue is known */
	nodes = malloc(m->size * sizeof *nodes);
	if (nodes == NULL)
		goto exit_no_mem_for_bfs;

	nodes[0] = (struct dup_data){
		.cur = m->root,
		.dup_parent_child = &new_root,
	};
	end = 1;
	while (current < end)
	{
		struct dup_data *d = &nodes[current++];
		shMapNode *n = malloc(sizeof *n + item_data_size);

		if (n == NULL)
			goto exit_no_mem;
		/* store n in case above if later fails and cleanup is needed */
		d->dup = n;

		*n = (shMapNode){
			.depth = d->cur->depth,
			.parent = d->dup_parent,
		};
		memcpy(n->key_data, d->cur->key_data, item_data_size);

		/* set it as parent's child */
		*d->dup_parent_child = n;

		/*
		 * if has children, put them in queue with appropriate prev and next.
		 * The children, will take care of setting this node's prev and next.
		 */
		if (d->cur->left)
			nodes[end++] = (struct dup_data){
				.cur = d->cur->left,
				.dup_parent = n,
				.dup_parent_child = &n->left,
				.dup_prev = d->dup_prev,
				.dup_next = n,
			};
		else
		{
			n->prev = d->dup_prev;
			if (n->prev)
				n->prev->next = n;
		}
		if (d->cur->right)
			nodes[end++] = (struct dup_data){
				.cur = d->cur->right,
				.dup_parent = n,
				.dup_parent_child = &n->right,
				.dup_prev = n,
				.dup_next = d->dup_next,
			};
		else
		{
			n->next = d->dup_next;
			if (n->next)
				n->next->prev = n;
		}

		/* if left most, it is m2's first, and if right most, it is m2's last */
		if (d->dup_prev == NULL && d->cur->left == NULL)
			m2->first = n;
		if (d->dup_next == NULL && d->cur->right == NULL)
			m2->last = n;
	}

	m2->root = new_root;
	m2->size = m->size;

	free(nodes);

	m2->size = m->size;

	return 0;
exit_no_mem:
	for (i = 0; i < current - 1; ++i)
		free(nodes[i].dup);
exit_no_mem_for_bfs:
	return -1;
}

static int _compare_lexic_common_size(const shMap *m, const shMap *m2)
{
	shMapNode *cur, *cur2;
	int ret;

	for (cur = m->first, cur2 = m2->first; cur && cur2; cur = cur->next, cur2 = cur2->next)
	{
		ret = m->compare(_key(m, cur), _key(m2, cur2));
		if (ret)
			return ret;
	}

	return 0;
}

int sh_map_compare(const void *m, const void *m2)
{
	const shMap *a = m;
	const shMap *b = m2;

	if (a->key_size != b->key_size || a->data_size != b->data_size || a->compare == NULL)
		return -2;

	if (a->size != b->size)
		return a->size < b->size?-1:1;

	return _compare_lexic_common_size(a, b);
}

int sh_map_compare_lexic(const void *m, const void *m2)
{
	const shMap *a = m;
	const shMap *b = m2;
	int ret;

	if (a->key_size != b->key_size || a->data_size != b->data_size || a->compare == NULL)
		return -2;

	ret = _compare_lexic_common_size(a, b);
	if (ret || a->size == b->size)
		return ret;

	/* if reached here, the common part is equal.  Whoever has left-over is bigger */
	return a->size < b->size?-1:1;
}

#ifndef NDEBUG
/* usage: _print_tree(m, m->root, ((shMapNode *)m->root)->depth); */
/*static void _print_tree(shMap *m, shMapNode *n, size_t max_depth)
{
	size_t i;
	unsigned char *d;

	if (n->left != NULL)
		_print_tree(m, n->left, max_depth);

	for (i = n->depth; i < max_depth; ++i)
		printf("  ");

	d = _key(m, n);
	for (i = 0; i < m->key_size; ++i)
		printf("0x%02x ", d[i]);
	d = _data(m, n);
	printf("-> ");
	for (i = 0; i < m->data_size; ++i)
		printf("0x%02x ", d[i]);
	printf("(depth: %d)\n", n->depth);

	if (n->right != NULL)
		_print_tree(m, n->right, max_depth);
}*/

static int _check_balance(shMapNode *n)
{
	size_t children_max_depth = 0;

	if (n->left)
	{
		if (_check_balance(n->left))
			return -1;
		else
			if (n->left->depth > children_max_depth)
				children_max_depth = n->left->depth;
	}

	if (n->right)
	{
		if (_check_balance(n->right))
			return -1;
		else
			if (n->right->depth > children_max_depth)
				children_max_depth = n->right->depth;
	}

	if (n->depth != children_max_depth + 1)
		return -1;

	return 0;
}

int sh_map_internal_balance_test(const shMap *m)
{
	if (m == NULL || m->root == NULL)
		return 0;

	if (_check_balance(m->root))
		return -1;

	return 0;
}
#endif

/* only called if not enough memory for sh_map_clear */
static void _clear_recursive(shMapNode *n)
{
	if (n->left)
		_clear_recursive(n->left);

	if (n->right)
		_clear_recursive(n->right);

	free(n);
}

void sh_map_clear(shMap *m)
{
	/* instead of a queue, an array is used because the maximum size of the queue is known */
	shMapNode **nodes;
	int current = 0;
	int end = 0;

	if (m == NULL || m->root == NULL)
		return;

	free(m->internal);

	nodes = malloc(m->size * sizeof *nodes);
	if (nodes == NULL)
		goto exit_no_mem;

	nodes[0] = m->root;
	end = 1;
	while (current < end)
	{
		shMapNode *n = nodes[current++];

		if (n->left)
			nodes[end++] = n->left;

		if (n->right)
			nodes[end++] = n->right;

		free(n);
	}

	free(nodes);

	goto exit_normal;
exit_no_mem:
	/* malloc failed, but let's hope there is enough memory on the stack to do it recursively */
	_clear_recursive(m->root);
exit_normal:
	m->size = 0;
	m->root = NULL;
	m->internal = NULL;
}

void sh_map_free(shMap *m)
{
	if (m == NULL)
		return;

	sh_map_clear(m);
	*m = (shMap){0};
}
