/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <shset.h>
#include <shvector.h>

static void _data_assign(void *to, void *from)
{
	/* nothing */
}

int sh_set_init(shSet *s, sh_assign assign, size_t item_size, sh_compare cmp)
{
	return sh_map_init(s, assign, item_size, _data_assign, 0, cmp);
}

bool _to_vector(shNode *n, void *extra)
{
	shVector *v = extra;

	memmove(sh_vector_at(v, v->size), n->key, v->item_size);
	++v->size;

	return true;
}

int sh_set_to_vector(shSet *s, shVector *v)
{
	int res = 0;

	res = sh_vector_init(v, s->key_assign, s->key_size, s->compare);
	if (res)
		return -1;
	if (sh_vector_reserve(v, s->size))
		return -1;

	sh_set_inorder(s, _to_vector, v);
	sh_vector_minimalize(v);

	return 0;
}
