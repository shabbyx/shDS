/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_VECTOR_H
#define SH_VECTOR_H

#include <stdbool.h>
#include <stddef.h>
#include "shnode.h"
#include "shcallbacks.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct shVector
{
	void *data;			/* array of data (NULL if inline) */
	size_t size;			/* number of items in vector */
	size_t item_size;		/* size of each item in the array */

	/*
	 * just a small bit of data could be inlined where `max_size` resides.  On a typical
	 * 64-bit architecture, this would be 8 bytes and on a typical 32-bit architecture
	 * only 4 bytes.  Note: tests on enlarging the inline area showed worse performance
	 * due to the enlarged shVector size.
	 */
	union
	{
		size_t max_size;	/*
					 * number of items worth of memory allocated currently.
					 * If inline data is used, this shouldn't be accessed.
					 */
		char inline_data[sizeof(size_t)];
	};

	sh_assign assign;
	sh_compare compare;		/* could be NULL if vector is never sorted or compared */
} shVector;

/*
 * shVector keeps an array of data.
 *
 * The functions of shVector return 0 on success and -1 on failure, unless expressed otherwise:
 *
 * init			initializes the vector.  The compare argument is optional as it is not always needed.  This compare
 *			function is necessary if the vector is to be sorted, binary searched or compared.
 * init_similar		initializes the vector similar to another vector.
 * get_first		gets the first item of the vector in given node.  Returns 0 if successful or -1 if error or vector is empty.
 * get_last		gets the last item of the vector in given node.  Returns 0 if successful or -1 if error or vector is empty.
 * get_next		given a node, gets the next node in `next`.  Returns 0 if successful or -1 if error or n is the last item of the vector.
 * get_prev		given a node, gets the previous node in `prev`.  Returns 0 if successful o -1 if error or n is the first item of the vector.
 * for_each		calls the callback for each element in the vector with given `extra` parameter in order of index.
 *			Returns -1 if the callback requests to stop traversal and 0 otherwise.
 * duplicate		make an exact duplicate of the vector.
 * assign		shallow copy a vector.
 * compare		compare two vectors by an arbitrary order.  The compare function from v is used.  If the vectors are
 *			incompatible or `v` doesn't have a compare function, it returns -2.  Otherwise it returns -1, 0 or 1
 *			similar to `sh_compare`.
 * compare_lexic	similar to `compare`, but compares lexicographically.
 * clear		frees vector memory, but vector can still be used (no sh_vector_init required).
 * free			free vector memory.  sh_vector_init needs to be called if vector is to be used again.
 *
 * shVector specific functions:
 *
 * reserve		doesn't change anything in the vector, except _possibly_ increasing its max_size.  If not enough memory,
 *			it will fail.  Either way, sh_vector_append should still be used afterwards.
 * data			get pointer to all data as a C array.
 * data_const		similar to `data`, but gets const pointer.
 * at			get pointer to element at given index.  If all elements need to be accessed, it is more efficient to
 *			get a pointer to all data and loop over it.
 * at_const		similar to `at`, but gets const pointer.
 * append		appends 1 item to the end of vector.
 * insert_sorted	inserts 1 item to the vector, in its sorted position.  Inserting items to the vector using this
 *			function is equivalent to insertion-sort.  `unique` tells whether the item should be unique or not.
 *			If `unique` is set and the item already exists, the functions returns with 1 without modifying
 *			the vector.
 * remove_last		removes one item from the array.  The item is thrown away.
 * remove_lasts		removes many items from the array.  The items are thrown away.
 * concat		appends all items in second vector to the end of the first.  Makes shallow copy.
 * concat_array		like sh_vector_concat, but will concat from an array (a void *) instead of shVector.
 * sort			sorts vector in ascending order.
 * bsearch		binary searches for item and return its index, or -1 if didn't exist. Should be called only if vector is
 *			sorted.  Unlike C's bsearch, the index is returned instead of pointer to object.  With index, the object is
 *			easily accessible.
 * minimalize		removes extra memory allocated but not used.
 * uninline		forces the vector to use dynamic memory.  This is useful if the memory of the vector is to be taken as
 *			dynamic memory and the vector itself thrown away.  Afterwards, it returns the vector memory, which would
 *			be the same as the output of sh_vector_data.
 */
#define sh_vector_init(...) sh_vector_init(__VA_ARGS__, NULL)
int (sh_vector_init)(shVector *v, sh_assign assign, size_t item_size, sh_compare cmp, ...);
static inline int sh_vector_init_similar(shVector *v, const shVector *orig)
{
	return sh_vector_init(v, orig->assign, orig->item_size, orig->compare);
}

int sh_vector_get_first(shVector *v, shNode *n);
int sh_vector_get_last(shVector *v, shNode *n);
int sh_vector_get_next(shNode *n, shNode *next);
int sh_vector_get_prev(shNode *n, shNode *prev);
#define sh_vector_for_each(...) sh_vector_for_each(__VA_ARGS__, NULL)
int (sh_vector_for_each)(shVector *v, sh_callback c, void *extra, ...);
int sh_vector_duplicate(shVector *v, shVector *v2);
static inline void sh_vector_assign(void *to, void *from)
{
	*(shVector *)to = *(shVector *)from;
}

int sh_vector_compare(const void *v, const void *v2);
int sh_vector_compare_lexic(const void *v, const void *v2);
void sh_vector_clear(shVector *v);
void sh_vector_free(shVector *v);


int sh_vector_reserve(shVector *v, size_t size);
static inline void *sh_vector_data(shVector *v)
{
	return v->data == NULL?v->inline_data:v->data;
}

static inline const void *sh_vector_data_const(const shVector *v)
{
	return v->data == NULL?v->inline_data:v->data;
}

static inline void *sh_vector_at(shVector *v, size_t i)
{
	return (char *)sh_vector_data(v) + i * v->item_size;
}

static inline const void *sh_vector_at_const(const shVector *v, size_t i)
{
	return (const char *)sh_vector_data_const(v) + i * v->item_size;
}

int sh_vector_append(shVector *v, void *item);
int sh_vector_insert_sorted(shVector *v, void *item, bool unique);
int sh_vector_remove_lasts(shVector *v, size_t count);
static inline int sh_vector_remove_last(shVector *v) { return sh_vector_remove_lasts(v, 1); }
int sh_vector_concat(shVector *v, const shVector *v2);
int sh_vector_concat_array(shVector *v, const void *items, size_t count);
void sh_vector_sort(shVector *v);
int sh_vector_bsearch(const shVector *v, const void *d);
void sh_vector_minimalize(shVector *v);
void *sh_vector_uninline(shVector *v);

#ifdef __cplusplus
}
#endif

#endif
