/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_NODE_H
#define SH_NODE_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * This is a generic node, returned by search functions.  They have references to their original structure and so can
 * be used to delete a node or get next node for example.  If the original node is not removed, an shNode will always
 * be a valid reference to the original node no matter how the data structure is changed.
 */
typedef struct shNode
{
	void *data;		/* points to user data, such as data stored in a list, or what a key points to in a map */
	void *key;		/*
				 * in ordered structures, this will contain the key.  Modifications to key should not change
				 * ordering. I.e., only parts of the key that don't participate in ordering may be modified
				 * through shNode.
				 */
	void *owner;		/* owning structure */

	void *origin;		/* the origin of the node.  It may point to the original node of the structure */
				/* or any other implementation defined value and should not be used by the user */
} shNode;

#ifdef __cplusplus
}
#endif

#endif
