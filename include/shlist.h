/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_LIST_H
#define SH_LIST_H

#include "shnode.h"
#include "shcallbacks.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct shList
{
	void *root;		/* the first element of the list */
	void *tail;		/* the last element of the list */
	size_t size;		/* the size of the list */
	size_t item_size;	/* size of each item in the list */

	sh_assign assign;
	sh_compare compare;
} shList;

/*
 * shList is a doubly-linked list keeping data.
 *
 * The functions of shList return 0 on success and -1 on failure, unless expressed otherwise:
 *
 * init			initializes the list.  The compare argument is optional as it is not always needed.  This compare
 *			function is necessary if the list is to be compared.
 * init_similar		initializes the list similar to another list.
 * get_first		gets the root of the list in given node.  Returns 0 if successful or -1 if error or list is empty.
 * get_last		gets the tail of the list in given node.  Returns 0 if successful or -1 if error or list is empty.
 * get_next		given a node, gets the next node in `next`.  Returns 0 if successful or -1 if error or n is the tail of list.
 * get_prev		given a node, gets the previous node in `prev`.  Returns 0 if successful o -1 if error or n is the root of list.
 * for_each		calls the callback for each element in the list with given `extra` parameter in order of root to tail.
 *			Returns -1 if the callback requests to stop traversal and 0 otherwise.
 * duplicate		make an exact duplicate of the list.
 * assign		shallow copy a list.
 * compare		compare two lists by an arbitrary order.  The compare function from l is used.  If the lists are
 *			incompatible or `l` doesn't have a compare function, it returns -2.  Otherwise it returns -1, 0 or 1
 *			similar to `sh_compare`.
 * compare_lexic	similar to `compare`, but compares lexicographically.
 * clear		clears the list.  The list will not contain any data, but it can still be used (no sh_list_init required).
 * free			frees the list.  Requires sh_list_init if it is to be used again.
 *
 * shList specific functions:
 *
 * append		adds data to end of list.
 * prepend		adds data to the beginning of list.
 * concat		concatenates two lists (adds the second list after the first).  The second list is cleared afterwards (empty,
 *			but does not need sh_list_init).
 * break_after		breaks the list after given node and set second list to the rest of the node's original list.
 * break_before		breaks the list before given node and set second list to the rest of the node's original list.
 *			The root of the second list will be the given node.
 * break		same as sh_list_break_after.
 * insert_after		inserts data in the list after given node.
 * insert_before	inserts data in the list before given node.
 * insert		same as sh_list_insert_after.
 * delete		deletes given node and sets `next` to the next node in list if not NULL.  Returns 0 if successful or -1 if
 *			deleting last element of list.
 */
#define sh_list_init(...) sh_list_init(__VA_ARGS__, NULL)
int (sh_list_init)(shList *l, sh_assign assign, size_t item_size, sh_compare cmp, ...);
static inline int sh_list_init_similar(shList *l, const shList *orig)
{
	return sh_list_init(l, orig->assign, orig->item_size, orig->compare);
}

int sh_list_get_first(shList *l, shNode *n);
int sh_list_get_last(shList *l, shNode *n);
int sh_list_get_next(shNode *n, shNode *next);
int sh_list_get_prev(shNode *n, shNode *prev);
#define sh_list_for_each(...) sh_list_for_each(__VA_ARGS__, NULL)
int (sh_list_for_each)(shList *l, sh_callback c, void *extra, ...);
int sh_list_duplicate(shList *l, shList *l2);
static inline void sh_list_assign(void *to, void *from)
{
	*(shList *)to = *(shList *)from;
}

int sh_list_compare(const void *l, const void *l2);
int sh_list_compare_lexic(const void *l, const void *l2);
void sh_list_clear(shList *l);
void sh_list_free(shList *l);


#define sh_list_append(...) sh_list_append(__VA_ARGS__, NULL)
int (sh_list_append)(shList *l, void *d, shNode *ref, ...);
#define sh_list_prepend(...) sh_list_prepend(__VA_ARGS__, NULL)
int (sh_list_prepend)(shList *l, void *d, shNode *ref, ...);
int sh_list_concat(shList *l, shList *l2);
int sh_list_break_after(shNode *n, shList *l2);
static inline int sh_list_break(shNode *n, shList *l2)
{
	return sh_list_break_after(n, l2);
}

int sh_list_break_before(shNode *n, shList *l2);
#define sh_list_insert_after(...) sh_list_insert_after(__VA_ARGS__, NULL)
int (sh_list_insert_after)(shNode *n, void *d, shNode *ref, ...);
#define sh_list_insert_before(...) sh_list_insert_before(__VA_ARGS__, NULL)
int (sh_list_insert_before)(shNode *n, void *d, shNode *ref, ...);
#define sh_list_delete(...) sh_list_delete(__VA_ARGS__, NULL)
int (sh_list_delete)(shNode *n, shNode *next, ...);

#ifdef __cplusplus
}
#endif

#endif
