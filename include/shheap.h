/*
 * Copyright (C) 2013-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_HEAP_H
#define SH_HEAP_H

#include <stdbool.h>
#include "shnode.h"
#include "shcallbacks.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct shHeap
{
	void *root;
	size_t size;		/* number of items in the heap */
	size_t item_size;	/* size of each item in the heap */

	sh_assign assign;
	sh_compare compare;	/* heap uses this function to compare its keys */
} shHeap;
/*
 * shHeap is a pairing min-heap.
 *
 * The functions of shHeap return 0 on success and -1 on failure, unless expressed otherwise:
 *
 * init			initializes the heap
 * init_similar		initializes the heap similar to another heap.
 * get_first		gets the root of the heap in given node.  Returns 0 if successful or -1 if error or heap is empty.
 * get_last		gets an arbitrary element of the heap as last element in given node.  Returns 0 if successful or -1 if error or heap is empty.
 * get_next		given a node, gets the next node in `next`.  Returns 0 if successful or -1 if error or n is the tail of heap.
 * get_prev		given a node, gets the previous node in `prev`.  Returns 0 if successful o -1 if error or n is the root of heap.
 * for_each		calls the callback for each node in the heap with given `extra` parameter.  This function
 *			can be used to keep pointers to nodes in the heap and use them with decrease_key.  Returns -1 if
 *			the callback requests to stop traversal and 0 otherwise.
 * duplicate		make an exact duplicate of the heap.
 * assign		shallow copy a heap.
 * compare		compare two heaps by an arbitrary order.  The compare function from h is used.  If the heaps are
 *			incompatible, it returns -2.  Otherwise it returns -1, 0 or 1 similar to `sh_compare`.
 * compare_lexic	similar to `compare`, but compares lexicographically.
 * clear		frees heap memory, but it can still be used (no sh_heap_init required)
 * free			frees heap memory.  sh_heap_init needs to be called if heap is to be used again
 *
 * shHeap specific functions:
 *
 * insert		insert a key in the heap.  If ref is not NULL, it will contain a reference to the inserted
 *			node so operations such as decrease_key and delete and can be performed on it.
 * find_min		get the element with the minimum key
 * delete_min		delete the element with the minimum key from the tree and return it
 * merge		merge two heaps.  The second heap will be cleared
 * decrease_key		decrease the value of a key given the node
 * delete		delete a given node
 */
int sh_heap_init(shHeap *h, sh_assign assign, size_t item_size, sh_compare cmp);
static inline int sh_heap_init_similar(shHeap *h, const shHeap *orig)
{
	return sh_heap_init(h, orig->assign, orig->item_size, orig->compare);
}

int sh_heap_get_first(shHeap *h, shNode *n);
int sh_heap_get_last(shHeap *h, shNode *n);
int sh_heap_get_next(shNode *n, shNode *next);
int sh_heap_get_prev(shNode *n, shNode *prev);
#define sh_heap_for_each(...) sh_heap_for_each(__VA_ARGS__, NULL)
int (sh_heap_for_each)(shHeap *h, sh_callback c, void *extra, ...);
int sh_heap_duplicate(shHeap *h, shHeap *h2);
static inline void sh_heap_assign(void *to, void *from)
{
	*(shHeap *)to = *(shHeap *)from;
}

int sh_heap_compare(const void *h, const void *h2);
int sh_heap_compare_lexic(const void *h, const void *h2);
void sh_heap_clear(shHeap *h);
void sh_heap_free(shHeap *h);


#define sh_heap_insert(...) sh_heap_insert(__VA_ARGS__, NULL)
int (sh_heap_insert)(shHeap *h, void *item, shNode *ref, ...);
int sh_heap_find_min(shHeap *h, shNode *n);
void sh_heap_delete_min(shHeap *h);
int sh_heap_merge(shHeap *h, shHeap *other);
int sh_heap_decrease_key(shNode *n, void *new_value);
int sh_heap_delete(shNode *n);

#ifndef NDEBUG
/* checks whether the heap is correctly is build.  Returns 0 if successful or -1 otherwise */
int sh_heap_internal_correctness_test(const shHeap *h);
#endif

#ifdef __cplusplus
}
#endif

#endif
