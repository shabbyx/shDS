/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_QUEUE_H
#define SH_QUEUE_H

#include "shlist.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef shList shQueue;

/*
 * shQueue is a queue of data.
 *
 * The functions of shQueue return 0 on success and -1 on failure, unless expressed otherwise:
 *
 * init			initializes the queue.
 * init_similar		initializes the queue similar to another queue.
 * get_first		gets the first (oldest) item of the queue in given node.  Returns 0 if successful or -1 if error or queue is empty.
 * get_last		gets the last (newest) item of the queue in given node.  Returns 0 if successful or -1 if error or queue is empty.
 * get_next		given a node, gets the next node in `next`.  Returns 0 if successful or -1 if error or n is the last item of the queue.
 * get_prev		given a node, gets the previous node in `prev`.  Returns 0 if successful o -1 if error or n is the first item of the queue.
 * for_each		calls the callback for each element in the queue with given `extra` parameter in order of index.
 *			Returns -1 if the callback requests to stop traversal and 0 otherwise.
 * duplicate		make an exact duplicate of the queue.
 * assign		shallow copy a queue.
 * compare		compare two queues by an arbitrary order.  The compare function from v is used.  If the queues are
 *			incompatible or `v` doesn't have a compare function, it returns -2.  Otherwise it returns -1, 0 or 1
 *			similar to `sh_compare`.
 * compare_lexic	similar to `compare`, but compares lexicographically.
 * clear		clears the queue.  The queue will not contain any data, but it can still be used (no sh_queue_init required).
 * free			frees the queue.  Requires sh_queue_init if it is to be used again.
 *
 * shQueue specific functions:
 *
 * push			insert data in the queue.
 * top			get the top of queue, which is the earliest element inserted which has not yet been removed.  Returns NULL
 *			if queue is empty.
 * pop			remove the top of queue.  Note that the pointer returned by top will be freed by this action.
 */
static inline int sh_queue_init(shQueue *q, sh_assign assign, size_t item_size)
{
	return sh_list_init(q, assign, item_size);
}

static inline int sh_queue_init_similar(shQueue *q, const shQueue *orig)
{
	return sh_list_init_similar(q, orig);
}

static inline int sh_queue_get_first(shQueue *q, shNode *n)
{
	return sh_list_get_first(q, n);
}

static inline int sh_queue_get_last(shQueue *q, shNode *n)
{
	return sh_list_get_last(q, n);
}

static inline int sh_queue_get_next(shNode *n, shNode *next)
{
	return sh_list_get_next(n, next);
}

static inline int sh_queue_get_prev(shNode *n, shNode *prev)
{
	return sh_list_get_prev(n, prev);
}

#define sh_queue_for_each(...) sh_queue_for_each(__VA_ARGS__, NULL)
static inline int (sh_queue_for_each)(shQueue *q, sh_callback c, void *extra, ...)
{
	return sh_list_for_each(q, c, extra);
}

static inline int sh_queue_duplicate(shQueue *q, shQueue *q2)
{
	return sh_list_duplicate(q, q2);
}

static inline void sh_queue_assign(void *to, void *from)
{
	sh_list_assign(to, from);
}

static inline int sh_queue_compare(const void *q, const void *q2)
{
	return sh_list_compare(q, q2);
}

static inline int sh_queue_compare_lexic(const void *q, const void *q2)
{
	return sh_list_compare_lexic(q, q2);
}

static inline void sh_queue_clear(shQueue *q)
{
	sh_list_clear(q);
}

static inline void sh_queue_free(shQueue *q)
{
	sh_list_free(q);
}


static inline int sh_queue_push(shQueue *q, void *d)
{
	return sh_list_append(q, d);
}

static inline void *sh_queue_top(shQueue *q)
{
	shNode n;

	if (sh_list_get_first(q, &n))
		return NULL;

	return n.data;
}

static inline void sh_queue_pop(shQueue *q)
{
	shNode n;

	if (sh_list_get_first(q, &n))
		return;

	sh_list_delete(&n, NULL);
}

#ifdef __cplusplus
}
#endif

#endif
