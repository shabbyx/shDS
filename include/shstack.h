/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_STACK_H
#define SH_STACK_H

#include "shvector.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef shVector shStack;

/*
 * shStack is a stack of data.
 *
 * The functions of shStack return 0 on success and -1 on failure, unless expressed otherwise:
 *
 * init			initializes the stack.
 * init_similar		initializes the stack similar to another stack.
 * get_first		gets the first (oldest) item of the stack in given node.  Returns 0 if successful or -1 if error or stack is empty.
 * get_last		gets the last (newest) item of the stack in given node.  Returns 0 if successful or -1 if error or stack is empty.
 * get_next		given a node, gets the next node in `next`.  Returns 0 if successful or -1 if error or n is the last item of the stack.
 * get_prev		given a node, gets the previous node in `prev`.  Returns 0 if successful o -1 if error or n is the first item of the stack.
 * for_each		calls the callback for each element in the stack with given `extra` parameter in order of index.
 *			Returns -1 if the callback requests to stop traversal and 0 otherwise.
 * duplicate		make an exact duplicate of the stack.
 * assign		shallow copy a stack.
 * compare		compare two stacks by an arbitrary order.  The compare function from v is used.  If the stacks are
 *			incompatible or `v` doesn't have a compare function, it returns -2.  Otherwise it returns -1, 0 or 1
 *			similar to `sh_compare`.
 * compare_lexic	similar to `compare`, but compares lexicographically.
 * clear		clears the stack.  The stack will not contain any data, but it can still be used (no sh_stack_init required).
 * free			frees the stack.  Requires sh_stack_init if it is to be used again.
 *
 * shStack specific functions:
 *
 * push			insert data in the stack.
 * top			get the top of stack, which is the latest element inserted which has not yet been removed.  Returns NULL
 *			if stack is empty.
 * pop			remove the top of stack.  Note that the pointer returned by top may be freed by this action.
 */
static inline int sh_stack_init(shStack *s, sh_assign assign, size_t item_size)
{
	return sh_vector_init(s, assign, item_size);
}

static inline int sh_stack_init_similar(shStack *s, const shStack *orig)
{
	return sh_vector_init_similar(s, orig);
}

static inline int sh_stack_get_first(shStack *s, shNode *n)
{
	return sh_vector_get_first(s, n);
}

static inline int sh_stack_get_last(shStack *s, shNode *n)
{
	return sh_vector_get_last(s, n);
}

static inline int sh_stack_get_next(shNode *n, shNode *next)
{
	return sh_vector_get_next(n, next);
}

static inline int sh_stack_get_prev(shNode *n, shNode *prev)
{
	return sh_vector_get_prev(n, prev);
}

#define sh_stack_for_each(...) sh_stack_for_each(__VA_ARGS__, NULL)
static inline int (sh_stack_for_each)(shStack *s, sh_callback c, void *extra, ...)
{
	return sh_vector_for_each(s, c, extra);
}

static inline int sh_stack_duplicate(shStack *s, shStack *s2)
{
	return sh_vector_duplicate(s, s2);
}

static inline void sh_stack_assign(void *to, void *from)
{
	sh_vector_assign(to, from);
}

static inline int sh_stack_compare(const void *s, const void *s2)
{
	return sh_vector_compare(s, s2);
}

static inline int sh_stack_compare_lexic(const void *s, const void *s2)
{
	return sh_vector_compare_lexic(s, s2);
}

static inline void sh_stack_clear(shStack *s)
{
	sh_vector_clear(s);
}

static inline void sh_stack_free(shStack *s)
{
	sh_vector_free(s);
}


static inline int sh_stack_push(shStack *s, void *d)
{
	return sh_vector_append(s, d);
}

static inline void *sh_stack_top(shStack *s)
{
	return sh_vector_at(s, s->size - 1);
}

static inline void sh_stack_pop(shStack *s)
{
	if (s->size > 0)
		--s->size;
}

#ifdef __cplusplus
}
#endif

#endif
