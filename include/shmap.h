/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_MAP_H
#define SH_MAP_H

#include <stdbool.h>
#include "shnode.h"
#include "shcallbacks.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct shMap
{
	void *root;
	void *first, *last;
	size_t size;			/* number of nodes in the map */
	size_t key_size;		/* size of each key in the map */
	size_t data_size;		/* size of each data in the map */

	sh_assign key_assign;
	sh_assign data_assign;
	sh_compare compare;		/* map uses this function to compare its keys */

	void *internal;
} shMap;

/*
 * shMap is a threaded AVL tree, as its search time is more important than modification time.
 *
 * The functions of shMap return 0 on success and -1 on failure, unless expressed otherwise:
 *
 * init			initializes the map.
 * init_similar		initializes the map similar to another map.
 * get_first		gets the first item of the map in given node.  Returns 0 if successful or -1 if error or map is empty.
 * get_last		gets the last item of the map in given node.  Returns 0 if successful or -1 if error or map is empty.
 * get_next		given a node, gets the next node in `next`.  Returns 0 if successful or -1 if error or n is the last item of the map.
 * get_prev		given a node, gets the previous node in `prev`.  Returns 0 if successful o -1 if error or n is the first item of the map.
 * for_each		calls the callback for each node in the map with given `extra` parameter.  The order of calling
 *			is breadth-first, because if this callback is used to cache the map, later when adding nodes one
 *			by one, the tree will never need rebalancing.  This is not guaranteed though, as the function may
 *			resort to sh_map_inorder if there is not enough memory.  Returns -1 if the callback requests to stop traversal
 *			and 0 otherwise.
 * duplicate		make an exact duplicate of the map.
 * assign		shallow copy a map.
 * compare		compare two maps by an arbitrary order.  The compare function from m is used.  If the maps are
 *			incompatible, it returns -2.  Otherwise it returns -1, 0 or 1 similar to `sh_compare`.
 * compare_lexic	similar to `compare`, but compares lexicographically.
 * clear		frees map memory, but it can still be used (no sh_map_init required).
 * free			frees map memory.  sh_map_init needs to be called if map is to be used again.
 *
 * shMap specific functions:
 *
 * find			looks key up and fills data in given node if not NULL.  Returns false if key does not exist or
 * 			true if successful.
 * exists		returns true if key exists in map and false otherwise.
 * at			get pointer to data of a node where a key is pointing to.  If no such key exists, NULL is returned.
 * insert		inserts (key, data) pair in the tree.  Returns 0 if successful, 1 if a node with given key already
 *			exists or -1 in case of errors.
 * replace		similar to insert, but instead of failing if key exists, it will replace its data (and return 0).
 *			If you require the old data, the most efficient way would be to `find` the node and replace the
 *			data it points to.
 * inorder		similar to `sh_map_for_each`, but traverses the tree in-order. This is useful to get the data
 * 			sorted based on key.
 * delete		deletes given node and sets `next` to the next node in map if not NULL.  Returns 0 if successful or -1 if
 *			deleting largest element of map.
 * delete_key		similar to delete, but searches for key and deletes it instead of being given the node.
 */
int sh_map_init(shMap *m, sh_assign key_assign, size_t key_size,
		sh_assign data_assign, size_t data_size, sh_compare cmp);
static inline int sh_map_init_similar(shMap *m, const shMap *orig)
{
	return sh_map_init(m, orig->key_assign, orig->key_size, orig->data_assign, orig->data_size, orig->compare);
}

int sh_map_get_first(shMap *m, shNode *n);
int sh_map_get_last(shMap *m, shNode *n);
int sh_map_get_next(shNode *n, shNode *next);
int sh_map_get_prev(shNode *n, shNode *prev);
#define sh_map_for_each(...) sh_map_for_each(__VA_ARGS__, NULL)
int (sh_map_for_each)(shMap *m, sh_callback c, void *extra, ...);
int sh_map_duplicate(shMap *m, shMap *m2);
static inline void sh_map_assign(void *to, void *from)
{
	*(shMap *)to = *(shMap *)from;
}

int sh_map_compare(const void *m, const void *m2);
int sh_map_compare_lexic(const void *m, const void *m2);
void sh_map_clear(shMap *m);
void sh_map_free(shMap *m);


bool sh_map_find(shMap *m, const void *k, shNode *n);
static inline bool sh_map_exists(shMap *m, const void *k)
{
	return sh_map_find(m, k, NULL);
}

static inline void *sh_map_at(shMap *m, const void *k)
{
	shNode n;

	if (!sh_map_find(m, k, &n))
		return NULL;

	return n.data;
}

#define sh_map_insert(...) sh_map_insert(__VA_ARGS__, NULL)
int (sh_map_insert)(shMap *m, void *k, void *d, shNode *ref, ...);
#define sh_map_replace(...) sh_map_replace(__VA_ARGS__, NULL)
int (sh_map_replace)(shMap *m, void *k, void *d, shNode *ref, ...);
#define sh_map_inorder(...) sh_map_inorder(__VA_ARGS__, NULL)
int (sh_map_inorder)(shMap *m, sh_callback c, void *extra, ...);
#define sh_map_delete(...) sh_map_delete(__VA_ARGS__, NULL)
int (sh_map_delete)(shNode *n, shNode *next, ...);

static inline int sh_map_delete_key(shMap *m, const void *k)
{
	shNode n;

	if (!sh_map_find(m, k, &n))
		return -1;

	sh_map_delete(&n);
	return 0;
}

#ifndef NDEBUG
/* checks whether the tree is balanced correctly.  Returns 0 if successful or -1 otherwise */
int sh_map_internal_balance_test(const shMap *m);
#endif

#ifdef __cplusplus
}
#endif

#endif
