/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_SET_H
#define SH_SET_H

#include <stdbool.h>
#include "shmap.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef shMap shSet;
struct shVector;

/*
 * A set is a map of keys that point to non-NULL (otherwise insignificant) values.  It uses shMap and later is able
 * to convert the set into an shVector.  Therefore, during construction it has optimal insertion and deletion and
 * after conversion to shVector has optimal storage and search time.  Of course, the search in the set after conversion
 * to shVector is done by sh_vector_bsearch.
 *
 * The functions of shSet return 0 on success and -1 on failure, unless expressed otherwise:
 *
 * init			same as sh_map_init
 * init_similar		same as sh_map_init_similar
 * get_first		gets the first item of the set in given node.  Returns 0 if successful or -1 if error or set is empty.
 * get_last		gets the last item of the set in given node.  Returns 0 if successful or -1 if error or set is empty.
 * get_next		given a node, gets the next node in `next`.  Returns 0 if successful or -1 if error or n is the last item of the set.
 * get_prev		given a node, gets the previous node in `prev`.  Returns 0 if successful o -1 if error or n is the first item of the set.
 * for_each		same as sh_map_for_each
 * duplicate		make an exact duplicate of the set.
 * assign		shallow copy a set.
 * compare		compare two sets by an arbitrary order.  The compare function from m is used.  If the sets are
 *			incompatible, it returns -2.  Otherwise it returns -1, 0 or 1 similar to `sh_compare`.
 * compare_lexic	similar to `compare`, but compares lexicographically.
 * clear		same as sh_map_clear
 * free			same as sh_map_free
 *
 * shSet specific functions:
 *
 * find			same as sh_map_find
 * exists		same as sh_map_exists
 * insert		same as sh_map_insert
 * inorder		same as sh_map_inorder
 * to_vector		converts the set into a sorted vector.  Later, the set can be discarded and sh_vector_bsearch used.
 *			This will be a vector of keys that are shallow copied from the map.  The vector will have the same
 *			assign function as the set.
 * delete		deletes given node and sets `next` to the next node in set if not NULL.  Returns 0 if successful or -1 if
 *			deleting largest element of set.
 * delete_key		similar to delete, but searches for key and deletes it instead of being given the node.
 */
int sh_set_init(shSet *s, sh_assign assign, size_t item_size, sh_compare cmp);
static inline int sh_set_init_similar(shSet *s, const shSet *orig)
{
	return sh_set_init(s, orig->key_assign, orig->key_size, orig->compare);
}

static inline int sh_set_get_first(shSet *s, shNode *n)
{
	return sh_map_get_first(s, n);
}

static inline int sh_set_get_last(shSet *s, shNode *n)
{
	return sh_map_get_last(s, n);
}

static inline int sh_set_get_next(shNode *n, shNode *next)
{
	return sh_map_get_next(n, next);
}

static inline int sh_set_get_prev(shNode *n, shNode *prev)
{
	return sh_map_get_prev(n, prev);
}

#define sh_set_for_each(...) sh_set_for_each(__VA_ARGS__, NULL)
static inline int (sh_set_for_each)(shSet *s, sh_callback c, void *extra, ...)
{
	return sh_map_for_each(s, c, extra);
}

static inline int sh_set_duplicate(shSet *s, shSet *s2)
{
	return sh_map_duplicate(s, s2);
}

static inline void sh_set_assign(void *to, void *from)
{
	sh_map_assign(to, from);
}

static inline int sh_set_compare(const void *s, const void *s2)
{
	return sh_map_compare(s, s2);
}

static inline int sh_set_compare_lexic(const void *s, const void *s2)
{
	return sh_map_compare_lexic(s, s2);
}

static inline void sh_set_clear(shSet *s)
{
	sh_map_clear(s);
}

static inline void sh_set_free(shSet *s)
{
	sh_map_free(s);
}


static inline bool sh_set_find(shSet *s, const void *k, shNode *n)
{
	return sh_map_find(s, k, n);
}

static inline bool sh_set_exists(shSet *s, const void *k)
{
	return sh_map_find(s, k, NULL);
}

#define sh_set_insert(...) sh_set_insert(__VA_ARGS__, NULL)
static inline int (sh_set_insert)(shSet *s, void *k, shNode *ref, ...)
{
	return (sh_map_insert)(s, k, s, ref);
}

#define sh_set_inorder(...) sh_set_inorder(__VA_ARGS__, NULL)
static inline int (sh_set_inorder)(shSet *s, sh_callback c, void *extra, ...)
{
	return sh_map_inorder(s, c, extra);
}

int sh_set_to_vector(shSet *s, struct shVector *v);

#define sh_set_delete(...) sh_set_delete(__VA_ARGS__, NULL)
static inline int (sh_set_delete)(shNode *n, shNode *next, ...)
{
	return sh_map_delete(n, next);
}

static inline int sh_set_delete_key(shMap *m, const void *k)
{
	return sh_map_delete_key(m, k);
}

#ifdef __cplusplus
}
#endif

#endif
