/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_CALLBACKS_H
#define SH_CALLBACKS_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

struct shNode;

/*
 * These are generic callbacks used by various data structured.
 *
 * sh_assign:		copy value from given data `from` to where the structure would store it `to`.
 *			If the data structure contains `X`, `to` would be of type `X *`, while `from`
 *			is whatever type given when adding an element.
 * sh_compare:		compare two values given their addresses.  For example if the structure contains
 *			`X *`s, then each `a` and `b` would have type `X * const *`.  The return value
 *			should be similar to `strcmp`, i.e. 0 if `a` and `b` are equal, negative if `a`
 *			is smaller and positive otherwise.
 * sh_callback:		called when traversing a structure.  It is given a generic node as well as extra
 *			data provided by the user.  If this function returns false, the traversing would
 *			be stopped.  The *_for_each function to which this callback is given would also
 *			return a negative value to indicate the traversal is canceled.  This is so even
 *			if this function returns false for the last element.
 */
typedef void (*sh_assign)(void *to, void *from);
typedef int (*sh_compare)(const void *a, const void *b);
typedef bool (*sh_callback)(struct shNode *n, void *extra);

#ifdef __cplusplus
}
#endif

#endif
