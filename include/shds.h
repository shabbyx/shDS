/*
 * Copyright (C) 2012-2015 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of shDS: Common Data Structures for C.
 *
 * shDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * shDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shDS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SH_DS_H
#define SH_DS_H

/*#include "shvector.h"		-- included in shstack.h */
/*#include "shlist.h"		-- included in shqueue.h */
/*#include "shnode.h"		-- included in shlist.h */
/*#include "shmap.h"		-- included in shset.h */
#include "shstack.h"
#include "shqueue.h"
#include "shset.h"
#include "shheap.h"

#endif
