shDS: Common Data Structures for C
==================================

Travis-CI: [![Build Status](https://travis-ci.org/ShabbyX/shDS.svg?branch=master)](https://travis-ci.org/ShabbyX/shDS)

This library implements some of the more common data structures for C.  Its goal is to have useful yet non-trivial
structures available to the programmer.  It does not at all mean to bring C++ STL to C.  Note the **non-trivial**
keyword, shDS does not try to dumb down C programming, for example by providing C++ style string concatenation and
copying, resulting in huge number of useless copies and memory allocations/frees.

It is striven for the properties governing all structures to follow the same rules.  The functionality of the
structures are inherently different, but their data storage, initialization and cleanup patterns are unified.  These
rules can be found on the bottom of this document.

shDS has implemented the following structures:

- `shVector`: A variable length vector
  * Insertion: Amortized O(1)
  * Search: O(log(n)) if sorted, otherwise handled by the user himself
  * Deletion: Only the last element (in O(1)).  Otherwise it would be inefficient
  * Utilities: Concat (O(m), m = size of second vector), sort (O(nlog(n))), sorted insert (O(n))
- `shStack`: Convenience structure for stacks wrapped around `shVector`
- `shList`: A doubly-linked list
  * Insertion: O(1) to back and front and anywhere else in the list given the neighboring node is already found
  * Deletion: O(1) anywhere in the list given the node is already found
  * Utilities: Concat (O(1)), break list in two (O(1))
- `shQueue`: Convenience structure for queues wrapped around `shList`
- `shMap`: A threaded AVL tree<sup>#</sup>
  * Insertion: O(log(n))
  * Search: O(log(n))
  * Deletion: O(log(n))
  * Utilities: traversal of nodes either sorted (in-order) or optimal for save/restore (BFS)
- `shSet`: Convenience structure for sets wrapped around `shMap`
  * Utilities: Convert to `shVector`<sup>##</sup>.
- `shHeap`: A pairing heap
  * Insertion: O(1)
  * Deletion: O(log(n)) amortized for minimum and any other given the node is already found
  * Decrease key: 2^O(sqrt(log log n)) amortized given the node is already found
  * Merge: O(1)

The following structures are provisioned:
- `shMapd`: A red-black tree.  Note that shMap has slower construction but faster lookup, while shMapd has faster
  construction and slower look up.  The suffix `d` means dynamic, so it is better for more dynamic maps
- `shSetd`: Same as shMapd.  Better than `shSet` for more dynamic sets
- `shHash`: A hash-map
- `shPriorityQueue`: This is easily written using `shHeap`

The following structures will _not_ be included:
- `shString`: Strings in C are quite simple, a wrapper for its functions are meaningless.
- `shArray`: Again, arrays are quite trivial.  Variable length arrays are the same as `shVector`, so there is no use in
  reimplementing them.

<sup>#</sup> Not exactly a threaded tree in the well-known sense, but more similar to a sorted doubly-linked list with
a binary tree on top.  In other words, while a normal threaded tree uses one pointer for either right or next node (and
similarly one for either left or previous node), this implementation uses separate pointers for each.  Therefore, it has
higher performance at the cost of slightly higher memory usage.

<sup>##</sup> Using this tool, one can create a set (removing duplicate values)
and then convert it to a sorted vector for efficiency reasons.  One example would be multiple maps from the same
string.  First, a set of these strings are created and converted to vector.  The maps are then created as simple
arrays.  To use this, first the string is binary searched in the vector, getting its index, and then use this
index in the arrays

---

For every structure `shX` the following properties hold:

### 1. Initialization and Cleanup

1. There always exist `sh_x_init`, `sh_x_init_similar`, `sh_x_duplicate`, `sh_x_clear` and `sh_x_free`
  * `sh_x_init` is mandatory before using the object
  * `sh_x_init_similar` is the same as `sh_x_init`, except it initializes the structure with properties similar to another `shX`
  * `sh_x_duplicate` duplicates the structure
  * `sh_x_clear` clears the structure, but it can still be used afterwards (no `sh_x_init` needed)
  * `sh_x_free` makes the object unusable (reuse requires `sh_x_init`)
2. The `sh_x_init` function takes size of items the structure stores as well as a function that knows how to assign those
  items.  If the structure uses `key`/`data` pairs, a pair of such arguments are taken.
  * _Rationale_: This allows the structures to store non-pointer types, avoiding extra `malloc`s at the cost of calling
    the `assign` callback that knows how to copy the structure's data.
3. Calling `sh_x_init` multiple times is safe and leak-free
  * _Rationale_: If the structure is reused, it may be inconvenient to track whether it has been previously initialized or
    not. This allows each user of reused structure to reinitialize it without worrying about previous initializations.
4. Calling `sh_x_free` multiple times is safe
  * _Rationale_: A final cleanup wouldn't need to care whether the objects are already freed or not.
5. It is safe for an object that has been initialized with `shX x = {0};` but for which `sh_x_init` has not been called,
  to be freed using `sh_x_free`
  * _Rationale_: If returning due to error in initialization of an object, other objects could be freed without having to
    worry about whether they are initialized or not.  Note that using this syntax, the object is not necessarily
    properly initialized and `sh_x_init` is still required before usage.
6. If `sh_x_clear` is called for an object for which `sh_x_init` has been previously successfully called, `sh_x_init` can be
  called on it safely without risk of memory leaks.
  * _Rationale_: This is for the case where many functions use the same object and clear it in the end, but they could
    be executed in any order.  Given this property, each of them can call `sh_x_init` without risk of memory leaks.
7. `sh_x_duplicate` tries to duplicate the structure exactly as it is made.  It will copy the structure data with `memcpy`,
  and therefore no `assign` or `compare` callback is called.
8. `sh_x_assign` shallow copies the structure.  It is equivalent to simply using the assignment operator.  This function is
  suitable as an `assign` callback.
9. `sh_x_compare` and `sh_x_compare_lexic` compare two structures.  The first one is faster since it can immediately
  distinguish two objects that have different `size`s.  Both are suitable as `compare` callbacks.

### 2. Data Types

1. If the structure stores values of type `T`, it will receive data as a pointer, which depending on the user's
  implementation of `assign` function could be of type `T` (if `T` is a pointer), `T *` or any other the user decides.
  The data returned are always pointers (such as that stored in `shNode`) and are always of type `T *`.
  * _Rationale_: For the structures to take data by value, freeing the user from having to `malloc/free` memory for them,
    the structures allocate space for the data and let the user fill them how they see fit.  Consequently they will always
    return a pointer to those data<sup>1</sup>.
2. If the structure stores values of type `T`, the first argument of the `assign` callback should be of type `T *` while the
  second argument is decided by the user.  Both arguments of the `compare` callback should be of type `const T *`.  _This
  implies that search (e.g. `find` or `exists`) parameters are also taken as `const T *`_. Recommended practice is to give
  `T *` as second argument of `assign` also<sup>2</sup>.

### 3. Access

1. There always exists a `size` member telling the number of elements in the structure
2. For every structure, `sh_x_for_each` iterates and runs a `callback` on every element of the structure in an unspecified
  order, unless otherwise stopped by the return value of the callback itself.  The callback receives a `shNode` that points
  to the data as well as a user-provided extra argument.  `shNode` contains `key` and `data` fields that point to key and/or
  data of the structure, depending on whether the structure has that kind of value.

  The structure is not to be modified in the callback.
3. For every structure, `sh_x_get_first` in combination with `sh_x_get_next` can be used to iterate over the elements of the
  structure. `sh_x_get_last` and `sh_x_get_prev` can be used to iterate the structure in the reverse order.

  Modifications to the structure may invalidate the iterator `shNode`s.

Documentation will be added.  In the meantime, look at the headers.

<sup>1</sup> To make sure this doesn't gravely affect performance, tests have been made.  One using `std::map` of C++,
another using older shDS `shMap` where each key and data was `malloc`ed and their pointers stored in map nodes, and
another with the current version.  Inserting 10,000,000 key/data pairs was tested as well as iterating over the map.
The results showed that the current method is slower than the old method by ~20% in insertion (and ~35% slower than
`std::map`), but faster than the old method by ~55% in traversal (and ~60% faster than `std::map`) on the test computer.
Note to self: apparently, the test has been performed with shDS not built with optimization.  Needs to be redone.

<sup>2</sup> Tests have been performed with giving a `void *extra` argument to `assign` and `compare` functions, but the
results show that the performance loss is above unacceptable.  Since standard C functions such as `qsort`, `bsearch` etc
assume assign-ability and comparability of items to be inherent and therefore no extra argument is required, I have adopted
the same strategy even though in rare cases that extra argument may come in handy.
